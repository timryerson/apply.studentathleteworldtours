# config valid only for Capistrano 3.1
lock '3.13.0'

# choose a name for this thing
set :application, 'apply-saw-tours'

# set the repo url cause... duh.
set :repo_url, 'git@gitlab.com:timryerson/apply.studentathleteworldtours.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
# set :deploy_to, '/home/studenta/apply.studentathleteworldtours.com/production_directory'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Set the tmp directory cause... permissions
set :tmp_dir, "/home/applystu/apply.studentathleteworldtours.com/tmp-deploy"

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
set :linked_dirs, %w{bin log public/apply-tours-admin/user/cache public/images/avatars public/images/captchas public/images/member_photos public/images/pm_attachments public/images/signature_attachments public/images/uploads}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Database syncing
require 'capistrano-db-tasks'

# Disable pushing dev database to production because... damn.
set :disallow_pushing, true

# Remove remote db after downloading
set :db_remote_clean, true

# Remove local, downloaded db after loading
# set :db_local_clean, true

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
