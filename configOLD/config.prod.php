<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Production config overrides & db credentials
 * 
 * Our database credentials and any environment-specific overrides
 * 
 * @package    Focus Lab Master Config
 * @version    2.1.1
 * @author     Focus Lab, LLC <dev@focuslabllc.com>
 */

$env_config['database'] = array (
	'expressionengine' => array (
		'hostname' => 'localhost',
		'username' => 'applystu_applyee',
		'password' => 'PelvicRageOutfitYeomen50',
		'database' => 'applystu_applysawt_ee',
		'dbdriver' => 'mysqli',
		'dbprefix' => 'exp_',
		'pconnect' => FALSE
	),
);

// Sample global variable for Production only
// Can be used in templates like "{global:google_analytics}"
// Global variable for Local only

// Google Analytics, set to xxx to prevent tracking on local
$env_global['global:google_id']= 'xxx';

// google Maps API Key
// $env_global['global:google_maps_api_key'] = 'AIzaSyC_9y_2S3ptkdJtCewlAldrmC0cNVuMwuE'; //Imp's catch-all (no domain limiting)
$env_global['global:google_maps_api_key'] = 'AIzaSyC0_w_iMsCQNxa8ULY8YtrHYa91nE16CNM'; // TESTING JS Api Key owned by brandon
$env_global['global:google_cse'] = '018309287662635088642:e_p8wq9sllm'; // Custom Search Engine ID owned by nashpr@nashcc.edu
$env_global['global:staging_redirect'] = '';
$env_global['global:environment_banner'] = '';

// javascript caching
/* for local versioning, we'll grab the current date so we always get a non-cached version of our js */
// $jsVersion = date('m.d.h-i-s'); //month.day.hour-minute-second
// $env_global['global:js_version'] = $jsVersion;


// javascript caching
/* for production/live versioning, enter the "real" version number (or if in developement and production deploys to test site, set to a date) */
$jsVersion = '0.3'; //date('m.d.h-i'); //month.day.hour-minute
$env_global['global:js_version'] = $jsVersion;


/* End of file config.local.php */
/* Location: ./config/config.local.php */