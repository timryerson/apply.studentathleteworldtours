<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Development config overrides & db credentials
 * 
 * Our database credentials and any environment-specific overrides
 * 
 * @package    Focus Lab Master Config
 * @version    2.1.1
 * @author     Focus Lab, LLC <dev@focuslabllc.com>
 */

$env_config['database'] = array (
	'expressionengine' => array (
		'hostname' => 'localhost',
		'username' => '',
		'password' => '',
		'database' => '',
		'dbdriver' => 'mysqli',
		'dbprefix' => 'exp_',
		'pconnect' => FALSE
	),
);

// Google Analytics, set to xxx to prevent tracking on local
$env_global['global:google_id']= 'xxx';

// google Maps API Key
// $env_global['global:google_maps_api_key'] = 'AIzaSyC_9y_2S3ptkdJtCewlAldrmC0cNVuMwuE'; //Imp's catch-all (no domain limiting)
$env_global['global:google_maps_api_key'] = 'AIzaSyC0_w_iMsCQNxa8ULY8YtrHYa91nE16CNM'; // TESTING JS Api Key owned by brandon
$env_global['global:google_cse'] = '018309287662635088642:e_p8wq9sllm'; // Custom Search Engine ID owned by nashpr@nashcc.edu
$env_global['global:staging_redirect'] = '';
$env_global['global:environment_banner'] = '<style type="text/css">#environment-banner{position: fixed;bottom: 0;left: 0;right: 0;height: 1.25rem;background-color: rgba(255,125,0,0.5);font-style: bold;font-size: 0.75em;line-height: 1em;text-align: center;z-index: 9999999;color: white;text-shadow: 1px 1px 1px black;line-height: 1.25rem;font-weight: 800;}</style><div id="environment-banner">This is the ' . ENV_FULL . ' Environment. PHP Version: '. PHP_VERSION . '. It may use a different database than production.</div>';

// javascript caching
/* for local versioning, we'll grab the current date so we always get a non-cached version of our js */
$jsVersion = date('m.d.h-i-s'); //month.day.hour-minute-second
$env_global['global:js_version'] = $jsVersion;


// javascript caching
/* for development versioning, we'll grab the current date so we always get a non-cached version of our js */
$jsVersion = date('m.d.h-i'); //month.day.hour-minute
$env_global['global:js_version'] = $jsVersion;


/* End of file config.local.php */
/* Location: ./config/config.local.php */