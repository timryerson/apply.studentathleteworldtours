**Hop SOPs and FAQs:** https://sites.google.com/hopstudios.com/wiki/developer-sops

## Local dev instructions

1. Build the local container
    > `npm start`
2. Import the DB from prod
    > `dbimport_remote`  

## EXAMPLE Compile assets

1. EXAMPLE: `npm run build`

## EXAMPLE More Stuff


1. Put here any other instructions
2. that are important 
3. to know in order to use this repo locally
