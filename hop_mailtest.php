<h1>Hop Mail Test</h1>

<?php 
    ini_set( 'display_errors', 1 );
    error_reporting( E_ALL );
    $from = "testing@hopstudios.com";
    $to = "tech@hopstudios.com";
    $subject = "Hop PHP Mail Test";
    $message = "If you can see this message, it is working!";
    $headers = "From:" . $from;
    mail($to,$subject,$message, $headers);
    echo "Hop php mail test sent. Please check tech@hopstudios.com for the message.";
?>