#!/bin/bash
HOPOPS='\033[00;93m--Hop-Ops-- \033[0m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
NORM='\033[0m'

# SET THESE VARIABLES (client name must be the same name you used in docker-compose)
CLIENTNAME='clientname'

# Check for underscores or dashes in container names
DOCKER_MSYQL_SUFFIX='-mysql-1'
if [ ! "$(docker ps -q -f name="${CLIENTNAME}${DOCKER_MSYQL_SUFFIX}")" ]; then
    DOCKER_MSYQL_SUFFIX='_mysql_1'
fi
DOCKER_MYSQL_CONTAINER=${CLIENTNAME}$DOCKER_MSYQL_SUFFIX

# WORDPRESS NOTE - If this is WordPress, uncomment below and make sure to also set variables in hop_scripts/pull_plugins.sh
# echo ''
# echo "${HOPOPS}${YELLOW}ATTENTION: If you haven't already imported the WordPress plugins from the remote server by running ${BLUE}npm run pullplugins${YELLOW}, hit cmd+c now to cancel this operation, and do that first!${NORM}"

echo ''
echo "${HOPOPS}Updating local database from local sql file at ./docker/mysql/dump/${CLIENTNAME}.sql"
# if the db is not on the same server, add -h MYSQL_HOST
docker container exec -i ${DOCKER_MYSQL_CONTAINER} mysql -u root -proot ${CLIENTNAME} < ./docker/mysql/dump/${CLIENTNAME}.sql

# WORDPRESS NOTE - If this is WordPress, uncomment below and make sure to also set variables in docker/mysql/startup.sql
# echo ''
# echo "${HOPOPS}Fixing URLs in local database..."
# docker container exec -i ${DOCKER_MYSQL_CONTAINER} mysql -u root -proot ${CLIENTNAME} < docker/mysql/startup.sql

echo "${HOPOPS}Done!"
echo ''
