#!/bin/bash
HOPOPS='\033[00;93m--Hop-Ops-- \033[0m'
YELLOW='\033[0;33m'
NORM='\033[0m'

# SET THESE VARIABLES (client name must be the same name you used in docker-compose)
CLIENTNAME='clientname'
SSH_USER_NAME='root'
SSH_HOST_NAME='myclient.com'

# Below variables will probably be correct in 92.5% of cases
REMOTE_PLUGIN_DIR='public_html/wp-content/plugins'
LOCAL_PLUGIN_DIR='wp-content'

echo ''
echo "${HOPOPS}Pulling remote plugins directory to your local project, this may take a minute..."
rsync -cazPq ${SSH_USER_NAME}@${SSH_HOST_NAME}:${REMOTE_PLUGIN_DIR} ${LOCAL_PLUGIN_DIR}
echo "${HOPOPS}Plugin file import done!"
echo ''
