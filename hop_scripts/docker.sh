#!/bin/bash
HOPOPS='\033[00;93m--Hop-Ops-- \033[0m'
YELLOW='\033[0;33m'
NORM='\033[0m'

echo ''
if [[ ! -d "node_modules" ]] ; then
    echo "${HOPOPS}Installing node modules..."
    npm install
else
    echo "${HOPOPS}Node modules already installed, skipping step..."
fi

echo ''
if [[ ! -d "vendor" ]] && [[ -f "composer.json" ]] ; then
    echo "${HOPOPS}Installing Composer packages..."
    composer install
else
    echo "${HOPOPS}Composer packages already installed or composer.json doesn't exist, skipping step..."
fi

echo ''

echo "${HOPOPS}Launching docker container..."

docker network create traefik
cd ../hoperations
docker-compose up -d
cd -
docker-compose up -d

echo "${HOPOPS}Client is now running at ${YELLOW}http://${PWD##*/}.localhost${NORM}"
echo "${HOPOPS}When you're done work, run ${YELLOW}npm stop${NORM} to recover container resources."
echo ""
echo "${HOPOPS}Need to import the DB? Look up the ssh password, then run ${YELLOW}npm dbimport_remote${NORM}."
