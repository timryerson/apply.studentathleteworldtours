#!/bin/bash
HOPOPS='\033[00;93m--Hop-Ops-- \033[0m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
NORM='\033[0m'

# SET THESE VARIABLES (client name must be the same name you used in docker-compose)
CLIENTNAME='apply.studentathleteworld'
SSH_USER_NAME='a2b074a5_1'
SSH_HOST_NAME='bee610e67e.nxcli.io'
REMOTE_MYSQL_USER='a2b074a5_apply23'
REMOTE_MYSQL_PASS='StalerTyingNasalsStared'
REMOTE_DB_NAME='a2b074a5_apply23'

# Check for underscores or dashes in container names
DOCKER_MSYQL_SUFFIX='-mysql-1'
if [ ! "$(docker ps -q -f name="${CLIENTNAME}${DOCKER_MSYQL_SUFFIX}")" ]; then
    DOCKER_MSYQL_SUFFIX='_mysql_1'
fi
DOCKER_MYSQL_CONTAINER=${CLIENTNAME}$DOCKER_MSYQL_SUFFIX

# WORDPRESS NOTE - If this is WordPress, uncomment below and make sure to also set variables in hop_scripts/pull_plugins.sh
# echo ''
# echo "${HOPOPS}${YELLOW}ATTENTION: If you haven't already imported the WordPress plugins from the remote server by running ${BLUE}npm run pullplugins${YELLOW}, hit cmd+c now to cancel this operation, and do that first!${NORM}"

echo ''
echo "${HOPOPS}Updating local database from remote production server..."
# if the db is not on the same server, add -h MYSQL_HOST
ssh -C ${SSH_USER_NAME}@${SSH_HOST_NAME} "mysqldump -u ${REMOTE_MYSQL_USER} -p${REMOTE_MYSQL_PASS}  --no-tablespaces ${REMOTE_DB_NAME}" > ${CLIENTNAME}.sql
docker container exec -i applystudentathleteworldtours-mysql-1 mysql -u root -proot ${CLIENTNAME} < ${CLIENTNAME}.sql
rm ${CLIENTNAME}.sql

# WORDPRESS NOTE - If this is WordPress, uncomment below and make sure to also set variables in docker/mysql/startup.sql
# echo ''
# echo "${HOPOPS}Fixing URLs in local database..."
# docker container exec -i ${DOCKER_MYSQL_CONTAINER} mysql -u root -proot ${CLIENTNAME} < docker/mysql/startup.sql

echo "${HOPOPS}Done!"
echo ''
