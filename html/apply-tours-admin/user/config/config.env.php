<?php

/**
 * Environment Declaration
 *
 * This switch statement sets our environment. The environment is used primarily
 * in our custom config file setup. It is also used, however, in the front-end
 * index.php file and the back-end admin.php file to set the debug mode
 *
 * @package    Focus Lab Master Config
 * @version    2.2.0
 * @author     Focus Lab, LLC <dev@focuslabllc.com>
 */

if ( ! defined('ENV'))
{
	// NOTE: is this a multisite? enter all your production/staging urls in these arrays
	$production	= ['apply.studentathleteworld.com'];
	$staging	= ['staging.CLIENTSITE.ca'];
	$local		= 'apply.studentathleteworld.localhost';
	$url 		= strtolower($_SERVER['HTTP_HOST']);

	if (in_array($url, $production)) {
		define('ENV', 'prod');
		define('ENV_FULL', 'Live');
		define('ENV_DEBUG', FALSE);
		define('ENV_DOMAIN', $url);
		define('PROTOCOL','https://');

	} elseif (in_array($url, $staging)) {
		define('ENV', 'staging');
		define('ENV_FULL', 'Staging');
		define('ENV_DEBUG', TRUE);
		define('ENV_DOMAIN', $url);
		define('PROTOCOL','https://');

	} else {
		define('ENV', 'local');
		define('ENV_FULL', 'Local');
		define('ENV_DEBUG', TRUE);
		define('ENV_DOMAIN', $local);
		define('PROTOCOL','http://');
	}
}

$env_global['CONF_ENV'] = ENV;
/* End of file config.env.php */
/* Location: ./config/config.env.php */
