<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * LIVE config overrides & db credentials
 *
 * Our database credentials and any environment-specific overrides
 *
 * @package    Focus Lab Master Config
 * @version    2.2.0
 * @author     Focus Lab, LLC <dev@focuslabllc.com>
 */

$config['database'] = array (
	'expressionengine' => array (
		'hostname' => 'localhost',
		'username' => 'a2b074a5_apply23',
		'password' => 'StalerTyingNasalsStared',
		'database' => 'a2b074a5_apply23',
		'dbdriver' => 'mysqli',
		'dbprefix' => 'exp_',
		'pconnect' => FALSE
	),
);


$config['debug'] = '0';
// In order to save anything to log files, you’ll need to create the /system/user/logs/ directory and ensure it’s writable
$config['log_threshold'] = '0';

// Add config variables here and use in templates like "{CONF_GOOGLE_ANALYTICS}"
// If you add more, start them with CONF_ for Config Variable
// $env_global['CONF_GOOGLE_ANALYTICS'] = 'UA-65722839-1';

/* End of file config.live.php */
