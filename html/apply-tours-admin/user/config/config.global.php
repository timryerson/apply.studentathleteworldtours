<?php

/*
| HOP STUDIOS GLOBAL CONFIG FILE
|
| This file is for everything that is shared between environments
| Please delete any blocks that are not used on this site, for readability.
| Make sure to read what you're using, and change anything that needs changing for your server!
|
*/

$config['base_url'] = PROTOCOL . strtolower($_SERVER['HTTP_HOST']) .'/';
$config['base_path'] = $_SERVER['DOCUMENT_ROOT'] .'/';
$config['theme_folder_url'] =  $config['base_url'] . '/themes/';
// removed PROTOCOL . from line above
$config['theme_folder_path'] = $config['base_path'] . 'themes/';

$config['use_category_name'] = 'y';
$config['updater_allow_advanced'] = 'y';

$config['is_site_on'] = 'y';
$config['save_tmpl_revisions'] = 'y';
$config['is_system_on'] = 'y';
$config['max_tmpl_revisions'] = '10';

$config['webmaster_name'] = 'STUDENTathleteWorld (SaWUSA)';
$config['webmaster_email'] = 'tim@studentathleteworld.com';

$config['gzip_output'] = 'y'; // serve compressed front-end pages for faster load times
$config['save_tmpl_files'] = 'y'; // save templates as files
$config['hidden_template_indicator'] = '.'; // hidden files start with .
$config['hidden_template_404'] = 'y'; // show a 404 if frontend loads a hidden template
$config['default_site_timezone'] = 'America/New_York';


// // Cookies
// $config['cookie_domain'] = '.example.com';
// $config['cookie_httponly'] = 'y';
// $config['cookie_path'] = '/';
// $config['cookie_prefix'] = 'clientSite';
// $config['cookie_secure'] = 'y';


// // Tracking settings
// $config['disable_all_tracking'] = 'y';
// $config['dynamic_tracking_disabling'] = 'y';
// $config['enable_entry_view_tracking'] = 'y';
// $config['enable_hit_tracking'] = 'y';
// $config['enable_online_user_tracking'] = 'y';

// // Caching
// $config['cache_driver'] = 'redis';
// $config['redis'] = array(
//   'host' => '127.0.0.1',
//   'password' => NULL,
//   'port' => 6379,
//   'timeout' => 0
// );

// $config['memcached'] = array(
//   array(
//     'host' => '192.168.1.100',
//     'port' => 11211,
//     'weight' => 2,
//   ),
//   array(
//     'host' => '192.168.1.101',
//     'port' => 11211,
//     'weight' => 1,
//   )
// );


// /*
// Hop Minifizer - https://hopstudios.com/software/hop_minifizer/docs
// */
// $config['hop_minifizer'] = array(
//     'cache_path' => $config['base_path'] . 'static_cache',
//     'cache_url' => $config['base_url'] . 'static_cache',
// );

// /*
// // Stash - https://github.com/croxton/Stash/wiki/Installing-and-upgrading
// */
// $config['stash_file_basepath'] = $config['base_path'] . 'stash_templates/'; // make sure this is a full and true basepath
// $config['stash_static_basepath'] = 'static_cache/';
// $config['stash_file_sync'] = false; // set to TRUE to sync stash embed file changes during development
// $config['stash_file_extensions'] = array('html', 'md', 'css', 'js', 'rss', 'xml');
// $config['stash_static_url'] = '/static_cache/'; // should be a relative url
// $config['stash_static_cache_enabled'] = false; // set to TRUE to enable static caching
// $config['stash_static_cache_index'] = true; // set to TRUE to use Stash as an index only when static caching (variable value not saved)
// $config['stash_static_character_blacklist'] = array('{', '}', '<', '>', ':', '"', '\\', '|', '*', '.');
// $config['stash_query_strings'] = false; // set to TRUE to cache query strings when referencing the current uri with @URI
// $config['stash_cookie'] = 'stashid'; // the stash cookie name
// $config['stash_cookie_expire'] = 0; // seconds - 0 means expire at end of session
// $config['stash_cookie_enabled'] = true; // set to FALSE if not using 'user' scope and cache pruning is disabled
// $config['stash_default_scope'] = 'local'; // default variable scope if not specified
// $config['stash_default_refresh'] = 0; // default cache refresh period in minutes
// $config['stash_limit_bots'] = false; // stop database writes by bots to reduce load on busy sites
// $config['stash_bots'] = array('bot', 'crawl', 'spider', 'archive', 'search', 'java', 'yahoo', 'teoma');
// $config['stash_prune_enabled'] = true; // set FALSE if you trigger pruning with a CRON/scheduled task (via Mustash API)
// $config['stash_prune_probability'] = 4;  // % chance that a request initiates cache pruning
// $config['stash_invalidation_period']  = 0;  // duration of cache invalidation in seconds
// $config['stash_parse_if_in']  = false;  // enable parsing of {if var IN (1|2|3)...{/if} style conditionals in Stash templates
// $config['stash_var_prefix'] = 'preview-'; // apply a prefix to all saved variables, useful for entry previews

// /*
// | CE Image - https://docs.causingeffect.com/expressionengine/ce-image/user-guide/configuration.html
// */
// $config['ce_image_cache_dir'] = '/images/made/'; // relative path to cache image directory
// $config['ce_image_document_root'] = '/chroot' . $_SERVER['DOCUMENT_ROOT']; // only use if you want images stored outside the default EE directory. If this is not Nexcess, remove chroot
// $config['ce_image_quality'] = 100; // default image quality
// /*
// | Amazon S3 settings and optional headers. For use with the optional AWS
// | extension. See the installation instructions for more details on how to install the extension.
// */
// $config['ce_image_aws_key'] = '';
// $config['ce_image_aws_secret_key'] = '';
// $config['ce_image_bucket'] = '';
// $config['ce_image_aws_request_headers'] = array();
// $config['ce_image_aws_request_headers']['Cache-Control'] = 'max-age=' . (30 * 24 * 60 * 60);
// $config['ce_image_aws_request_headers']['Expires'] = gmdate("D, d M Y H:i:s T", strtotime('+1 month'));
// $config['ce_image_aws_storage_class'] = 'STANDARD';


// /*
// | CE Cache - https://docs.causingeffect.com/expressionengine/ce-cache/user-guide/configuration.html
// */
// $config['ce_cache_off'] = 'no';

/*
| Speedy Cache - https://docs.boldminded.com/speedy/docs/configuration
*/
$config['speedy_enabled'] = 'yes';
$config['speedy_driver'] = 'file';
$config['speedy_static_enabled'] = 'no'; // Set this to yes to enable the static file cache
$config['speedy_static_path'] = $config['base_path'] . 'static_cache'; // The path to the static file cache directory
// define some url paths that are to be ignored when caching. Note that ignores can be valid regular expressions.
$config['speedy_static_settings'] = [
    'ignore_urls' => [
        ['url' => 'invalid/page'],
        ['url' => '^[a-z]{2}/products'],
        ['url' => '^products/.*'],
    ],
];


// Add global config variables here and use in templates like "{CONF_GOOGLE_ANALYTICS}"
// If you add more, start them with CONF_ for Config Variable
// $env_global['CONF_GOOGLE_ANALYTICS'] = 'UA-65722839-1';
