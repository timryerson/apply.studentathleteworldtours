<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Local config overrides & db credentials
 *
 * Our database credentials and any environment-specific overrides
 * This file should be specific to each developer and not tracked in Git
 *
 * @package    Focus Lab Master Config
 * @version    2.2.0
 * @author     Focus Lab, LLC <dev@focuslabllc.com>
 */

$config['database'] = array (
	'expressionengine' => array (
		'hostname' => 'mysql',
		'username' => 'root',
		'password' => 'root',
		'database' => 'apply.studentathleteworld',
		'dbdriver' => 'mysqli',
		'dbprefix' => 'exp_',
		'pconnect' => FALSE
	),
);

// Make sure we can login in case of weirdness with caching or cookies
$config['disable_csrf_protection'] = "y";
$config['cp_session_type'] = 'c';

// DEV: ignore what's in the stash database and use templates directly
$config['stash_file_sync'] = TRUE;

// DEV: turn off ce_cache
$config['ce_cache_off'] = 'yes';

// DEV: Turn off Speedy Cache
$config['speedy_enabled'] = 'no';
$config['allow_extensions'] = 'y';


$config['debug'] = '2';
$config['show_profiler'] = 'y';
// In order to save anything to log files, you’ll need to create the /system/user/logs/ directory and ensure it’s writable
$config['log_threshold'] = '4';




// Add config variables here and use in templates like "{CONF_GOOGLE_ANALYTICS}"
// If you add more, start them with CONF_ for Config Variable
// $env_global['CONF_GOOGLE_ANALYTICS'] = 'UA-65722839-1';

/* End of file config.local.php */
