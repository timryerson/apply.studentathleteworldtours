<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

global $env_global, $assign_to_config;
if ( ! defined('ENV')) {
    require 'config.env.php';
}

/*
|--------------------------------------------------------------------------
| ExpressionEngine Config Items
|--------------------------------------------------------------------------
|
| The following items are for use with ExpressionEngine.  The rest of
| the config items are for use with CodeIgniter, some of which are not
| observed by ExpressionEngine, e.g. 'permitted_uri_chars'
|
*/

$config['app_version'] = '7.3.11';
$config['debug'] = '1';
// $config['cp_url'] = 'https://apply.studentathleteworld.com/admin.php';
$config['doc_url'] = 'https://ellislab.com/expressionengine/user-guide/';
$config['is_system_on'] = 'y';
$config['allow_extensions'] = 'y';
$config['cache_driver'] = 'file';
// $config['database'] = array (
//   'expressionengine' => array (
//     'hostname' => '127.0.0.1',
//     'username' => 'root',
//     'password' => '',
//     'database' => 'applysaw_ee',
//     'dbdriver' => 'mysqli',
//     'dbprefix' => 'exp_',
//     'pconnect' => FALSE
//   ),
// );
$config['db_port'] = '';
$config['site_label'] = '';
$config['cookie_prefix'] = '';
$config['cookie_httponly'] = 'y';

$config['multiple_sites_enabled'] = 'n';
$config['enable_devlog_alerts'] = 'n';

$config['session_crypt_key'] = '3c73b616d22a10b58fa5994954eb44f9b5e48247';

$config['share_analytics'] = 'y';
$config['show_ee_news'] = 'y';
$config['allow_php'] = 'y';
$config['legacy_member_templates'] = 'y';
$config['site_license_key'] = '2a7bb2bbeb22c1b708f3de7e9e5dcb4c489f3c55';
$config['index_page'] = '';

// END EE config items










/*
|--------------------------------------------------------------------------
| Third Party Config
|--------------------------------------------------------------------------
*/
$config['simple_registration_no_other_registrations'] = TRUE;
$config['simple_registration_global_honeypot_field'] = 'screen_name';



/*
|--------------------------------------------------------------------------
| URI PROTOCOL
|--------------------------------------------------------------------------
|
| This item determines which server global should be used to retrieve the
| URI string.  The default setting of "AUTO" works for most servers.
| If your links do not seem to work, try one of the other delicious flavors:
|
| 'AUTO'			Default - auto detects
| 'PATH_INFO'		Uses the PATH_INFO
| 'QUERY_STRING'	Uses the QUERY_STRING
| 'REQUEST_URI'		Uses the REQUEST_URI
| 'ORIG_PATH_INFO'	Uses the ORIG_PATH_INFO
|
*/
$config['uri_protocol']	= 'AUTO';

/*
|--------------------------------------------------------------------------
| Default Character Set
|--------------------------------------------------------------------------
|
| This determines which character set is used by default in various methods
| that require a character set to be provided.
|
*/
$config['charset'] = 'UTF-8';


/*
|--------------------------------------------------------------------------
| Class Extension Prefix
|--------------------------------------------------------------------------
|
| This item allows you to set the filename/classname prefix when extending
| native libraries.  For more information please see the user guide:
|
| http://codeigniter.com/user_guide/general/core_classes.html
| http://codeigniter.com/user_guide/general/creating_libraries.html
|
*/
$config['subclass_prefix'] = 'EE_';

/*
|--------------------------------------------------------------------------
| Error Logging Threshold
|--------------------------------------------------------------------------
|
| If you have enabled error logging, you can set an error threshold to
| determine what gets logged. Threshold options are:
|
|	0 = Disables logging, Error logging TURNED OFF
|	1 = Error Messages (including PHP errors)
|	2 = Debug Messages
|	3 = Informational Messages
|	4 = All Messages
|
| For a live site you'll usually only enable Errors (1) to be logged otherwise
| your log files will fill up very fast.
|
*/
$config['log_threshold'] = 0;

/*
|--------------------------------------------------------------------------
| Date Format for Logs
|--------------------------------------------------------------------------
|
| Each item that is logged has an associated date. You can use PHP date
| codes to set your own date formatting
|
*/
$config['log_date_format'] = 'Y-m-d H:i:s';


/*
|--------------------------------------------------------------------------
| Encryption Key
|--------------------------------------------------------------------------
|
| If you use the Encryption class or the Sessions class with encryption
| enabled you MUST set an encryption key.  See the user guide for info.
|
*/

$config['encryption_key'] = 'DlRtUpmtWSgWywVsiqMUzLP75zqwz81e';


/*
|--------------------------------------------------------------------------
| Rewrite PHP Short Tags
|--------------------------------------------------------------------------
|
| If your PHP installation does not have short tag support enabled CI
| can rewrite the tags on-the-fly, enabling you to utilize that syntax
| in your view files.  Options are TRUE or FALSE (boolean)
|
*/
$config['rewrite_short_tags'] = TRUE;

require 'config.global.php';
require 'config.' . ENV . '.php';
if( ! isset($assign_to_config['global_vars'])) {
	$assign_to_config['global_vars'] = array();
}
$assign_to_config['global_vars'] = array_merge($assign_to_config['global_vars'], $env_global); // global var arrays
/* End of file config.php */
/* Location: ./system/expressionengine/config/config.php */