<?php

namespace Low\Alphabet\Conduit;

use Low\Alphabet\FluxCapacitor\Conduit\McpNav as FluxNav;

class McpNav extends FluxNav
{
    protected function defaultItems()
    {
        $this->setToolbarIcon();

        return array(
            'license' => 'License',
        );
    }
}
