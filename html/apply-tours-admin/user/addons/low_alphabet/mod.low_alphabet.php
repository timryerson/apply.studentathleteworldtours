<?php

if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

use Low\Alphabet\Library\Alpha;
use Low\Alphabet\Library\Base;
use Low\Alphabet\Library\Field;
use Low\Alphabet\Library\Multibyte;
use Low\Alphabet\Library\Param;
use Low\Alphabet\Library\Sieve;
/**
 * Low Alphabet Module class
 *
 * @package        low_alphabet
 * @author         Lodewijk Schutte <hi@gotolow.com>
 * @link           http://gotolow.com/addons/low-alphabet
 * @copyright      Copyright (c) 2019, Low
 */

include_once "addon.setup.php";
use Low\Alphabet\FluxCapacitor\Base\Mod;
class Low_alphabet extends Mod
{
    use Base;

    const DEFAULT_FIELD = 'title';
    const NUMBER_URL = '0-9';

// --------------------------------------------------------------------
    // PROPERTIES
    // --------------------------------------------------------------------

    /**
     * Extra numeric fields as defined in params
     *
     * @access      private
     * @var         array
     */


    private $numeric_fields = array();
/**
     * Sort orders for entries
     *
     * @access      private
     * @var         array
     */
    private $sort_order = array();
/**
     * Ranges used: ['a-f', 'g-l', 'm-r', 's-w', ...]
     *
     * @access      private
     * @var         array
     */
    private $ranges;
/**
     * Map to characters to ranges: ['a' => 'a-f', 'b' => 'a-f', 'k' => 'g-l', ...]
     *
     * @access      private
     * @var         array
     */
    private $map;
// --------------------------------------------------------------------
    // METHODS
    // --------------------------------------------------------------------

    /**
     * Constructor
     *
     * @access      public
     * @return      bool
     */
    public function __construct()
    {
        parent::__construct();
// Initialize base data for addon
        $this->initializeBaseData();
    }

    /**
     * Creates list from A to Z, filtered by parameters when given
     *
     * @access      public
     * @return      string
     */
    public function azlist()
    {
        // --------------------------------------
        // Prep no_results to avoid conflicts
        // --------------------------------------

        $this->_prep_no_results();
// --------------------------------------
        // Initiate some variables used later on
        // --------------------------------------

        $variables = $entries = $map = array();
// --------------------------------------
        // Check alpha_field parameter
        // --------------------------------------

        $alpha_field = ee()->TMPL->fetch_param('alpha_field', static::DEFAULT_FIELD);
// Validate and translate field name to 'field_id_x', where applicable
        if (! ($field = Field::column($alpha_field))) {
            $this->_log($alpha_field . ' is not a valid field');
            return ee()->TMPL->no_results();
        }

        $sieve = new Sieve();
        $sieve->select($field);
        $sieve->filter($field, '!=', '');
        $sieve->params(ee()->TMPL->tagparams);
        $sieve->params('site_id', implode('|', ee()->TMPL->site_ids));
        $query = $sieve->get();
// --------------------------------------
        // Set the ranges and the map
        // --------------------------------------

        $this->_set_ranges();
// --------------------------------------
        // Loop through results and populate entries array
        // so we get $entries['a'] = 123;
        // --------------------------------------

        foreach ($query as $entry) {
            $alpha = $entry->$field;
// Skip if field is empty
            if (empty($alpha)) {
                continue;
            }

            $title  = Alpha::clean($alpha, $this->_get_ignore_words());
            $letter = Alpha::first($title);
            if (array_key_exists($letter, $this->map)) {
                $entries[$this->map[$letter]][] = $title;
            }
        }

        // --------------------------------------
        // Are we showing empty ranges?
        // --------------------------------------

        $show_empty = (ee()->TMPL->fetch_param('show_empty') == 'yes');
// --------------------------------------
        // Is there a numbers label for 0-9?
        // --------------------------------------

        $numbers_label = ee()->TMPL->fetch_param('numbers_label', static::NUMBER_URL);
// --------------------------------------
        // Loop through the ranges and create vars
        // --------------------------------------

        foreach ($this->ranges as $a) {
// Skip these
            if (! $show_empty && ! array_key_exists($a, $entries)) {
                continue;
            }

            // Add letter to vars
            $variables[] = array(
                'low_alphabet_url'     => $a,
                'low_alphabet_label'   => ($a == static::NUMBER_URL) ? $numbers_label : Multibyte::strtoupper($a),
                'low_alphabet_entries' => array_key_exists($a, $entries) ? count($entries[$a]) : 0
            );
        }

        // --------------------------------------
        // Set return data to either parsed variables
        // or the {if no_results} code block
        // --------------------------------------

        $it = ($variables)
            ? ee()->TMPL->parse_variables(ee()->TMPL->tagdata, $variables)
            : ee()->TMPL->no_results();
// --------------------------------------
        // Like a movie, like a style
        // --------------------------------------

        return $it;
    }

    // --------------------------------------------------------------------

    /**
     * Parse channel entries, filtered by letter if given
     *
     * @access      public
     * @return      string
     */
    public function entries()
    {
        // --------------------------------------
        // Prep no_results to avoid conflicts
        // --------------------------------------

        $this->_prep_no_results();
// --------------------------------------
        // No entries? Return no_results
        // --------------------------------------

        if ($entry_ids = $this->_get_entry_ids()) {
// --------------------------------------
            // Set params
            // --------------------------------------

            $params = array(
                'fixed_order' => implode('|', $entry_ids),
                'orderby'     => '',
                'sort'        => ''
            );
// --------------------------------------
            // Auto limit?
            // --------------------------------------

            if (ee()->TMPL->fetch_param('auto_limit') == 'yes') {
                $params['limit'] = count($entry_ids);
            }

            // --------------------------------------
            // Call channel::entries
            // --------------------------------------

            return $this->_channel_entries($params);
        } else {
            return ee()->TMPL->no_results();
        }
    }

    // --------------------------------------------------------------------

    /**
     * Return pipe-delimited list of ordered entry_ids
     *
     * @access      public
     * @return      string
     */
    public function entry_ids()
    {
        // --------------------------------------
        // Get some parameters and check pair tag
        // --------------------------------------

        $pair       = ($tagdata = ee()->TMPL->tagdata) ? true : false;
        $no_results = ee()->TMPL->fetch_param('no_results');
        $separator  = ee()->TMPL->fetch_param('separator', '|');
// --------------------------------------
        // Create single string from entry ids
        // --------------------------------------

        $entry_ids = $this->_get_entry_ids();
        $entry_ids = empty($entry_ids) ? $no_results : implode($separator, $entry_ids);
// --------------------------------------
        // Parse+return or just return, depending on tag pair or not
        // --------------------------------------

        if ($pair) {
            return ee()->TMPL->parse_variables_row($tagdata, array(
                'low_alphabet:entry_ids' => $entry_ids
            ));
        } else {
            return $entry_ids;
        }
    }

    // --------------------------------------------------------------------

    /**
     * Return letter based on given text
     *
     * @access      public
     * @return      string
     */
    public function file_under()
    {
        // Set internal ranges and map
        $this->_set_ranges();
// Get the text, fallback to the tagdata
        $text = ee()->TMPL->fetch_param('text', ee()->TMPL->tagdata);
// Handle it
        $text = Alpha::clean($text, $this->_get_ignore_words());
// Get the first letter
        $text = Alpha::first($text);
// Get the group this text belongs to
        $group = array_key_exists($text, $this->map)
            ? $this->map[$text]
            : false;
// Bail out if group not found
        if (! $group) {
            return ee()->TMPL->no_results();
        }

        // Determine label for this group
        $label = ($group == static::NUMBER_URL)
            ? ee()->TMPL->fetch_param('numbers_label', static::NUMBER_URL)
            : Multibyte::strtoupper($group);
        if (count(ee()->TMPL->tagparts) == 3) {
            $out = ee()->TMPL->tagparts[2];
            return ($out == 'label') ? $label : $group;
        } else {
            return ee()->TMPL->parse_variables_row(ee()->TMPL->tagdata, array(
                    'low_alphabet_url'   => $group,
                    'low_alphabet_label' => $label
                ));
        }
    }

    // --------------------------------------------------------------------

    /**
     * To uppercase
     *
     * @access      public
     * @return      string
     */
    public function uppercase()
    {
        return $this->_case(ee()->TMPL->fetch_param('text', ''), 'upper');
    }

    // --------------------------------------------------------------------

    /**
     * To lowercase
     *
     * @access      public
     * @return      string
     */
    public function lowercase()
    {
        return $this->_case(ee()->TMPL->fetch_param('text', ''), 'lower');
    }

    // --------------------------------------------------------------------
    // PRIVATE METHODS
    // --------------------------------------------------------------------

    /**
     * Get ignore words
     */
    private function _get_ignore_words()
    {
        return array_filter(explode('|', ee()->TMPL->fetch_param('alpha_ignore')));
    }

    // --------------------------------------------------------------------

    /**
     * Call channel::entries
     *
     * @access     private
     * @param      array
     * @return     string
     */
    private function _channel_entries($params = array())
    {
        // --------------------------------------
        // Force given params
        // --------------------------------------

        ee()->TMPL->tagparams = array_merge(ee()->TMPL->tagparams, $params);
// --------------------------------------
        // Make sure the following params are set
        // --------------------------------------

        $set_params = array(
            'dynamic'  => 'no',
            'paginate' => 'bottom'
        );
        foreach ($set_params as $key => $val) {
            if (! ee()->TMPL->fetch_param($key)) {
                ee()->TMPL->tagparams[$key] = $val;
            }
        }

        // --------------------------------------
        // Get channel module
        // --------------------------------------

        if (! class_exists('Channel')) {
            require_once PATH_MOD . 'channel/mod.channel.php';
        }

        // --------------------------------------
        // Create new Channel instance
        // --------------------------------------

        $channel = new \Channel();
// --------------------------------------
        // Let the Channel module do all the heavy lifting
        // --------------------------------------

        $this->_log('Calling the Channel module');
        return $channel->entries();
    }

    // --------------------------------------------------------------------

    /**
     * Get ordered entry ids
     *
     * @access     private
     * @return     array
     */
    private function _get_entry_ids()
    {
        // --------------------------------------
        // Check numeric fields
        // --------------------------------------

        if ($numeric_fields = ee()->TMPL->fetch_param('numeric_fields')) {
            list($numeric_fields, $in) = Param::explode($numeric_fields);
// Get proper field names
            $numeric_fields = array_map(array(new Field(), 'name'), $numeric_fields);
// Add to class property
            $this->numeric_fields = array_filter($numeric_fields);
        }

        // --------------------------------------
        // Get alpha_filter parameter
        // --------------------------------------

        $alpha_filter = ee()->TMPL->fetch_param('alpha_filter');
// --------------------------------------
        // Turn 'yes' and 'no' back to 'y' and 'n'
        // --------------------------------------

        if (in_array($alpha_filter, array('yes', 'no'))) {
            $alpha_filter = substr($alpha_filter, 0, 1);
        }

        // --------------------------------------
        // Check if alpha filter does not match a known filter, set it to FALSE
        // --------------------------------------

        if (! (preg_match('/^[a-z0-9](\-[a-z0-9])?$/', $alpha_filter))) {
            $alpha_filter = false;
        }

        // --------------------------------------
        // Get numbers parameter
        // --------------------------------------

        $numbers = ee()->TMPL->fetch_param('numbers', 'before');
// --------------------------------------
        // Check orderby parameter
        // --------------------------------------

        $orderby = array();
        if ($val = ee()->TMPL->fetch_param('orderby')) {
            foreach (explode('|', $val) as $field) {
                if ($name = Field::column($field)) {
                    $orderby[] = $name;
                }
            }
        }

        if (empty($orderby)) {
            $orderby[] = static::DEFAULT_FIELD;
        }

        // --------------------------------------
        // Check sort parameter
        // --------------------------------------

        $sort = ($val = ee()->TMPL->fetch_param('sort'))
            ? explode('|', $val)
            : array('asc');
// --------------------------------------
        // Get alpha_field
        // --------------------------------------

        $alpha_field = ($val = ee()->TMPL->fetch_param('alpha_field'))
            ? Field::column($val)
            : false;
// Nothing? Default to the first orderby field
        if (! $alpha_field) {
            $alpha_field = $orderby[0];
        }

        // --------------------------------------
        // Combine the orderby and sort arrays
        // --------------------------------------

        if (count($orderby) > count($sort)) {
            $sort = array_pad($sort, count($orderby), 'asc');
        } elseif (count($orderby) < count($sort)) {
            $sort = array_slice($sort, 0, count($orderby));
        }

        $this->sort_order = array_combine($orderby, $sort);
        unset($orderby, $sort);
// --------------------------------------
        // Initiate entries
        // --------------------------------------

        $entries = array();
// --------------------------------------
        // Get all titles and handle those; there
        // might be accents, punctuation or entities
        // present that we need to filter out.
        // --------------------------------------

        $this->_log('Getting entries from DB');
        $sieve = new Sieve();
//$sieve->select(array_keys($this->sort_order));
        $sieve->params(ee()->TMPL->tagparams);
        $sieve->params('site_id', implode('|', ee()->TMPL->site_ids));
        $query = $sieve->get();
// --------------------------------------
        // Set groups and map
        // --------------------------------------

        $this->_set_ranges();
// --------------------------------------
        // Loop through results, add entry ids to array
        // --------------------------------------

        $this->_log('Mapping entries according to alpha groups');
        foreach ($query as $entry) {
            $row = $entry->toArray();
        // Keep track of flag whether to add the result to the entry ids
            $add = true;
            foreach (array_keys($this->sort_order) as $field) {
        // Only prep field if it's not a numeric field
                if (! Field::isNumeric($field)) {
// Clean the alpha fields
                    $row[$field] = Alpha::clean($row[$field], $this->_get_ignore_words());
                }
            }

            // Get first char
            $first = Alpha::first($row[$alpha_field]);
        // If we have a char to filter by, check it
            if ($alpha_filter !== false) {
                if (! isset($this->map[$first]) || $this->map[$first] != $alpha_filter) {
                    $add = false;
                }
            }

            // Add entry to entry_ids, and keep alpha field
            if ($add === true) {
                $entry_id = $row['entry_id'];
                foreach (array_keys($row) as $key) {
                    if (! in_array($key, array_keys($this->sort_order))) {
                        unset($row[$key]);
                    }
                }

                $entries[$entry_id] = $row;
            }
        }

        // --------------------------------------
        // Check existing entry_id parameter
        // --------------------------------------

        if ($entry_ids = ee()->TMPL->fetch_param('entry_id')) {
// Get array of found ids by getting the keys (gross)
            $gross_ids = array_keys($entries);
// Determine net IDs
            $net_ids = Param::merge($gross_ids, $entry_ids);
// Loop through these and unset non-existent ones
            foreach (array_diff($gross_ids, $net_ids) as $id) {
                unset($entries[$id]);
            }
        }

        // --------------------------------------
        // Sort the entries
        // --------------------------------------

        $this->_log('Sorting entries by alpha');
        uasort($entries, array($this, '_multi_alpha_sort'));
// --------------------------------------
        // Put numbers at the end of the query array
        // --------------------------------------

        if ($numbers == 'after') {
// Initiate letters and numbers array
            $letters = $numbers = array();
// Loop through entries
            foreach ($entries as $id => $row) {
                if (is_numeric(substr($row[$alpha_field], 0, 1))) {
                    $numbers[] = $id;
                } else {
                    $letters[] = $id;
                }
            }

            // merge letters and numbers, so numbers are at the end
            $entry_ids = array_merge($letters, $numbers);
        } else {
        // Regular order, just give me the keys, thank you
            $entry_ids = array_keys($entries);
        }

        // --------------------------------------
        // Add entries to cache, so the extension can use them
        // --------------------------------------

        ee()->session->set_cache($this->package, 'entries', $entries);
        $this->_log('Returning entries: <code>' . implode('|', $entry_ids) . '</code>');
        return $entry_ids;
    }

    // --------------------------------------------------------------------

    /**
     * Check for {if low_alphabet_no_results}
     *
     * @access      private
     * @return      void
     */
    private function _prep_no_results()
    {
        // Shortcut to tagdata
        $td =& ee()->TMPL->tagdata;
        $open = 'if ' . $this->package . '_no_results';
        $close = '/if';
// Check if there is a custom no_results conditional
        if (strpos($td, $open) !== false && preg_match('#' . LD . $open . RD . '(.*?)' . LD . $close . RD . '#s', $td, $match)) {
            $this->_log("Prepping {$open} conditional");
// Check if there are conditionals inside of that
            if (stristr($match[1], LD . 'if')) {
                $match[0] = ee()->functions->full_tag($match[0], $td, LD . 'if', LD . '\/if' . RD);
            }

            // Set template's no_results data to found chunk
            ee()->TMPL->no_results = substr($match[0], strlen(LD . $open . RD), -strlen(LD . $close . RD));
// Remove no_results conditional from tagdata
            $td = str_replace($match[0], '', $td);
        }
    }

    // --------------------------------------------------------------------

    /**
     * Return upper/lower case
     *
     * @access      private
     * @param       string
     * @param       string
     * @return      string
     */
    private function _case($str, $to = 'upper')
    {
        // Override for numbers label
        if ($str == static::NUMBER_URL) {
            return ee()->TMPL->fetch_param('numbers_label', $str);
        }

        // Compose method
        $method = 'strto' . $to;
// Strip to first letter, only if word="yes"
        if (! (ee()->TMPL->fetch_param('word', 'no') == 'yes')) {
            $str = Multibyte::substr($str, 0, 1);
        }

        // Return modified string
        return Multibyte::$method($str);
    }

    // --------------------------------------------------------------------

    /**
     * Set internal ranges and map based on params
     *
     * @access      private
     * @param       string
     * @return      string
     */
    private function _set_ranges()
    {
        // --------------------------------------
        // Reset
        // --------------------------------------

        $this->ranges = $this->map = array();
// --------------------------------------
        // Define the ranges based on groups or default
        // --------------------------------------

        if ($groups = ee()->TMPL->fetch_param('alpha_groups')) {
// Do it manually
            $this->ranges = explode('|', $groups);
        } else {
        // Default per letter
            $letters = range('a', 'z');
            $numbers = (ee()->TMPL->fetch_param('group_numbers') == 'yes')
                ? array(static::NUMBER_URL)
                : range(0, 9);
        // Add numbers to the ranges?
            switch (ee()->TMPL->fetch_param('numbers', 'before')) {
                case 'before':
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    $this->ranges = array_merge($numbers, $letters);

                    break;
                case 'after':
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    $this->ranges = array_merge($letters, $numbers);

                    break;
                default:
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    $this->ranges = $letters;
            }
        }

        // --------------------------------------
        // Create map from ranges so we know which letter/number belongs to which range/group
        // --------------------------------------

        foreach ($this->ranges as $range) {
            if (strlen($range) === 1) {
            // Single letter
                $this->map[$range] = $range;
            } elseif (preg_match('/^[a-z0-9]\-[a-z0-9]$/', $range)) {
            // Range of letters: map each one
                list($from, $to) = explode('-', $range);
                foreach (range($from, $to) as $x) {
                    $this->map[$x] = $range;
                }
            }
        }
    }

    // --------------------------------------------------------------------

    /**
     * Sorts the array of entries based on the sort order set in the class - used by uasort()
     *
     * @access      private
     * @param       array
     * @param       array
     * @return      int
     */
    private function _multi_alpha_sort($a, $b)
    {
        foreach ($this->sort_order as $key => $sort) {
            if (Field::isNumeric($key) || in_array($key, $this->numeric_fields)) {
                if ($a[$key] < $b[$key]) {
                    return ($sort == 'asc') ? -1 : 1;
                } elseif ($a[$key] > $b[$key]) {
                    return ($sort == 'asc') ? 1 : -1;
                } else {
                    continue;
                }
            } else {
                if (strnatcasecmp($a[$key], $b[$key]) < 0) {
                    return ($sort == 'asc') ? -1 : 1;
                } elseif (strnatcasecmp($a[$key], $b[$key]) > 0) {
                    return ($sort == 'asc') ? 1 : -1;
                } else {
                    continue;
                }
            }
        }

        return 0;
    }

    // --------------------------------------------------------------------

    /**
     * Log template item
     */
    private function _log($msg)
    {
        ee()->TMPL->log_item($this->info->getName() . ': ' . $msg);
    }
}
// END CLASS

/* End of file mod.low_alphabet.php */
