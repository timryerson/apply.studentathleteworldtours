<?php

/**
 * Low Alphabet Add-On Setup file for EE4/5
 *
 * @package        low_alphabet
 * @author         Lodewijk Schutte <hi@gotolow.com>
 * @link           http://gotolow.com/addons/low-alphabet
 * @copyright      Copyright (c) 2019, Low
 */

require_once 'autoload.php';
$addonJson = json_decode(file_get_contents(__DIR__ . '/addon.json'));

return array(
    'name'           => $addonJson->name,
    'description'    => $addonJson->description,
    'version'        => $addonJson->version,
    'namespace'      => $addonJson->namespace,
    'author'         => 'EEHarbor',
    'author_url'     => 'https://eeharbor.com/',
    'docs_url'       => 'https://eeharbor.com/low-alphabet',
    'settings_exist' => true
);
