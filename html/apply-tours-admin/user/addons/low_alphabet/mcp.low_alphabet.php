<?php

if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Low Alphabet MCP class (unused)
 *
 * @package        low_alphabet
 * @author         Lodewijk Schutte <hi@gotolow.com>
 * @link           http://gotolow.com/addons/low-alphabet
 * @copyright      Copyright (c) 2019, Low
 */

include_once "addon.setup.php";
use Low\Alphabet\FluxCapacitor\Base\Mcp;

class Low_alphabet_mcp extends Mcp
{
}
