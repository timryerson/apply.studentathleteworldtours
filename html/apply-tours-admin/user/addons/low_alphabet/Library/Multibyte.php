<?php

namespace Low\Alphabet\Library;

/**
 * Low Multibyte class, for optional multibyte support
 *
 * @package        low_alphabet
 * @author         Lodewijk Schutte ~ Low <hi@gotolow.com>
 * @link           http://gotolow.com/addons/low-alphabet
 * @copyright      Copyright (c) 2019, Low
 */
class Multibyte
{

    /**
     * Magic Call Method
     *
     * @access     public
     * @param      string
     * @param      array
     * @return     mixed
     */
    public static function __callStatic($fn, $args)
    {
        // Multibyte function
        $mbfn = 'mb_' . $fn;

        // Call which one?
        $call = function_exists($mbfn) ? $mbfn : $fn;

        // Make sure the fallback exists, too
        if (function_exists($call)) {
            // Execute
            return call_user_func_array($call, $args);
        } else {
            // Or throw error
            throw new \Exception("Function {$call}() does not exist.");
        }
    }
}
// End of file Multibyte.php
