<?php

namespace Low\Alphabet\Library;

/**
 * Low Alphabet Alpha Class
 *
 * @package        low_alphabet
 * @author         Lodewijk Schutte <hi@gotolow.com>
 * @link           http://gotolow.com/addons/low-alphabet
 * @copyright      Copyright (c) 2019, Low
 */

class Alpha
{

    /**
     * Get utf-8 character from ascii integer
     *
     * @access     public
     * @param      int
     * @return     string
     */
    public static function chr($int)
    {
        return html_entity_decode("&#{$int};", ENT_QUOTES, 'UTF-8');
    }

    /**
     * Strip string from unwanted chars for better sorting
     *
     * @param      string    String to clean up
     * @param      array     Words to ignore at beginning of said string
     * @return     string
     */
    public static function clean($str, $ignore = array())
    {
        static $chars = array();

        // --------------------------------------
        // Get translation array from native foreign_chars.php file
        // --------------------------------------

        if (! $chars) {
            $foreign_characters = static::getForeignChars();

            if (is_array($foreign_characters)) {
                foreach ($foreign_characters as $k => $v) {
                    $chars[static::chr($k)] = $v;
                }
            }

            // Punctuation characters and misc ascii symbols
            $punct = array(
                33,34,35,38,39,40,41,42,43,44,45,46,47,58,59,60,62,63,64,
                91,92,93,94,123,124,125,126,161,162,163,164,165,166,167,
                168,169,170,171,172,174,175,176,177,178,179,180,181,182,
                183,184,185,186,187,188,189,190,191,215,402,710,732,
                8211,8212,8213,8216,8217,8218,8220,8221,8222,8224,8225,
                8226,8227,8230,8240,8242,8243,8249,8250,8252,8254,8260,
                8364,8482,8592,8593,8594,8595,8596,8629,8656,8657,8658,
                8659,8660,8704,8706,8707,8709,8711,8712,8713,8715,8719,
                8721,8722,8727,8730,8733,8734,8736,8743,8744,8745,8746,
                8747,8756,8764,8773,8776,8800,8801,8804,8805,8834,8835,
                8836,8838,8839,8853,8855,8869,8901,8968,8969,8970,8971,
                9001,9002,9674,9824,9827,9829,9830
            );

            // Add punctuation characters to chars array
            foreach ($punct as $k) {
                $chars[static::chr($k)] = ' ';
            }
        }

        // --------------------------------------
        // Get rid of tags
        // --------------------------------------

        $str = strip_tags($str);

        // --------------------------------------
        // Get rid of entities
        // --------------------------------------

        $str = html_entity_decode($str, ENT_QUOTES, 'UTF-8');

        // --------------------------------------
        // Lowercase it
        // --------------------------------------

        $str = Multibyte::strtolower($str);

        // --------------------------------------
        // Replace accented chars with unaccented versions
        // --------------------------------------

        if ($chars) {
            $str = strtr($str, $chars);
        }

        // --------------------------------------
        // Strip out words to ignore
        // --------------------------------------

        if ($ignore) {
            // Escape values in ignore array
            $ignore = array_map('preg_quote', $ignore);

            // Strip them off if they're followed by white space
            $str = preg_replace('/^(' . implode('|', $ignore) . ')\s/iu', '', trim($str));
        }

        // --------------------------------------
        // Get rid of non-alphanumeric chars
        // --------------------------------------

        $str = preg_replace('/[^\p{L}\p{N}\s]+/u', '', $str);

        // --------------------------------------
        // Return trimmed
        // --------------------------------------

        $str = trim($str);

        return $str;
    }

    /**
     * Get first char
     *
     * @access     public
     * @param      int
     * @return     string
     */
    public static function first($str)
    {
        $str = Multibyte::strtolower($str);
        return $str[0];
    }

    /**
     * Get foreign characters
     */
    private static function getForeignChars()
    {
        if (version_compare(APP_VER, '3.4.0', '>=')) {
            return ee()->config->loadFile('foreign_chars');
        }

        if (file_exists(APPPATH . 'config/foreign_chars.php')) {
            // This contains a map of accented character numbers and their translations
            include APPPATH . 'config/foreign_chars.php';

            return $foreign_characters;
        }
    }
}
