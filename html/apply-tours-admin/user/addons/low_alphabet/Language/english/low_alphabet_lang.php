<?php

/**
 * Low Alphabet Language file
 *
 * @package        low_alphabet
 * @author         Lodewijk Schutte <hi@gotolow.com>
 * @link           http://gotolow.com/addons/low-alphabet
 * @copyright      Copyright (c) 2019, Low
 */

$lang = array('low_alphabet_module_name' => 'Low Alphabet');
