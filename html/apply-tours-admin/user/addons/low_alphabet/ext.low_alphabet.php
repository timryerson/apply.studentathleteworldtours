<?php

if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

use Low\Alphabet\Library\Alpha;
use Low\Alphabet\Library\Base;
use Low\Alphabet\Library\Multibyte;
/**
 * Low Alphabet extension class
 *
 * @package         low_alphabet
 * @author          Lodewijk Schutte ~ Low <hi@gotolow.com>
 * @copyright       Copyright (c) 2019, Lodewijk Schutte
 * @link            http://gotolow.com/addons/low-alphabet
 */

include_once "addon.setup.php";
use Low\Alphabet\FluxCapacitor\Base\Ext;
class Low_alphabet_ext extends Ext
{
    use Base;

    const DEFAULT_FIELD = 'title';
    const NUMBER_URL = '0-9';

    // --------------------------------------------------------------------
    // METHODS
    // --------------------------------------------------------------------

    /**
     * Constructor
     *
     * @access      public
     * @return      bool
     */
    public function __construct()
    {
        parent::__construct();

        // Initialize base data for addon
        $this->initializeBaseData();
    }

    /**
     *
     * @param       object    Current Channel object
     * @param       array     DB result set
     * @return      array
     */
    public function channel_entries_query_result($obj, $query)
    {
        // -------------------------------------------
        // Get the latest version of $query
        // -------------------------------------------

        if (ee()->extensions->last_call !== false) {
            $query = ee()->extensions->last_call;
        }

        // --------------------------------------
        // Don't do anything if the tag that called it isn't our package
        // --------------------------------------

        if (ee()->TMPL->tagparts[0] != $this->package) {
            return $query;
        }

        // --------------------------------------
        // Get entries from cache
        // --------------------------------------

        $entries = ee()->session->cache($this->package, 'entries');

        // --------------------------------------
        // Loop through query result to set alpha title
        // --------------------------------------

        foreach ($query as $i => &$row) {
            // Fetch field from cache
            $entry = (array) @$entries[$row['entry_id']];

            // Clean up the field
            $row['low_alphabet_field'] = implode(' ', $entry);

            // Set the letter
            $row['low_alphabet_letter'] = Alpha::first($row['low_alphabet_field']);
        }

        // --------------------------------------
        // Are we grouping numbers?
        // --------------------------------------
        $group_numbers = (ee()->TMPL->fetch_param('group_numbers') == 'yes');

        // --------------------------------------
        // Do we have a custom label for grouped numbers?
        // --------------------------------------
        $numbers_label = ee()->TMPL->fetch_param('numbers_label', static::NUMBER_URL);

        // --------------------------------------
        // Heading / Footer vars for conditionals
        // First, let's keep track of last letter encountered
        // --------------------------------------
        $last_letter = false;

        // --------------------------------------
        // Loop through query again to set heading and footer vars
        // --------------------------------------
        foreach ($query as $i => &$row) {
            // Initiate heading and footer vars to empty string
            $row['low_alphabet_heading'] = $row['low_alphabet_footer'] = '';

            // Set current letter
            $current_letter = $row['low_alphabet_letter'];

            // Set next letter
            $next_letter = (isset($query[$i + 1])) ? $query[$i + 1]['low_alphabet_letter'] : false;

            // Set numeric letters to the same number if group
            if ($group_numbers) {
                if (is_numeric($current_letter)) {
                    $current_letter = static::NUMBER_URL;
                }
                if (is_numeric($next_letter)) {
                    $next_letter = static::NUMBER_URL;
                }
            }

            // If next letter differs from the current letter,
            // set footer to y
            if ($current_letter != $next_letter || $next_letter === false) {
                $row['low_alphabet_footer'] = 'y';
            }

            // If previous letter differs from the current letter,
            // set header to y
            if ($last_letter != $current_letter) {
                $row['low_alphabet_heading'] = 'y';
                $last_letter = ($group_numbers && is_numeric($current_letter)) ? static::NUMBER_URL : $current_letter;
            }

            // Save letter / label in current row
            $row['low_alphabet_url']   = $current_letter;
            $row['low_alphabet_label'] = ($current_letter == static::NUMBER_URL) ? $numbers_label : Multibyte::strtoupper($current_letter);

            // Unset the letter
            unset($row['low_alphabet_letter']);
        }

        // --------------------------------------
        // Return modified query array
        // --------------------------------------

        return $query;
    }
}
// END CLASS

/* End of file ext.low_alphabet.php */
