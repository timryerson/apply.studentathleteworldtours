<?php

if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Low Alphabet UPD class
 *
 * @package        low_alphabet
 * @author         Lodewijk Schutte <hi@gotolow.com>
 * @link           http://gotolow.com/addons/low-alphabet
 * @copyright      Copyright (c) 2019, Low
 */

include_once "addon.setup.php";
use Low\Alphabet\FluxCapacitor\Base\Upd;
use Low\Alphabet\Library\Base;

class Low_alphabet_upd extends Upd
{
    use Base;

    const DEFAULT_FIELD = 'title';
    const NUMBER_URL = '0-9';

    // --------------------------------------------------------------------
    // PROPERTIES
    // --------------------------------------------------------------------

    /**
     * Actions
     *
     * @var        array
     * @access     private
     */
    private $actions = array();

    /**
     * Extension hooks
     *
     * @var        array
     * @access     private
     */
    private $hooks = array(
        'channel_entries_query_result'
    );

    /**
     * Extension hooks
     *
     * @var        array
     * @access     private
     */
    private $className;

    // --------------------------------------------------------------------
    // METHODS
    // --------------------------------------------------------------------

    /**
     * Constructor
     *
     * @access      public
     * @return      bool
     */
    public function __construct()
    {
        // Call the parent for most
        parent::__construct();

        // Initialize base data for addon
        $this->initializeBaseData();

        // Set className
        $this->className = ucfirst($this->package);
    }

    // --------------------------------------------------------------------

    /**
     * Install the module
     *
     * @access      public
     * @return      bool
     */
    public function install()
    {
        // --------------------------------------
        // Add row to modules table
        // --------------------------------------

        ee()->db->insert('modules', array(
            'module_name'    => $this->className,
            'module_version' => $this->version,
            'has_cp_backend' => 'n'
        ));

        // --------------------------------------
        // Add actions
        // --------------------------------------

        foreach ($this->actions as $action) {
            $this->addAction($action);
        }

        // --------------------------------------
        // Add hooks
        // --------------------------------------

        foreach ($this->hooks as $hook) {
            $this->addHook($hook);
        }

        return true;
    }

    // --------------------------------------------------------------------

    /**
     * Uninstall the module
     *
     * @return  bool
     */
    public function uninstall()
    {
        // --------------------------------------
        // get module id
        // --------------------------------------

        $query = ee()->db
            ->select('module_id')
            ->from('modules')
            ->where('module_name', $this->className)
            ->get();

        // --------------------------------------
        // remove references from module_member_groups
        // --------------------------------------

        ee()->db->where('module_id', $query->row('module_id'));
        ee()->db->delete(version_compare(APP_VER, '6.0', '>=') ? 'module_member_roles' : 'module_member_groups');

        // --------------------------------------
        // remove references from modules
        // --------------------------------------

        ee()->db->where('module_name', $this->className);
        ee()->db->delete('modules');

        // --------------------------------------
        // remove references from actions
        // --------------------------------------

        ee()->db->where_in('class', array($this->className, $this->className . '_mcp'));
        ee()->db->delete('actions');

        // --------------------------------------
        // remove references from extensions
        // --------------------------------------

        ee()->db->where('class', $this->className . '_ext');
        ee()->db->delete('extensions');

        return true;
    }

    // --------------------------------------------------------------------

    /**
     * Update the module
     *
     * @return  bool
     */
    public function update($current = '')
    {
        // -------------------------------------
        //  Same version? A-okay, daddy-o!
        // -------------------------------------

        if ($current == '' or version_compare($current, $this->version) === 0) {
            return false;
        }

        // Extension data
        $ext_data = array('version' => $this->version);

        // Update the extension and fieldtype in the DB
        ee()->db->update('extensions', $ext_data, "class = '{$this->className}_ext'");

        // Return TRUE to update version number in DB
        return true;
    }

    // --------------------------------------------------------------------

    /**
     * Add action
     *
     * @access     private
     * @param      array
     * @return     void
     */
    private function addAction($action)
    {
        list($class, $method) = $action;

        ee()->db->insert('actions', array(
            'class'  => $class,
            'method' => $method
        ));
    }

    // --------------------------------------------------------------------

    /**
     * Add extension hook
     *
     * @access     private
     * @param      string
     * @return     void
     */
    private function addHook($name)
    {
        ee()->db->insert(
            'extensions',
            array(
                'class'    => $this->className . '_ext',
                'method'   => $name,
                'hook'     => $name,
                'settings' => '',
                'priority' => 5,
                'version'  => $this->version,
                'enabled'  => 'y'
            )
        );
    }

    // --------------------------------------------------------------------
}

/* End of file upd.low_alphabet.php */
