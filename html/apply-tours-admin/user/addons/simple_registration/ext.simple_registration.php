<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!class_exists('Ab_ExtBase')) {
    require_once PATH_THIRD.'simple_registration/libraries/ab/ab_extbase.php';
}

require_once PATH_THIRD.'simple_registration/const.simple_registration.php';


class Simple_registration_ext extends Ab_ExtBase
{
    public $name            = 'Simple Registration';
    public $version         = Const_Simple_Registration::CURRENT_VERSION;
    public $description     = 'Enables you to remove otherwise required information from the EE signup form.';
    public $settings_exist  = 'n';
    public $docs_url        = 'http://wedoaddons.com/addon/simple-registration';

    const TRIGGER_SIGNUP = 'on_signup';
    const TRIGGER_ACTIVATION = 'on_activation';

    private $new_member_id = FALSE; // member_id of newly registered user

	/**
	 * Settings
	 */
	function settings()
	{
	    $settings = array();
	    return $settings;
	}

	function activate_extension()
	{
		ee()->load->dbforge();

		$register_hooks = array(
			'member_member_register_start' => 'on_member_register_start',
            'member_member_register' => 'on_member_register',
            'member_register_validate_members' => 'on_member_register_validate_members',
		);

		$class_name = get_class($this);
		foreach($register_hooks as $hook => $method)
		{
			$data = array(
				'class'        => $class_name,
				'method'       => $method,
				'hook'         => $hook,
				'settings'     => "",
				'priority'     => 10,
				'version'      => $this->version,
				'enabled'      => "y"
			);
			ee()->db->insert('extensions', $data);
		}

	}

    function on_member_register_validate_members($member_id)
    {
        if(!is_array($member_id))
        {
            $member_id = intval($member_id);
            if($member_id > 0)
            {
                // check if we have any pending signup key
                ee()->db->from('simple_registration_pending_events ev, simple_registration_signup_keys k');
                ee()->db->where('k.simple_registration_signup_key_id','ev.signup_key_id', FALSE);
                ee()->db->where('member_id', $member_id);
                $q = ee()->db->get();

                if($q->num_rows() > 0)
                {
                    foreach($q->result() as $pending_event)
                    {
                        if (ee()->extensions->active_hook('simple_registration_signup_key_triggered') === TRUE)
                        {
                            ee()->extensions->call('simple_registration_signup_key_triggered', $pending_event->member_id, $pending_event->signup_key, 'activation', $this);
                            if (ee()->extensions->end_script === TRUE) return;
                        }

                        ee()->db->where('simple_registration_pending_event_id', $pending_event->simple_registration_pending_event_id);
                        ee()->db->delete('simple_registration_pending_events');

                        // member group
                        $member_group_id = $pending_event->member_group_id;
                        ee()->db->where('member_id', $member_id);
                        ee()->db->update('members', array('group_id' => $member_group_id));
                    }
                }
            }
        }
    }

    /**
	 * This hook is called before member is registered.
	 */
	function on_member_register_start()
	{
        ee()->load->config('simple_registration');
        ee()->lang->loadfile('simple_registration');
        ee()->load->library('srlib');

        if(ee()->input->post('simple_registration') != 'y')
        {
            if(ee()->config->item('simple_registration_no_other_registrations'))
            {
                show_error(ee()->lang->line('sr_no_outher_registrations_error'));
            }
            else
            {
                return;
            }
        }

        ee()->load->library('abmembers');
        ee()->load->library('abprefs', array('module_name' => 'simple_registration')); // can't load this in constructor due to EE bug #12479

        $require_at_least_one_signup_key = ee()->abprefs->get('require_at_least_one_signup_key');
        if($require_at_least_one_signup_key) {
            // a signup key is required, so check that we're getting one
            $found_valid_signup_key = FALSE;
            $signup_key = ee()->input->post('signup_key');
            if($signup_key) {
                $found_valid_signup_key = ee()->srlib->get_signup_key_info($signup_key);
            }

            if(!$found_valid_signup_key) {
                $data = array(	'title' 	=> ee()->lang->line('simple_registration'),
                    'heading'	=> ee()->lang->line('error'),
                    'content'	=> ee()->lang->line('sr_valid_signup_key_required_err'),
                    'link'		=> array(ee()->functions->fetch_site_index(), stripslashes(ee()->config->item('site_name')))
                );

                ee()->output->show_message($data);
            }
        }

        // check honeypot
        $honeypot = ee()->config->item('simple_registration_global_honeypot_field');
        if(!$honeypot) {
            $honeypot = ee()->input->post('simple_registration_hp');
        }

        if($honeypot)
        {
            if(ee()->input->post($honeypot) != '')
            {
                $honeypot_info = array(
                    'ip_address' => ee()->input->ip_address(),
                    'email' => ee()->input->post('email'),
                    'username' => ee()->input->post('username'),
                    'screen_name' => ee()->input->post('screen_name'),
                    'honeypot' => ee()->input->post($honeypot),
                    'honeypot_key' => $honeypot,
                );

                if (ee()->extensions->active_hook('simple_registration_honeypot_hit') === TRUE)
                {
                    ee()->extensions->call('simple_registration_honeypot_hit', $honeypot_info, $this);
                    if (ee()->extensions->end_script === TRUE) return;
                }

                $data = array(	'title' 	=> ee()->lang->line('simple_registration'),
                                'heading'	=> ee()->lang->line('error'),
                                'content'	=> ee()->lang->line('sr_honeypot_err'),
                                'link'		=> array(ee()->functions->fetch_site_index(), stripslashes(ee()->config->item('site_name')))
                             );

                ee()->output->show_message($data);
            }
        }

        // after we've checked the honeypot value we check any of the rewrites
        foreach(ee()->config->item('simple_registration_input_name_rewrites') as $real_key => $rewritten_key) {
            if(isset($_POST[$rewritten_key])) {
                $_POST[$real_key] = $_POST[$rewritten_key];
            }
        }

        if(ee()->abprefs->get('auto_generate_password'))
        {
            $_POST['password'] = ee()->srlib->get_random_password();
        }

        if(ee()->abprefs->get('use_email_for_username'))
        {
            $_POST['username'] = ee()->input->post('email');

            if($_POST['username'] == '')
            {
                // if we ever get here email is empty. email can never be empty.
                // to hide the "you must enter a username" message we just set the username to some random garbage
                $_POST['username'] = md5(ee()->srlib->get_random_password());
            }
        }

        if(ee()->abprefs->get('username_equals_screen_name'))
        {
            $username = ee()->input->post('username');
            $_POST['screen_name'] = $username ? $username : md5(time());    // just hide the screen name error if username is empty
        }

        if(ee()->abprefs->get('do_not_require_email_confirmation'))
        {
            $_POST['email_confirm'] = ee()->input->post('email');
        }

        if(ee()->abprefs->get('do_not_require_password_confirmation'))
        {
            $_POST['password_confirm'] = ee()->input->post('password');
        }

        if(ee()->abprefs->get('do_not_require_accept_terms'))
        {
            $_POST['accept_terms'] = 'y';
        }

        /**
         * Handle custom member fields
         */
        $custom_member_fields = ee()->abmembers->get_member_fields();
        foreach($custom_member_fields as $field_name => $field_col_name)
        {
            if(isset($_POST[$field_name]))
            {
                $_POST[$field_col_name] = $_POST[$field_name];
                unset($_POST[$field_name]);
            }
        }

        /**
         * If the password should be mailed out to the user we need to keep it here. We only do it if
         * this setting is enabled + it's only stored in the session 0.0001 secs until the other hook
         * is called ;)
         */
        if(ee()->abprefs->get('email_password_on_registration'))
        {
            if(!isset($_SESSION)){
			    session_start();
		    }
            $_SESSION['USR_PWD'] = $_POST['password'];
        }
	}



	/**
	 * This hook is called when a member registration is submitted. It will check if we have
	 *
	 * @param $data
	 */
	function on_member_register($data, $member_id)
	{
        if(ee()->input->post('simple_registration') != 'y')
        {
          return;
        }
        $this->new_member_id = $member_id;
        ee()->load->library('srlib');
        ee()->load->helper('url_helper');
        ee()->load->library('abprefs', array('module_name' => 'simple_registration')); // can't load this in constructor due to EE bug #12479

        // check if we have a signup key
        $signup_key = ee()->input->post('signup_key');

        if($signup_key)
        {
            $signup_key_info = ee()->srlib->get_signup_key_info($signup_key);
            if($signup_key_info)
            {
                if($signup_key_info['trigger_event'] == 'on_signup')
                {
                    if (ee()->extensions->active_hook('simple_registration_signup_key_triggered') === TRUE)
                    {
                        ee()->extensions->call('simple_registration_signup_key_triggered', $member_id, $signup_key, 'signup', $this);
                        if (ee()->extensions->end_script === TRUE) return;
                    }

                    if($signup_key_info['member_group_id'] > 0)
                    {
                        ee()->db->where('member_id', $member_id);
                        ee()->db->update('members', array('group_id' => $signup_key_info['member_group_id']));
                    }
                }
                else if($signup_key_info['trigger_event'] == 'on_activation')
                {
                    // save it for later
                    ee()->db->insert('simple_registration_pending_events', array('member_id' => $member_id, 'signup_key_id' => $signup_key_info['signup_key_id']));
                }
            }
        }

        if(ee()->abprefs->get('email_password_on_registration'))
        {
            // first things first
            if(!isset($_SESSION)){
			    session_start();
		    }
            $password = $_SESSION['USR_PWD'];
            unset($_SESSION['USR_PWD']);

            $vars = array();    // temp array
            foreach($data as $data_key => $data_value)
            {
                $vars[$data_key] = $data_value;
            }
            $vars['password'] = $password;
            $vars['member_id'] = $member_id;

            ee()->srlib->send_new_member_email($vars);
        }

        // deprecated for 3.1 for now ..
        $return_url = ee()->security->xss_clean(ee()->input->post('RET'));
        $skip_success_message = (ee()->input->post('skip_success_message') == 'y');
        $is_ajax = (ee()->input->post('simple_registration_ajax') == 'y');

        if (ee()->extensions->active_hook('simple_registration_success') === TRUE)
        {
            ee()->extensions->call('simple_registration_success', $member_id, $data, $this);
            if (ee()->extensions->end_script === TRUE) return;
        }
	}



}
// END CLASS


/* End of file ext.get_listed.php */
/* Location: ./system/expressionengine/third_party/get_listed/ext.get_listed.php */