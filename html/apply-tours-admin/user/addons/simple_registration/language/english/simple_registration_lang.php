<?php

$lang['simple_registration_module_name'] = "Simple Registration";
$lang['simple_registration_module_description'] = "Break free from the member templates and customize your registration form.";
$lang['welcome'] = "Welcome to Simple Registration";
$lang['settings'] = 'Settings';
$lang['your_reg_form'] = 'Your Simple Registration Form';

$lang['tab_welcome'] = "Welcome";
$lang['tab_form_code'] = "Your Form Code";
$lang['tab_form_settings'] = "Form Settings";
$lang['tab_settings'] = "Settings / Email";
$lang['tab_invite'] = "Invite Only";
$lang['tab_signup_keys'] = "Signup Keys";
$lang['tab_register_member'] = "Register a New Member";
$lang['tab_docs'] = "Documentation";

$lang['title_registration_form'] = 'Simple Registration Form Settings';
$lang['title_register_member'] = 'Register a New Member';
$lang['title_form_code'] = 'Your Form Code';

$lang['auto_pwd_no_email_warning'] = '<strong>Please note:</strong> you have enabled auto-generation of the user\'s password on account creation, so you should also enable the "Send user an email with password on registration" feature in "Email Settings" (tab to the right). Or else the user won\'t know the password!';

$lang['btn_get_code'] = "Save & Get Form Code";
$lang['btn_save'] = 'Save';
$lang['btn_save_changes'] = 'Save Changes';
$lang['btn_register_new_member'] = 'Register New Member';

$lang['sr_register_member_key'] = 'Item';
$lang['sr_register_member_value'] = 'Value';

/* Registration Form */

$lang['reg_form_select_elements'] = 'Required Fields';
$lang['reg_form_required_fields'] = 'Always required (cannot be removed)';
$lang['reg_form_select_expl'] = 'Pick and choose the elements you\'d like to include in your registration form. The checked elements will display.';
$lang['screen_name_expl'] = 'The user will enter a screen name (if disabled <strong>username</strong> will be used, if username is disabled as well email will be used)</strong>';
$lang['username_expl'] = 'The user will enter an username (disable this and <strong>email will be used</strong> for username)';
$lang['email_expl'] = 'The user will enter an e-mail address (this is the only required input for Simple Registration)';
$lang['email_confirm_expl'] = 'The user will confirm the e-mail address by inputting it again.';
$lang['password_expl'] = 'The user will enter a password (if disabled password will be shipped in e-mail on registration, think WordPress)';
$lang['password_confirm_expl'] = 'The user will confirm the password by entering it over again';
$lang['terms_expl'] = 'The user will have to tick a checkbox indicating that he/she has agreed to the Terms of Service';

$lang['required_fields_saved_title'] = 'Field Settings saved!';
$lang['required_fields_saved_body'] = 'You can find an example code using the current settings in the form below.';


/* General Settings */
$lang['settings_saved_title'] = 'Settings saved!';
$lang['general_settings'] = "General Settings";
$lang['username_equals_screen_name'] = 'Screen name equals username (user does not have to input screen name)';
$lang['do_not_require_email_confirmation'] = "Do not require email confirmation";
$lang['use_email_for_username'] = 'If this is set the email entered will be used for username';
$lang['do_not_require_accept_terms'] = "Do not require user to accept a 'Terms & Conditions'";
$lang['password_settings'] = "Password Settings";
$lang['do_not_require_password_confirmation'] = "Do not require password confirmation";
$lang['auto_generate_password'] = "Auto-generate password";
$lang['trigger'] = 'Trigger';
$lang['sr_settings_register_member_use_membergroup'] = 'Quick Registration Member Group';
$lang['sr_settings_register_member_use_membergroup_desc'] = 'Use this member group when registering new members using the \"Quick Registration\" functionality';

$lang['sr_settings'] = "Settings";
$lang['email_password_on_registration'] = 'Send user an email on registration?';

$lang['sr_settings_redirect_after_login'] = 'Auto Login Redirect';
$lang['sr_settings_redirect_after_login_desc'] = 'Redirect when clicking the {member_auto_login_link} in the email? (use relative url, ie. /community/welcome .. will default to site_url)';

/* Signup Keys */
$lang['signup_keys'] = "Signup Keys";
$lang['signup_keys_expln'] = "Generate signup keys to attach members to member groups";
$lang['no_signup_keys_added'] = "No signup keys exists.";
$lang['signup_key_added_title'] = "New signup key was added";
$lang['signup_key_added_body'] = "The new signup was added to the 'Existing Signup Keys' table.";

$lang['add_signup_key'] = "Add New Signup Key";
$lang['signup_key_deleted_title'] = 'Signup key deleted';

$lang['signup_keys_saved'] = 'Signup keys saved';
$lang['require_at_least_one_valid_signup_key'] = 'Require at least one valid signup key? <em>If checked users will be denied registration w/o a signup key (think beta invite)</em>';

$lang['table_signup_key'] = 'Signup Key';
$lang['table_member_group'] = 'Member Group';
$lang['table_trigger'] = 'Trigger';
$lang['table_delete'] = 'Delete';



/* Registration form */

$lang['sr_register_member_info_with_email_info_text'] = 'Use this form to create a new member. The email specified in Email Settings will be sent to the newly created member.';
$lang['sr_register_member_info_without_email_info_text'] = 'Use this form to create a new member.';
$lang['sr_success_member_created_email_sent'] = 'New member <strong>{screen_name}</strong> was created, and welcome email was sent to <strong>{email}</strong>!';
$lang['sr_success_member_created'] = 'New member <strong>{screen_name}</strong> was created!';

/* Errors */

$lang['sr_registration_not_allowed'] = '{exp:simple_registration:form} tag is used but the setting "Allow New Member Registrations?" is set to "No" - you must change this in Membership Preferences.';
$lang['sr_honeypot_err'] = 'Thanks! You entered the honeypot field and thus confirmed that you are not a human.';
$lang['sr_no_outher_registrations_error'] = 'This website only accept registrations through the main registration form';
$lang['sr_valid_signup_key_required_err'] = 'This website requires a valid signup key to complete the signup process.';
$lang['sr_on_registration_disabled_value_not_valid'] = 'The "on_registration_disabled" parameter was specified but the value does not seem valid. It needs to specify both the template group and the template you want to display when registrations are disabled like this: "template_group/template"';