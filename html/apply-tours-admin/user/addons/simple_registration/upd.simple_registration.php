<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!class_exists('Ab_UpdBase')) {
    require_once PATH_THIRD . 'simple_registration/libraries/ab/ab_updbase.php';
}

require_once PATH_THIRD . 'simple_registration/const.simple_registration.php';

/**
 * Let's you put a registration form anywhere
 *
 * @package        Simple_registration
 * @subpackage    ThirdParty
 * @category    Modules
 * @author        Bjørn Børresen
 * @link        http://wedoaddons.com
 */
class Simple_registration_upd extends Ab_UpdBase
{
    var $version = Const_Simple_Registration::CURRENT_VERSION;
    var $module_name = "Simple_registration";

    function __construct($switch = TRUE)
    {
        parent::__construct();
    }

    /**
     * Installer for the Simple_registration module
     */
    function install()
    {
        ee()->load->dbforge();

        $data = array(
            'module_name' => $this->module_name,
            'module_version' => $this->version,
            'has_cp_backend' => 'y'
        );

        ee()->db->insert('modules', $data);
        ee()->load->library('abprefs', array('module_name' => $this->module_name));
        $this->install_preferences();
        $this->install_signupkeys_tables();
        $this->install_actions();

        return TRUE;
    }

    /**
     * INSTALL preferences
     *
     * @param null $prefs_arr
     */
    private function install_preferences($prefs_arr = NULL)
    {
        if ($prefs_arr == NULL) {
            $prefs_arr = Const_Simple_Registration::$default_preferences;
        }

        ee()->load->library('abprefs', array('module_name' => $this->module_name));
        ee()->abprefs->install($prefs_arr, 1);
        if (ee()->config->item('site_id') != 1) // if we're not currently on site 1 also set settings on this site
        {
            ee()->abprefs->init_site($prefs_arr, ee()->config->item('site_id'));
        }
    }

    private function install_signupkeys_tables()
    {
        ee()->load->dbforge();
        $simple_registration_signup_keys_fields = array(
            'simple_registration_signup_key_id' => array(
                'type' => 'int',
                'constraint' => '10',
                'unsigned' => TRUE,
                'auto_increment' => TRUE),
            'site_id' => array('type' => 'int',
                'constraint' => '10',
                'null' => FALSE),
            'signup_key' => array(
                'type' => 'varchar',
                'constraint' => '255',
                'null' => FALSE,),

            'trigger_event' => array(
                'type' => 'varchar',
                'constraint' => '255',
                'null' => FALSE),

            'member_group_id' => array(
                'type' => 'int',
                'constraint' => '10',
                'null' => FALSE,),
        );

        ee()->dbforge->add_field($simple_registration_signup_keys_fields);
        ee()->dbforge->add_key('simple_registration_signup_key_id', TRUE);
        ee()->dbforge->create_table('simple_registration_signup_keys');

        $simple_registration_pending_events_fields = array(
            'simple_registration_pending_event_id' => array(
                'type' => 'int',
                'constraint' => '10',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,),
            'member_id' => array(
                'type' => 'int',
                'constraint' => '10',
                'null' => FALSE,),
            'signup_key_id' => array(
                'type' => 'int',
                'constraint' => '10',
                'null' => TRUE),
        );

        ee()->dbforge->add_field($simple_registration_pending_events_fields);
        ee()->dbforge->add_key('simple_registration_pending_event_id', TRUE);
        ee()->dbforge->create_table('simple_registration_pending_events');

        $this->addEventInfoToPendingEvents();   // added in 1.4.4
    }

    /**
     * Add event info cols to the pending events table
     */
    private function addEventInfoToPendingEvents()
    {
        $this->EE->db->query("ALTER TABLE  `" . $this->EE->db->dbprefix('simple_registration_pending_events') . "` ADD  `event_name` VARCHAR( 255 ) NOT NULL DEFAULT  'signup_key', ADD  `event_time` INT( 11 ) NOT NULL , ADD  `event_hash` VARCHAR( 255 ) NOT NULL");
        $this->EE->db->update('simple_registration_pending_events', array('event_name' => 'signup_key'));   // update any previous events, these were all signup keys
    }

    // install/update actions
    public function install_actions()
    {
        $actions = array('login_event');    // add actions here

        foreach ($actions as $action) {
            $action_arr = array('class' => $this->module_name, 'method' => $action);
            $q = $this->EE->db->get_where('actions', $action_arr);

            if ($q->num_rows() == 0) {
                $this->EE->db->insert('actions', $action_arr);
            }
        }
    }

    /**
     * Uninstall the Simple_registration module
     */
    function uninstall()
    {
        ee()->db->select('module_id');
        $query = ee()->db->get_where('modules', array('module_name' => $this->module_name));

        ee()->db->where('module_id', $query->row('module_id'));
        ee()->db->delete('module_member_roles');

        ee()->db->where('module_name', $this->module_name);
        ee()->db->delete('modules');

        ee()->db->where('class', $this->module_name);
        ee()->db->delete('actions');

        ee()->db->where('class', $this->module_name . '_mcp');
        ee()->db->delete('actions');

        ee()->load->library('abprefs', array('module_name' => $this->module_name));
        ee()->abprefs->uninstall();

        ee()->dbforge->drop_table('simple_registration_signup_keys');
        ee()->dbforge->drop_table('simple_registration_pending_events');

        return TRUE;
    }

    /**
     * Update the Simple_registration module
     *
     * @param $current current version number
     * @return boolean indicating whether or not the module was updated
     */

    function update($current = '')
    {
        ee()->load->library('abprefs', array('module_name' => $this->module_name));
        $prefs_arr = Const_Simple_Registration::$default_preferences;
        ee()->abprefs->update($prefs_arr);

        if ($current == $this->version) {
            return FALSE;
        }

        if ($current < '1.5') {
            $this->addEventInfoToPendingEvents();
            $this->install_actions();   // install new actions added in 1.5

            ee()->db->query("ALTER TABLE `" . ee()->db->dbprefix('simple_registration_pending_events') . "` CHANGE  `signup_key_id`  `signup_key_id` INT( 10 ) NULL DEFAULT NULL;");
        }


        if ($current < '1.2') {
            ee()->load->library('abprefs', array('module_name' => $this->module_name));
            // we have old serialized preferences, MSN support was not avail. at that time so only need to get one
            $pq = ee()->db->get_where('simple_registration_prefs', array('site_id' => '1'));
            $pq_arr = $pq->result_array();
            $current_preferences = $this->_unserialize($pq_arr[0]['preferences']);

            ee()->load->dbforge();
            ee()->dbforge->drop_table('simple_registration_prefs');
            $prefs_arr = Const_Simple_Registration::$default_preferences;
            foreach ($prefs_arr as $pref_key => $pref_value) {
                if (isset($current_preferences[$pref_key])) {
                    $pref_value['value'] = $current_preferences[$pref_key];
                }

                $prefs_arr[$pref_key] = $pref_value;
            }

            ee()->abprefs->install($prefs_arr, 1);


            // new table in this version as well
            $this->install_signupkeys_tables();

            // another hook was added in this version
            ee()->db->insert('extensions', array(
                'class' => 'Simple_registration_ext',
                'method' => 'on_member_register_validate_members',
                'hook' => 'member_register_validate_members',
                'settings' => '',
                'priority' => '10',
                'version' => Const_Simple_Registration::CURRENT_VERSION,
                'enabled' => 'y'));
        }

        if ($current < '1.2.2') {
            ee()->db->query("ALTER TABLE `" . ee()->db->dbprefix("simple_registration_signup_keys") . "` ADD `trigger_event` VARCHAR( 255 ) NOT NULL");
        }

        if ($current < '1.4.2') {
            ee()->db->query("ALTER TABLE `" . ee()->db->dbprefix("simple_registration_prefs") . "` ADD `require_at_least_one_signup_key` CHAR(1) NOT NULL");
            ee()->db->update('simple_registration_prefs', array('require_at_least_one_signup_key' => '0'));
        }

        return TRUE;
    }

    private function _unserialize($data)
    {
        $data = @unserialize(strip_slashes($data));

        if (is_array($data)) {
            foreach ($data as $key => $val) {
                $data[$key] = str_replace('{{slash}}', '\\', $val);
            }

            return $data;
        }

        return str_replace('{{slash}}', '\\', $data);
    }

}

/* End of file upd.simple_registration.php */
/* Location: ./system/expressionengine/third_party/simple_registration/upd.simple_registration.php */ 