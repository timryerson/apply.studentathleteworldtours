<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Const_Simple_Registration
{
    const CURRENT_VERSION = '1.6.3';

    public static $default_preferences = array(
                'username_equals_screen_name' => array('type' => Abprefs::TYPE_BOOLEAN, 'value' => FALSE),
                'do_not_require_email_confirmation' => array('type' => Abprefs::TYPE_BOOLEAN, 'value' => FALSE),
                'do_not_require_accept_terms' => array('type' => Abprefs::TYPE_BOOLEAN, 'value' => FALSE),
                'do_not_require_password_confirmation' => array('type' => Abprefs::TYPE_BOOLEAN, 'value' => FALSE),
                'use_email_for_username' => array('type' => Abprefs::TYPE_BOOLEAN, 'value' => FALSE),
                'auto_generate_password' => array('type' => Abprefs::TYPE_BOOLEAN, 'value' => FALSE),
                'email_password_on_registration' => array('type' => Abprefs::TYPE_BOOLEAN, 'value' => FALSE),
                'password_subject' => array('type' => Abprefs::TYPE_STRING, 'value' => 'Welcome to {site_name} - your login & password enclosed'),
                'password_email' => array('type' => Abprefs::TYPE_TEXT, 'value' => 'Welcome to {site_name}!

Your login: {username}
Your password: {password}

Click this link to log in automatically:
{member_auto_login_link}

Please keep this email for your reference.'),
        'require_at_least_one_signup_key' => array('type' => Abprefs::TYPE_BOOLEAN, 'value' => FALSE),
        'register_member_use_membergroup' => array('type' => Abprefs::TYPE_INT, 'value' => NULL),
        'register_member_redirect_to' => array('type' => Abprefs::TYPE_STRING, 'value' => ''),
        );

}