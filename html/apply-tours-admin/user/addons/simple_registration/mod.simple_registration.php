<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!class_exists('Ab_ModBase')) {
    require PATH_THIRD.'simple_registration/libraries/ab/ab_modbase.php';
}


/**
 * Let's you put a registration form anywhere
 *
 * @package		Simple_registration
 * @subpackage	ThirdParty
 * @category	Modules
 * @author		bjorn
 * @link		http://ee.bybjorn.com/
 */
class Simple_registration extends Ab_ModBase
{

    public function __construct()
    {
        parent::__construct();
        ee()->lang->loadfile('simple_registration');
    }

    public function forgot_password()
    {
        $return_to = $this->get_param('return');
        $action = $this->get_param('action');
        $exact_return_url = $this->get_param('exact_return_url');
        $data = array();
        $data['hidden_fields'] = array(
            'ACT'	=> ee()->functions->fetch_action_id('Member', 'send_reset_token'),
        );

        if($action)
        {
            $data['action'] = $action;
        }

        if($return_to && !$exact_return_url)
        {
            $data['hidden_fields']['RET'] = ee()->functions->create_url($return_to);
        }
        else if($exact_return_url)
        {
            $data['hidden_fields']['RET'] = $exact_return_url;
        }

        $data['id']	= $this->get_param('id', 'forgot_password_form');

        $form_name = $this->get_param('name');
        if($form_name)
        {
            $data['name'] = $form_name;
        }

        $form_class = $this->get_param('class');
        if($form_class)
        {
            $data['class'] = $form_class;
        }


        $form_data = ee()->TMPL->tagdata;

        $this->return_data = ee()->functions->form_declaration($data)."\n" . $form_data ."</form>";
        return $this->return_data;
    }

    /**
     * Login link
     */
    public function login_event()
    {
        $this->EE->load->library('srlib');
        $hash = $this->EE->input->get('hash');
        if($hash && strlen($hash) > 12)
        {
            $q = $this->EE->db->get_where('simple_registration_pending_events', array('event_hash' => $hash));

            if($q->num_rows() == 1)
            {
                $login_as_member_id = $q->row('member_id');
                //$this->EE->db->where(array('event_hash' => $hash))->delete('simple_registration_pending_events');
                $this->EE->srlib->login_member($login_as_member_id);
            }
        }
    }

	/**
	 * Generate a registration form
	 */
	public function form()
	{
        if (ee()->config->item('allow_member_registration') == 'n')
        {
            $on_registration_disabled = $this->get_param('on_registration_disabled');
            if($on_registration_disabled) {
                $template_group_and_template = explode('/', trim($on_registration_disabled,'/'));
                if(count($template_group_and_template) > 1) {
                    ee()->TMPL->run_template_engine($template_group_and_template[0], $template_group_and_template[1]);
                } else {
                    $data = array(	'title' 	=> ee()->lang->line('simple_registration'),
                        'heading'	=> ee()->lang->line('error'),
                        'content'	=> ee()->lang->line('sr_on_registration_disabled_value_not_valid'),
                        'link'		=> array(ee()->functions->fetch_site_index(), stripslashes(ee()->config->item('site_name')))
                    );

                    ee()->output->show_message($data);
                }
            } else {
                $data = array(	'title' 	=> ee()->lang->line('simple_registration'),
                    'heading'	=> ee()->lang->line('error'),
                    'content'	=> ee()->lang->line('sr_registration_not_allowed'),
                    'link'		=> array(ee()->functions->fetch_site_index(), stripslashes(ee()->config->item('site_name')))
                );

                ee()->output->show_message($data);
            }
        }

        $action = $this->get_param('action');
        $exact_return_url = $this->get_param('exact_return_url');
		$return_to = $this->get_param('return');
        $ajax = $this->get_param('ajax') == 'yes';


        $allow_fields = $this->get_param('allow_fields');
        $login_member = $this->get_param('login_member');

		$skip_success_message = $this->get_param('skip_success_message');
		
		$data = array();
		$data['hidden_fields'] = array(
            'ACT'	=> ee()->functions->fetch_action_id('Member', 'register_member'),
            'simple_registration' => 'y',
        );


        /**
         * We explicitly open for other fields such as url, location, bio, etc.
         */
        if($allow_fields)
        {
            ee()->load->library('encrypt');
            $data['hidden_fields']['allow_fields'] = ee()->encrypt->encode($allow_fields);
        }

        if($action)
        {
            $data['action'] = $action;
        }

        if($ajax)
        {
            $data['hidden_fields']['simple_registration_ajax'] = 'y';
        }

        if($return_to && !$exact_return_url)
        {
            $data['hidden_fields']['RET'] = ee()->functions->create_url($return_to);
        }
        else if($exact_return_url)
        {
            $data['hidden_fields']['RET'] = $exact_return_url;
        }

        if($skip_success_message == 'y' || $skip_success_message == 'yes')
        {
        	$data['hidden_fields']['skip_success_message'] = 'y';
        }

        if($login_member == 'no') {
            $data['hidden_fields']['login_member'] = 'n';
        }

		$data['id']	= $this->get_param('id', 'register_member_form');

        /**
         * Do we have a honeypot field?
         */
        $honeypot = $this->get_param('honeypot');
        if($honeypot)
        {
            $data['hidden_fields']['simple_registration_hp'] = $honeypot;
        }

        $form_name = $this->get_param('name');
        if($form_name)
        {
            $data['name'] = $form_name;
        }

        $form_class = $this->get_param('class');
        if($form_class)
        {
            $data['class'] = $form_class;
        }
	
		$reg_form = ee()->TMPL->tagdata;

        if (ee()->config->item('use_membership_captcha') == 'y')
        {
            $captcha_image = '';
            if(ee()->session->userdata('member_id') != 0)
            {
                $captcha_image = '(CAPTCHA only generated for logged out users)';
            }
            else
            {
                $captcha_image = ee()->functions->create_captcha();
            }
            $reg_form = preg_replace("/{captcha}/", $captcha_image, $reg_form);
		}

		$this->return_data = ee()->functions->form_declaration($data)."\n" . $reg_form ."</form>";
		return $this->return_data;		
	}
    
}

/* End of file mod.simple_registration.php */ 
/* Location: ./system/expressionengine/third_party/simple_registration/mod.simple_registration.php */ 