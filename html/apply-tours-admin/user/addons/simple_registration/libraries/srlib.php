<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
if (!class_exists('Ab_LibBase')) {
    require_once 'ab/ab_libbase.php';
}

require APPPATH . 'libraries/Auth.php';

/**
 * EE Library - Simple Registration Library
 *
 * Developer: Bjorn Borresen / AddonBakery
 * Date: 01.04.11
 * Time: 14:11
 *
 */
class Srlib extends Ab_LibBase
{

    /**
     * Get an array of signup keys for the current site
     *
     * @param $trigger string get signup keys for a specific trigger
     * @return array
     */
    public function get_signup_keys($trigger = FALSE)
    {
        $keys = array();
        $where_arr = array('site_id' => ee()->config->item('site_id'));
        if ($trigger) {
            $where_arr['trigger_event'] = $trigger;
        }
        $q = ee()->db->get_where('simple_registration_signup_keys', $where_arr);
        foreach ($q->result() as $signup_key) {
            $keys[$signup_key->signup_key] = array('signup_key_id' => $signup_key->simple_registration_signup_key_id, 'member_group_id' => $signup_key->member_group_id, 'trigger_event' => $signup_key->trigger_event);
        }
        return $keys;
    }

    /**
     * Get information about a signup key
     *
     * @param  $signup_key
     * @return array of info or false if signup key could not be found
     */
    public function get_signup_key_info($signup_key)
    {
        ee()->db->select('simple_registration_signup_key_id as signup_key_id, site_id, signup_key, trigger_event, member_group_id');
        $q = ee()->db->get_where('simple_registration_signup_keys', array('site_id' => ee()->config->item('site_id'), 'signup_key' => $signup_key));
        if ($q->num_rows() > 0) {
            $arr = $q->result_array();
            return $arr[0];
        } else {
            return FALSE;
        }
    }


    /**
     * Generate a random password
     * @return string
     */
    public function get_random_password()
    {
        $allowed_chars = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $password = array();
        $allowed_chars_len = strlen($allowed_chars) - 1;
        $password_length = rand(8, 12);
        for ($i = 0; $i < $password_length; $i++) {
            $char_index = rand(0, $allowed_chars_len);
            $password[] = $allowed_chars[$char_index];
        }
        return implode($password);
    }

    /**
     * @return array of (id => member group name)
     */
    public function get_member_groups()
    {
        $member_groups_arr = array();
        $mg = ee()->db->order_by('role_id', 'desc')->get_where('roles');
        foreach ($mg->result() as $member_group) {
            $member_groups_arr[$member_group->role_id] = $member_group->name;
        }
        return $member_groups_arr;
    }


    /**
     * Validate user information
     *
     * @param $username
     * @param $email
     * @param $password
     * @param $screen_name
     * @return array of errors
     */
    public function validate_member($username, $email, $password, $screen_name)
    {

        // Instantiate validation class
        if (!class_exists('EE_Validate')) {
            require APPPATH . 'libraries/Validate.php';
        }

        $VAL = new EE_Validate(array(
            'member_id' => '',
            'val_type' => 'new', // new or update
            'fetch_lang' => TRUE,
            'require_cpw' => FALSE,
            'enable_log' => FALSE,
            'username' => $username,
            'cur_username' => '',
            'screen_name' => $screen_name,
            'cur_screen_name' => '',
            'password' => $password,
            'password_confirm' => $password,
            'cur_password' => '',
            'email' => $email,
            'cur_email' => ''
        ));

        $VAL->validate_username();
        $VAL->validate_screen_name();
        $VAL->validate_password();
        $VAL->validate_email();

        // EE will produce duplicate error messages, so make sure we remove those
        $errors = $VAL->errors;
        $ret_errors = array();
        foreach ($errors as $error_msg) {
            if (!in_array($error_msg, $ret_errors)) {
                $ret_errors[] = $error_msg;
            }
        }

        return $ret_errors;
    }

    /**
     * @param $vars data array consisting of: member_id, email, password, username, screen_name, ip_address, group_id, join_date
     */
    public function send_new_member_email($vars)
    {
        $vars['site_url'] = ee()->config->item('site_url');
        $vars['site_name'] = ee()->config->item('site_name');
        $vars['webmaster_email'] = ee()->config->item('webmaster_email');

        ee()->load->library('template', NULL, 'TMPL');
        ee()->load->library('email');

        $email = $vars['email'];
        $email_body_template = ee()->abprefs->get('password_email');
        $email_subject_template = ee()->abprefs->get('password_subject');

        // if the template uses the auto login link, we need to generate a login hash
        if (strpos($email_body_template, '{member_auto_login_link}') !== FALSE) {
            $login_hash = $this->get_login_event_hash($vars['member_id']);
            $auto_login_url = $this->get_login_event_url($login_hash);
            $vars['member_auto_login_link'] = $auto_login_url;
        }

        $email_body = ee()->TMPL->parse_variables($email_body_template, array($vars));
        $email_subject = ee()->TMPL->parse_variables($email_subject_template, array($vars));

        if (ee()->extensions->active_hook('simple_registration_email_password') === TRUE) {
            $email_info = array('email_body' => $email_body, 'email_subject' => $email_subject, 'email' => $email);
            $email_info = ee()->extensions->call('simple_registration_email_password', $email_info, $this);
            if (ee()->extensions->end_script === TRUE) return;
            if ($email_info && is_array($email_info)) {
                $email_body = $email_info['email_body'];
                $email_subject = $email_info['email_subject'];
                $email = $email_info['email'];
            }
        }

        ee()->email->clear();

        ee()->email->wordwrap = true;

        $email_config = array(
            'newline' => "\r\n",
            'crlf' => "\n",
        );

        ee()->email->initialize($email_config);

        ee()->email->from(ee()->config->item('webmaster_email'), ee()->config->item('site_name'));
        ee()->email->message($email_body);
        ee()->email->subject($email_subject);
        ee()->email->to($email);
        ee()->email->send();
    }


    /**
     * Log in a specific member - currently superhacky since EE's API does not have any way of doing this (yet)
     *
     * @param $member_id
     */
    public function login_member($member_id)
    {
        ee()->load->library('abprefs', array('module_name' => 'simple_registration'));
        // Create a new session
        $session_id = ee()->session->create_new_session($member_id);
        $redirect_after_login = ee()->abprefs->get('register_member_redirect_to');

        if (!$redirect_after_login) {
            $redirect_after_login = ee()->config->item('site_url');
        }

        ee()->functions->redirect($redirect_after_login);
    }

    /**
     * Get url for a login event
     *
     * @param $hash
     * @return string
     */
    public function get_login_event_url($hash)
    {
        $url = FALSE;
        $q = ee()->db->get_where('actions', array('class' => 'Simple_registration', 'method' => 'login_event'));
        if ($q->num_rows() > 0) {
            $url = ee()->functions->fetch_site_index() . '?ACT=' . $q->row('action_id') . '&hash=' . $hash;
        }
        return $url;
    }

    /**
     * Will return a login hash for a specific member_id. If a login event hash does not exist, it will
     * generate one and store it in the database, otherwise the existing one will be returned.
     *
     * @param $member_id
     * @return bool|string the login hash
     */
    public function get_login_event_hash($member_id)
    {
        $hash = FALSE;
        $q = ee()->db->get_where('simple_registration_pending_events', array('member_id' => $member_id, 'event_name' => 'email_login'));
        if ($q->num_rows() == 0) {
            $hash = md5(ee()->localize->now . $member_id . rand(999, getrandmax()));
            ee()->db->insert('simple_registration_pending_events', array(
                'member_id' => $member_id,
                'event_name' => 'email_login',
                'event_time' => ee()->localize->now,
                'event_hash' => $hash
            ));
        } else {
            $hash = $q->row('event_hash');
        }

        return $hash;
    }
}

class NoHashAuth extends Auth
{
    public function hash_password($password, $salt = FALSE, $h_byte_size = FALSE)
    {
        return array('salt' => $salt, 'password' => $password);
    }
}