<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!class_exists('Ab_McpBase')) {
    require_once PATH_THIRD . 'simple_registration/libraries/ab/ab_mcpbase.php';
}

require_once PATH_THIRD . 'simple_registration/const.simple_registration.php';

/**
 * Let's you put a registration form anywhere
 *
 * @package        Simple_registration
 * @subpackage    ThirdParty
 * @category    Modules
 * @author        bjorn
 * @link        http://ee.bybjorn.com/
 */
class Simple_registration_mcp extends Ab_McpBase
{
    var $base;            // the base url for this module
    var $form_base;        // base url for forms
    var $cp_theme_path;
    var $module_name = "simple_registration";

    private $triggers = array('on_signup' => 'Signup', 'on_activation' => 'Activation');

    public function __construct($switch = TRUE)
    {
        parent::__construct($switch);
        ee()->load->add_package_path($this->module_name);
        ee()->load->config('simple_registration');
        $this->cp_theme_path = ee()->config->slash_item('theme_folder_url') . 'third_party/simple_registration/';

        // Make a local reference to the ExpressionEngine super object 
        $this->base = BASE . AMP . 'C=addons_modules' . AMP . 'M=show_module_cp' . AMP . 'module=' . $this->module_name;
        $this->form_base = 'C=addons_modules' . AMP . 'M=show_module_cp' . AMP . 'module=' . $this->module_name;

        ee()->cp->load_package_js('simple_registration');

        // load libraries
        ee()->load->library('abprefs', array('module_name' => $this->module_name));

        // init if we do not have preferences for this site
        if (!ee()->abprefs->has_preferences()) {
            ee()->abprefs->init_site(Const_Simple_Registration::$default_preferences); // init preferences for this site
            ee()->abprefs->fetch_preferences();
        }

    }


    /**
     * This function will load the javascript required for the module
     */
    public function load_module_js()
    {
        $js = file_get_contents(PATH_THIRD . '/' . $this->module_name . '/js/simple_registration.js');

        $disable_arr = array(
            'username_equals_screen_name' => 'screen_name',
            'do_not_require_email_confirmation' => 'confirm_email',
            'do_not_require_accept_terms' => 'terms',
            'do_not_require_password_confirmation' => 'confirm_password',
            'use_email_for_username' => 'username',
            'auto_generate_password' => 'password',
        );

        $add_js = '';
        foreach ($disable_arr as $setting => $input_element) {
            if (ee()->abprefs->get($setting)) {
                $add_js .= "\ndisableOption('" . $input_element . "');";
            }
        }

        $js = str_replace('{{magick}}', $add_js, $js);
        die($js);
    }

    public function index()
    {
        $vars = array();
        $vars['preferences'] = ee()->abprefs->get_preferences();

        $view = ee('View')->make('simple_registration:main');
        return $view->render($vars);


    }

    public function form_settings()
    {
        $vars = array();
        $vars['preferences'] = ee()->abprefs->get_preferences();

        $view = ee('View')->make('simple_registration:registration_form');
        return $view->render($vars);

    }

    public function register_member()
    {
        ee()->load->library('srlib');

        $vars = array('username' => '', 'email' => '', 'screen_name' => '', 'member_group_id' => '', 'password' => ee()->srlib->get_random_password());
        $vars['member_groups'] = ee()->srlib->get_member_groups();
        $vars['errors'] = array();
        $vars['success'] = FALSE;
        $vars['preferences'] = ee()->abprefs->get_preferences();

        $submitted = ee()->input->post('registration_form_submitted');
        if ($submitted) {

            $email = ee()->input->post('email');
            $username = trim(ee()->input->post('username'));
            $password = trim(ee()->input->post('password'));
            $screen_name = trim(ee()->input->post('screen_name'));

            /**
             * If we haven't specified a member group to use we allow this to come via the form.
             */
            $member_group_id = ee()->abprefs->get('register_member_use_membergroup');
            if (!$member_group_id) {
                $member_group_id = intval(ee()->input->post('member_group_id'));
            }

            if (!$username) {
                $username = $email;
            }

            if (!$screen_name) {
                $screen_name = $username ? $username : $email;
            }

            $errors = ee()->srlib->validate_member($username, $email, $password, $screen_name);
            if (count($errors)) {

                // reset these back to what we got as input
                $username = trim(ee()->input->post('username'));
                $screen_name = trim(ee()->input->post('screen_name'));

                $vars['username'] = $username;
                $vars['email'] = $email;
                $vars['screen_name'] = $screen_name;
                $vars['member_group_id'] = $member_group_id;
                $vars['password'] = $password;
                $vars['auto_password'] = ee()->srlib->get_random_password();
                $vars['errors'] = $errors;

                $view = ee('View')->make('simple_registration:register_member');
                return $view->render($vars);

            } else {

                ee()->load->helper('security');

                // Assign the base query data
                $member_data = array(
                    'username' => $username,
                    'password' => sha1($password),
                    'ip_address' => ee()->input->ip_address(),
                    'unique_id' => ee()->functions->random('encrypt'),
                    'join_date' => ee()->localize->now,
                    'email' => $email,
                    'screen_name' => $screen_name,
                    'group_id' => $member_group_id,

                    // overridden below if used as optional fields
                    'language' => (ee()->config->item('deft_lang')) ?
                        ee()->config->item('deft_lang') : 'english',
                    'time_format' => (ee()->config->item('time_format')) ?
                        ee()->config->item('time_format') : 'us',
                    'timezone' => ee()->config->item('default_site_timezone')
                );

                ee()->db->insert('members', $member_data);
                $member_id = ee()->db->insert_id();

                // Insert custom fields
                $cust_fields['member_id'] = $member_id;

                ee()->db->insert('member_data', array('member_id' => $member_id));
                ee()->stats->update_member_stats();

                ee()->load->library('abprefs', array('module_name' => 'simple_registration')); // can't load this in constructor due to EE bug #12479

                if (ee()->abprefs->get('email_password_on_registration')) {
                    $email_vars = array(
                        'member_id' => $member_id,
                        'email' => $email,
                        'username' => $username,
                        'screen_name' => $screen_name,
                        'password' => $password,
                        'group_id' => $member_group_id,
                        'ip_address' => $member_data['ip_address'],
                        'join_date' => $member_data['join_date']
                    );
                    ee()->srlib->send_new_member_email($email_vars);
                    $vars['success'] = str_replace(array('{screen_name}', '{email}'), array($screen_name, $email), lang('sr_success_member_created_email_sent'));

                } else {
                    $vars['success'] = str_replace(array('{screen_name}', '{email}'), array($screen_name, $email), lang('sr_success_member_created'));
                }

            }

        }

        $view = ee('View')->make('simple_registration:register_member');
        return $view->render($vars);
    }

    public function form_code()
    {
        $vars = array();
        $vars['preferences'] = ee()->abprefs->get_preferences();
        $vars['use_membership_captcha'] = (ee()->config->item('use_membership_captcha') == 'y');

        $view = ee('View')->make('simple_registration:form_code');
        return $view->render($vars);
    }

    public function settings()
    {
        ee()->load->library('srlib');
        $vars['preferences'] = ee()->abprefs->get_preferences();

        $member_groups = ee()->srlib->get_member_groups();
        $member_groups_arr[0] = '== NONE (select one when registering) ==';
        foreach ($member_groups as $group_id => $group_title) {
            $member_groups_arr[$group_id] = $group_title;
        }

        $vars['member_groups'] = $member_groups_arr;

        $view = ee('View')->make('simple_registration:settings');
        return $view->render($vars);
    }

    public function add_signup_key()
    {
        $member_group_id = ee()->input->post('member_group_id');
        if ($member_group_id) {
            $signup_key = md5(rand() . time() . $member_group_id);
            ee()->db->insert('simple_registration_signup_keys', array(
                'site_id' => ee()->config->item('site_id'),
                'signup_key' => $signup_key,
                'trigger_event' => ((ee()->config->item('req_mbr_activation') == 'email') ? 'on_activation' : 'on_signup'),    // trigger on_activation default if activation is enabled
                'member_group_id' => $member_group_id));
        }

        ee('CP/Alert')->makeStandard('simple-registration-signup-key-added')
            ->asSuccess()
            ->withTitle(lang('signup_key_added_title'))
            ->addToBody(lang('signup_key_added_body'))
            ->defer();

        ee()->functions->redirect(
            ee('CP/URL', 'addons/settings/simple_registration/signup_keys')
        );
    }

    public function delete_signup_key()
    {
        $key = ee()->uri->segment(6);

        if ($key) {
            ee()->db->where('signup_key', $key);
            ee()->db->delete('simple_registration_signup_keys');

            ee('CP/Alert')->makeStandard('simple-registration-signup-key-deleted')
                ->asSuccess()
                ->withTitle(lang('signup_key_deleted_title'))
                ->defer();


        }
        ee()->functions->redirect(
            ee('CP/URL', 'addons/settings/simple_registration/signup_keys')
        );
    }

    public function update_signup_keys()
    {
        ee()->load->library('srlib');
        $keys = ee()->srlib->get_signup_keys();
        foreach ($keys as $signup_key => $signup_key_arr) {
            $new_member_group = ee()->input->post('key_' . $signup_key_arr['signup_key_id']);
            $new_trigger = ee()->input->post('trigger_' . $signup_key_arr['signup_key_id']);
            if ($new_member_group) {
                ee()->db->where('signup_key', $signup_key);
                ee()->db->update('simple_registration_signup_keys', array('member_group_id' => $new_member_group, 'trigger_event' => $new_trigger));
            }
        }

        ee()->abprefs->set('require_at_least_one_signup_key', ee()->input->post('require_at_least_one_signup_key') == '1' ? '1' : '0');
        ee()->abprefs->save_preferences();

        ee('CP/Alert')->makeStandard('simple-registration-signup-key-added')
            ->asSuccess()
            ->withTitle(lang('signup_keys_saved'))
            ->defer();


        ee()->functions->redirect(
            ee('CP/URL', 'addons/settings/simple_registration/signup_keys')
        );
    }

    public function signup_keys()
    {
        ee()->load->library('srlib');

        $table = ee('CP/Table');

        $signup_keys = ee()->srlib->get_signup_keys();

        $vars['preferences'] = ee()->abprefs->get_preferences();

        ee()->db->order_by('role_id', 'desc');
        $mg = ee()->db->get_where('roles');
        $member_groups = array();
        foreach ($mg->result() as $member_group) {
            $member_groups[$member_group->role_id] = $member_group->name;
        }
        $vars['member_groups'] = $member_groups;

        $table_data = array();
        foreach ($signup_keys as $signup_key => $key_data) {
            $table_data[] = array(
                $signup_key,
                form_dropdown('key_' . $key_data['signup_key_id'], $member_groups, $key_data['member_group_id']),
                form_dropdown('trigger_' . $key_data['signup_key_id'], $this->triggers, $key_data['trigger_event']),
                '<ul class="toolbar"><li class="remove"><a title="Delete this signup key" href="' . ee('CP/URL', 'addons/settings/simple_registration/delete_signup_key/' . $signup_key) . '"></a></li></ul>'
            );
        }

        $table->setColumns(
            array(
                'table_signup_key',
                'table_member_group' => array('encode' => FALSE),
                'table_trigger' => array('encode' => FALSE),
                'table_delete' => array('encode' => FALSE),
            )
        );

        $table->setData($table_data);
        $table->setNoResultsText('no_signup_keys_added', 'add_signup_key');
        $vars['table_html'] = ee('View')->make('_shared/table')->render($table->viewData());

        $view = ee('View')->make('simple_registration:signup_keys');
        return $view->render($vars);
    }

    public function save_settings()
    {
        ee()->abprefs->set('email_password_on_registration', (ee()->input->post('email_password_on_registration') == '1'));
        ee()->abprefs->set('password_email', ee()->input->post('password_email'));
        ee()->abprefs->set('password_subject', ee()->input->post('password_subject'));
        ee()->abprefs->set('register_member_use_membergroup', intval(ee()->input->post('register_member_use_membergroup')));
        ee()->abprefs->set('register_member_redirect_to', ee()->input->post('register_member_redirect_to'));
        ee()->abprefs->save_preferences();

        ee('CP/Alert')->makeStandard('simple-registration-settings-saved')
            ->asSuccess()
            ->withTitle(lang('settings_saved_title'))
            ->defer();

        ee()->functions->redirect(ee('CP/URL', 'addons/settings/simple_registration/settings'));
    }

    function save_form_settings()
    {
        $save_arr = array(
            'username_equals_screen_name',
            'do_not_require_email_confirmation',
            'do_not_require_accept_terms',
            'do_not_require_password_confirmation',
            'use_email_for_username',
            'auto_generate_password',
        );

        foreach ($save_arr as $setting_name) {
            $value = (ee()->input->post($setting_name) != 1);
            ee()->abprefs->set($setting_name, $value);
        }

        // updating preference settings
        ee()->abprefs->save_preferences();

        ee('CP/Alert')->makeStandard('simple-registration-required-fields-settings-saved')
            ->asSuccess()
            ->withTitle(lang('required_fields_saved_title'))
            ->addToBody(lang('required_fields_saved_body'))
            ->defer();

        ee()->functions->redirect(ee('CP/URL', 'addons/settings/simple_registration/form_code'));
    }

}

/* End of file mcp.simple_registration.php */
/* Location: ./system/expressionengine/third_party/simple_registration/mcp.simple_registration.php */ 