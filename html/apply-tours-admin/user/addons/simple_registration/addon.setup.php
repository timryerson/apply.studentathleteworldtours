<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_THIRD.'simple_registration/const.simple_registration.php';

return array(
    'author'      => 'Wedoaddons',
    'author_url'  => 'http://wedoaddons.com',
    'name'        => 'Simple Registration',
    'description' => 'A simple stand-alone registration form for ExpressionEngine.',
    'version'     => Const_Simple_Registration::CURRENT_VERSION,
    'namespace'   => 'Wedoaddons\Simple-Registration',
    'settings_exist' => TRUE,
    'docs_url' => 'http://wedoaddons.com/addon/simple-registration',
);