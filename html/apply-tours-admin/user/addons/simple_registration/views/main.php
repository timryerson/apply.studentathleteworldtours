<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$this->extend('_wrapper');

?>

<h1>Create beautiful signup forms with ease!</h1>
<div class="txt-wrap">
    <p>This addon removes the need to edit the member templates (think /member/register/ and /member/forgot_password/), in addition to enabling you to remove otherwise required elements from the signup form to make it simpler / easier for the user to create an account.</p>
    <p>
        Check out the <a href="http://wedoaddons.com/simple-registration" target="_blank">documentation online</a> for more information.
    </p>
</div>