<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$this->extend('_wrapper');

?>
<h1>Add Signup Key</h1>

<?php
echo form_open(ee('CP/URL', 'addons/settings/simple_registration/add_signup_key', 'class="settings"'));
?>
<p class="txt-wrap">
 Generate a new key for member group: <?php print form_dropdown('member_group_id', $member_groups);?>

    <input class="btn" type="submit" value="<?php echo lang('add_signup_key');?>">

<?php print form_close()?>
</p>

<h1>Existing Signup Keys</h1>

<?php
echo form_open(ee('CP/URL', 'addons/settings/simple_registration/update_signup_keys'), 'class="settings"' );
?>

<?php echo $table_html ?>


<fieldset class="form-ctrls">
    <input class="btn" type="submit" value="<?php echo lang('btn_save_changes');?> ">
</fieldset>

</form>


<h1>About Signup Keys</h1>
<div class="settings">
    <h2><strong>Signup keys</strong> can be used to assign a user to a specific member group on signup, and are used by adding an element named "<em>signup_key</em>" to your form.</h2>

    <p>
        Read <a href="http://wedoaddons.com/simple-registration/configuration/signup-keys" target="_blank">more about Signup Keys and see usage examples</a> in the documentation.
    </p>

</div>
