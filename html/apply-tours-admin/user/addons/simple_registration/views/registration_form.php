<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$this->extend('_wrapper');

?>

<h1><?php echo lang('reg_form_select_elements')?><span class="req-title"><?php echo lang('reg_form_required_fields')?></span></h1>

<div class="txt-wrap">
    <p><?php echo lang('reg_form_select_expl');?></p>

<?php echo form_open(ee('CP/URL', 'addons/settings/simple_registration/save_form_settings'), 'class="settings"'); ?>

        <fieldset class="col-group">
            <div class="setting-txt col w-8">
                <h3>Screen name</h3>
                <em><?php echo lang('screen_name_expl');?></em>
            </div>
            <div class="setting-field col w-8 last">
                <label class="choice <?php if($preferences['username_equals_screen_name'] == "0") echo "chosen"; ?>"><input type="checkbox" value="1" name="username_equals_screen_name" <?php if($preferences['username_equals_screen_name'] == "0") echo 'checked="checked"'?>> Required</label>
            </div>
        </fieldset>

        <fieldset class="col-group">
            <div class="setting-txt col w-8">
                <h3>Username</h3>
                <em><?php echo lang('username_expl');?></em>
            </div>
            <div class="setting-field col w-8 last">
                <label class="choice <?php if($preferences['use_email_for_username'] == "0") echo "chosen"; ?>"><input type="checkbox" value="1" name="use_email_for_username" <?php if($preferences['use_email_for_username'] == "0") echo 'checked="checked"'?>> Required</label>
            </div>
        </fieldset>

        <fieldset class="col-group required">
            <div class="setting-txt col w-8">
                <h3>Email Address</h3>
                <em><?php echo lang('email_expl');?></em>
            </div>
            <div class="setting-field col w-8 last">
                <label class="choice mr chosen"><input type="checkbox" checked="checked" disabled> Required</label>
            </div>
        </fieldset>

        <fieldset class="col-group">
            <div class="setting-txt col w-8">
                <h3>Confirm Email Address</h3>
                <em><?php echo lang('email_confirm_expl');?></em>
            </div>
            <div class="setting-field col w-8 last">
                <label class="choice <?php if($preferences['do_not_require_email_confirmation'] == "0") echo "chosen"; ?>"><input type="checkbox" value="1" name="do_not_require_email_confirmation" <?php if($preferences['do_not_require_email_confirmation'] == "0") echo 'checked="checked"'?>> Required</label>
            </div>
        </fieldset>

        <fieldset class="col-group">
            <div class="setting-txt col w-8">
                <h3>Password</h3>
                <em><?php echo lang('password_expl');?></em>
            </div>
            <div class="setting-field col w-8 last">
                <label class="choice <?php if($preferences['auto_generate_password'] == "0") echo "chosen"; ?>"><input type="checkbox" value="1" name="auto_generate_password" <?php if($preferences['auto_generate_password'] == "0") echo 'checked="checked"'?>> Required</label>
            </div>
        </fieldset>

        <fieldset class="col-group">
            <div class="setting-txt col w-8">
                <h3>Confirm Password</h3>
                <em><?php echo lang('password_confirm_expl');?></em>
            </div>
            <div class="setting-field col w-8 last">
                <label class="choice <?php if($preferences['do_not_require_password_confirmation'] == "0") echo "chosen"; ?>"><input type="checkbox" value="1" name="do_not_require_password_confirmation" <?php if($preferences['do_not_require_password_confirmation'] == "0") echo 'checked="checked"'?>> Required</label>
            </div>
        </fieldset>

        <fieldset class="col-group">
            <div class="setting-txt col w-8">
                <h3>Terms of Service</h3>
                <em><?php echo lang('terms_expl');?></em>
            </div>
            <div class="setting-field col w-8 last">
                <label class="choice <?php if($preferences['do_not_require_accept_terms'] == "0") echo "chosen"; ?>"><input type="checkbox" value="1" name="do_not_require_accept_terms" <?php if($preferences['do_not_require_accept_terms'] == "0") echo 'checked="checked"'?>> Required</label>
            </div>
        </fieldset>



        <fieldset class="form-ctrls">
            <input class="btn" type="submit" value="Save settings">
        </fieldset>



    </form>

</div>