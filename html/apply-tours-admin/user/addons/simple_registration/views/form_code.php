<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$this->extend('_wrapper');

?>

<h1>Form HTML Code</h1>
<div class="txt-wrap">
<p>The form code below is the HTML you'll need to paste into your template to create a registration form. It's based on your <a href="<?php echo ee('CP/URL', 'addons/settings/simple_registration/form_settings'); ?>">current form settings</a>.</p>

<form class="settings">
    <fieldset class="col-group">
        <div class="setting-txt col w-6">
            <h3>Registration Form</h3>
            <em>Example registration form that can be used in any template.</em>
        </div>
        <div class="setting-field col w-10 last">
            <textarea cols="" rows="">
{exp:simple_registration:form}
<?php if(!$preferences['username_equals_screen_name']) {?>
    <p>Screen name: <input type="text" name="screen_name"/></p>
<?php } ?>
<?php if(!$preferences['use_email_for_username']) {?>
    <p>Username: <input type="text" name="username"/></p>
<?php } ?>
E-mail: <input type="text" name="email"/>
<?php if(!$preferences['do_not_require_email_confirmation']) {?>
    <p>Confirm E-mail: <input type="text" name="email_confirm"/></p>
<?php } ?>
<?php if(!$preferences['auto_generate_password']) {?>
    <p>Password: <input type="password" name="password"/></p>
<?php } ?>
<?php if(!$preferences['do_not_require_password_confirmation']) {?>
    <p>Confirm Password: <input type="password" name="password_confirm"/></p>
<?php } ?>
<?php if(!$preferences['do_not_require_accept_terms']) {?>
    <p><input type="checkbox" name="accept_terms" value="y"/> I agree to the terms of service</p>
<?php }

if($use_membership_captcha)
{
    ?>
    <p>Submit the word you see below:</p>
    <p>{captcha}</p>
    <p><input type="text" name="captcha"></p>
<?php } ?>

<p><input type="submit" value="Register account"/></p>
{/exp:simple_registration:form}

            </textarea>
        </div>



        <fieldset class="col-group">
            <div class="setting-txt col w-6">
                <h3>Forgot Password Form</h3>
                <em>"Forgot Password?" form that can be used anywhere.</em>
            </div>
            <div class="setting-field col w-10 last">
            <textarea cols="" rows="">
{exp:simple_registration:forgot_password}
    Your email: <input type="text" name="email"/><br/>
    <input type="submit" value="Send me password reset link"/>
{/exp:simple_registration:forgot_password}
            </textarea>
            </div>


        </fieldset>
</form>

</div>
