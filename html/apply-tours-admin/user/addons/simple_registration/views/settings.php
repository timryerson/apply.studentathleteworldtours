<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$this->extend('_wrapper');

?>
<h1>Settings</h1>

<?php
    echo form_open(ee('CP/URL', '/addons/settings/simple_registration/save_settings'));
?>

    <div class="settings">

        <fieldset class="col-group">
            <div class="setting-txt col w-8">
                <h3><?php echo lang('sr_settings_register_member_use_membergroup');?></h3>
                <em><?php echo lang('sr_settings_register_member_use_membergroup_desc');?></em>
            </div>
            <div class="setting-field col w-8 last">
                <?php echo form_dropdown('register_member_use_membergroup', $member_groups, $preferences['register_member_use_membergroup']); ?>
            </div>
        </fieldset>




    </div>

    <h1>Email Settings</h1>
    <div class="settings">
        <h2><p>
                All the regular email settings in EE will apply as usual (activation etc.). In the settings below, you can choose whether or not to send the user an additional email on registration.</p>
            <p>See the documentation for a <a target="_blank" href="http://wedoaddons.com/simple-registration/configuration/settings#available-email-variables">a list of all available email variables</a>. Also, please note that sending out an email with either {password} or {auto_login_link} has <strong>security implications</strong> - read more about that <a target="_blank" href="http://wedoaddons.com/simple-registration/configuration/settings#available-email-variables">here</a>.</a></p>
        </h2>

        <fieldset class="col-group">
            <div class="setting-txt col w-4">
                <h3><?php echo lang('email_password_on_registration');?></h3>
                <em>This setting must be enabled if you want the email below to be sent to new users!</em>
            </div>
            <div class="setting-field col w-12 last">
                <label class="choice <?php if($preferences['email_password_on_registration'] == "1") echo "chosen"; ?>"><input type="checkbox" value="1" name="email_password_on_registration" <?php if($preferences['email_password_on_registration'] == "1") echo 'checked="checked"'?>> Yes</label>
            </div>
        </fieldset>

        <fieldset class="col-group">
            <div class="setting-txt col w-4">
                <h3>E-mail Subject</h3>
                <em>See below for available variables that can be used in the email subject.</em>
            </div>
            <div class="setting-field col w-12 last">
                <?php echo form_input('password_subject', $preferences['password_subject'], "id='password_subject'")?>
            </div>
        </fieldset>

        <fieldset class="col-group">
            <div class="setting-txt col w-4">
                <h3>E-mail Body</h3>
                <em>See below for available variables that can be used in the email body text.</em>
            </div>
            <div class="setting-field col w-12 last">
                <?php echo form_textarea('password_email', $preferences['password_email'], "class='email_body', id='password_email'")?>
            </div>
        </fieldset>

        <fieldset class="col-group">
            <div class="setting-txt col w-4">
                <h3><?php echo lang('sr_settings_redirect_after_login');?></h3>
                <em><?php echo lang('sr_settings_redirect_after_login_desc');?></em>
            </div>
            <div class="setting-field col w-12 last">
                <?php echo form_input('register_member_redirect_to', $preferences['register_member_redirect_to']); ?>
            </div>
        </fieldset>

    </div>


        <div class="txt-wrap">

            <fieldset class="form-ctrls">
                <input class="btn" type="submit" value="Save Settings">
            </fieldset>
        </div>

</form>