<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="col-group">
    <div class="col w-4">
        <div class="box sidebar">
            <h2>Settings</h2>
            <ul>
                <li><a href="<?php echo ee('CP/URL', 'addons/settings/simple_registration/index'); ?>">Introduction</a></li>
                <li><a href="<?php echo ee('CP/URL', 'addons/settings/simple_registration/settings');?>">Settings</a></li>
            </ul>
            <h2>Registration Form Settings</h2>
            <ul>
                <li><a href="<?php echo ee('CP/URL', 'addons/settings/simple_registration/form_settings');?>">Required Fields</a></li>
                <li><a href="<?php echo ee('CP/URL', 'addons/settings/simple_registration/form_code');?>">Form HTML Code</a></li>
                <li><a href="<?php echo ee('CP/URL', 'addons/settings/simple_registration/signup_keys');?>">Signup Keys</a></li>
            </ul>

            <h2>Members</h2>
            <ul>
                <li>
                    <a href="<?php echo ee('CP/URL', 'addons/settings/simple_registration/register_member');?>">Quick Registration</a>
                </li>
            </ul>

        </div>
    </div>
    <div class="col w-12">
        <div class="box">
            <?php echo $child_view?>
        </div>
    </div>
</div>
