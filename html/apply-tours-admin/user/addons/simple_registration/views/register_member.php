<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$this->extend('_wrapper');

?>

<h1>Register new member <span class="req-title">Required Fields</span></h1>

<?php
echo form_open(ee('CP/URL', 'addons/settings/simple_registration/register_member'), 'class="settings"' );
echo form_hidden('registration_form_submitted', 1);
?>

<?php
if(count($errors)) {
    ?>
    <div class="alert inline issue">
        <h3>Error</h3>
        <ul>
<?php
    foreach($errors as $error_msg)
    {
        echo "<li>".$error_msg."</li>";
    }
?>
        </ul>
        <a class="close" href=""></a>
    </div>

<?php
} else if($success) {
    ?>


    <div class="alert inline success">
        <h3>Member created</h3>
        <?php echo $success; ?>
    </div>

    <?php
}
?>





<fieldset class="col-group required">
    <div class="setting-txt col w-8">
        <h3>E-mail</h3>
        <em>New user's email address.</em>
    </div>
    <div class="setting-field col w-8 last">

        <?php echo form_input("email", $email); ?>

    </div>
</fieldset>


<fieldset class="col-group">
    <div class="setting-txt col w-8">
        <h3>Username</h3>
        <em>Will use email if left empty.</em>
    </div>
    <div class="setting-field col w-8 last">

        <?php echo form_input(
            array(
                'name' => 'username',
                'placeholder' => 'Will use email if left empty',
                'value' => $username,
            )
        ); ?>

    </div>
</fieldset>

<fieldset class="col-group">
    <div class="setting-txt col w-8">
        <h3>Screen name</h3>
        <em>If left empty, username will be used. If username is also left empty email will be used.</em>
    </div>
    <div class="setting-field col w-8 last">

        <?php echo form_input(
            array(
                'name' => 'screen_name',
                'placeholder' => 'Will use username OR email if left empty',
                'value' => $screen_name,
            )
        ); ?>

    </div>
</fieldset>

<fieldset class="col-group">
    <div class="setting-txt col w-8">
        <h3>Password</h3>
        <em>A random password has been generated for you.</em>
    </div>
    <div class="setting-field col w-8 last">

        <?php echo form_input("password", $password); ?>

    </div>
</fieldset>

<?php
if(!isset($preferences['register_member_use_membergroup']) || $preferences['register_member_use_membergroup'] == 0) {
    ?>
    <fieldset class="col-group">
        <div class="setting-txt col w-8">
            <h3>Member Group</h3>
            <em>Assign user to a member group.</em>
        </div>
        <div class="setting-field col w-8 last">

            <?php echo form_dropdown("member_group_id", $member_groups); ?>

        </div>
    </fieldset>



    <?php
}
?>

<fieldset class="form-ctrls">
    <input class="btn" type="submit" value="Create New User">
</fieldset>


</form>
