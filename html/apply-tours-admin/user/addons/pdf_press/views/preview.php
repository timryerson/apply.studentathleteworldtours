<style>
    .ok {color:green;}
    .warning {color:orange;}
    .failed {color:red;}
</style>
<div style="padding: 10px;">
    <?= form_open(ee('CP/URL')->make('addons/settings/pdf_press/preview')); ?>
    <table class="mainTable">
        <thead>
            <tr>
                <th colspan="2" class="even">
                    <?=lang('preview')?>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="odd" colspan="2">
                    <p><?=lang('preview_desc')?></p>
                </td>
            </tr>
            <tr>
                <td class="odd">
                    <label for="size">Size:</label><br/>
                    <select id="size" name="size">
                        <?php foreach ($paper_sizes as $paper) : ?>
                                <option <?= (($size == "" && $paper == 'Letter') ? 'selected' : ($size == $paper ? "selected" : '')) ?> value="<?=$paper?>"><?=$paper?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
                <td class="even">
                    <label for="orientation">Orientation:</label><br/>
                    <select id="orientation" name="orientation">
                        <option <?= ($orientation == "portrait" ? "selected" : "") ?> value="portrait">Portrait (default)</option>
                        <option <?= ($orientation == "landscape" ? "selected" : "") ?> value="landscape">Landscape</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="odd">
                    <label for="url">EE Template Path (ex. site/test):</label>
                    <input type="text" id="url" name="path" style="width:500px;" value="<?=$url_path?>" class="search" />
                </td>
            </tr>
        </tbody>
    </table>
    <button id="get_pdf" type="submit" class="submit"><?=lang('submit')?></button>
    </form>
    <p>&nbsp;</p>
    <iframe id="preview" width="100%" height="600" name="preview" src="<?= $dom_path?>" frameborder="0" marginheight="0" marginwidth="0"></iframe>
</div>
