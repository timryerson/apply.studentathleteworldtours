<p>Place the font files in <?=$theme_path?>. You should add names like `filename.ttf` below.</p>
<?=form_open($action_url);?>
<table>
<?php
foreach ($data as $key => $val) {
    echo("<tr><td>".lang($key, $key)."</td><td>".$val."</td></tr>");
}
?>
</table>
<p><?=form_submit('submit', lang('submit'), 'class="submit"')?></p>
<?php
    form_close();
?>
