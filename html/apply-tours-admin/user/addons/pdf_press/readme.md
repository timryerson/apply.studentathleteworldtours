# PDF PRESS

Generate PDFs directly from your EE templates! Powered by [https://mpdf.github.io/](mPDF).

## PREREQUISITES

PHP 7.3+
DOMDocument extension
MBString extension
XML extension
GD
ExpressionEngine 6.0+
Opcache and IMagick or GMagick (recommended)

## INSTALLATION

1. Copy entire `pdf_press` folder to your `system/user/addons` folder.
2. Copy entire `themes/pdf_press` folder to your themes folder, so that you have `/themes/users/pdf_press`
3. On your EE backend, navigate to `Developer > Addons` (yoursite.com/admin.php?/cp/addons).
4. Scroll to `Third Party Add-Ons`.
5. Find `PDF Press` and click `Install`.
6. Add your settings, and enjoy!

### Upgrading From Previous Versions

PDF Press V5 now requires a `themes` folder. If you are upgrading, ensure you follow step 2 in the installation instructions.

## USAGE

PDF Press has two tags, each will generate a PDF from an ExpressionEngine template.

### save_to_pdf: Generate PDF

```
{exp:pdf_press:save_to_pdf}
```

The `save_to_pdf` tag generates a link to download the template as a PDF, it contains the following parameters:

- **path**: Accepts an ExpressionEngine template path in the form template_group/template (ex. site/pdf), if no path is given then the current page will be used.

- **filename**: Sets the filename for the downloaded PDF. Default: filename.pdf

- **size**: sets the paper size for the PDF (ex. letter, legal, a5, etc). Paper sizes are listed below.

- **orientation**: sets the orientation of the page, either portrait (default) or landscape. Default: portrait.

- **attachment**: Automatically downloads the generated PDF. If the attachment parameter is set to 0 the PDF will be opened in the browser. Default: 1

- **key**: Optional setting preset shortcode. See Settings Presets for more information.

- **default_font**: Default font family to use in your PDF. This must match your font name inside of your add-on settings.

Example:
```
<a href="{exp:pdf_press:save_to_pdf path='site/projects' attachment='0' filename='projects.pdf'}">Download Project Case Studies</a>
```

This will create a link to open a PDF titled "projects.pdf" in the browser using the site/projects template

### parse_pdf: Parse Inline Template to PDF

```
{exp:pdf_press:parse_pdf}
```

Sometimes you'll want to create a PDF with a member only or logged in page. For this you want to use parse_pdf:

```
{exp:pdf_press:parse_pdf}
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Parse Test</title>
    </head>
    <body>
        {if logged_in}
            {exp:channel:entries channel="Projects"}
                <p><strong>{title}</strong></p>
            {/exp:channel:entries}
        {if:else}
            <p>Not logged in</p>
        {/if}
    </body>
    </html>
{/exp:pdf_press:parse_pdf}
```

As you can guess, this tag, instead of creating a link to download/open a PDF, will create a PDF using whatever HTML & EE tags that are inside the parse_pdf tag-pair. It has all of the same parameters as `save_to_pdf` (except for path)

**Note: Remember to put the parse_pdf tag before and after the HTML tag (and DOCTYPE) otherwise PDF Press won't render your complete page)**

## SETTINGS

Presets are a great way to save time, they allow you to set all of the options passed as parameters in the save_to_pdf & parse_pdf tags in the Control Panel. You can then re-use those settings in your template by just passing the short code into the "key" parameter.

`{exp:save_to_pdf key="product_settings"}`

Presets also let you securely set a variety of security settings for your PDFs. You can enable encryption, set a owner password, user password and then control if users can save, print, or modify PDFs.

You can access the Setting Presets by going to `Add-Ons > Modules > PDF Press > Setting Presets`

## FONT SUPPORT

Custom fonts are supported inside of PDF press. Fonts must be of the TrueType format (ttf), and must be placed in `themes/user/pdf_press/fonts`.

### Adding a Font

Add the font in your settings. Include a name for that font that will refer to your font when you use it in your CSS. Please note, this name should be lowercase and include only dashes (no spaces or punctuation); the settings manager will do this for you if you forget.

Include the names of a regular, bold, and italic font TTF file that is included in your `themes/users/pdf_press/fonts` folder.

### Using a Font

Fonts are applied to your text in a few different ways:

#### CSS

All CSS in PDF Press is inlined as its read, so you can apply a font as you would on HTML. Ensure the `font-family` on your element is applied with the same name that appears in the fonts settings.

#### Default Font

If you are applying the font across the entirety of your document, you can use the `default_font` parameter as indicated above. Ensure the `font-family` on your element is applied with the same name that appears in the fonts settings.

## PARAMETERS

PDF Press uses mPDF as it's engine, and accepts all parameters. See [https://mpdf.github.io/reference/mpdf-variables/overview.html](the mPDF documentation) for more info.
All additional mpdf parameters can be added to your EE tag as such:

### Pagination

- **mirror-margins**: [https://mpdf.github.io/reference/mpdf-variables/mirror-margins.html](mirrorMargins settings)
- **restore-block-pagebreaks**: [https://mpdf.github.io/reference/mpdf-variables/restore-block-pagebreaks.html](restoreBlockPagebreaks settings)
- **force-portrait-margins**: [https://mpdf.github.io/reference/mpdf-variables/force-portrait-margins.html](forcePortraitMargins settings)
- **display-default-orientation**: [https://mpdf.github.io/reference/mpdf-variables/display-default-orientation.html](displayDefaultOrientation settings)
- **auto-page-break**: [https://mpdf.github.io/reference/mpdf-variables/auto-page-break.html](autoPageBreak settings)
- **set-auto-top-margin**: [https://mpdf.github.io/reference/mpdf-variables/set-auto-top-margin.html](setAutoTopMargin settings)
- **set-auto-bottom-margin**: [https://mpdf.github.io/reference/mpdf-variables/set-auto-bottom-margin.html](setAutoBottomMargin settings)
- **auto-margin-padding**: [https://mpdf.github.io/reference/mpdf-variables/auto-margin-padding.html](autoMarginPadding settings)
- **marg-buffer**: [https://mpdf.github.io/reference/mpdf-variables/marg-buffer.html](margBuffer settings)
- **printers-info**: [https://mpdf.github.io/reference/mpdf-variables/printers-info.html](printers_info settings)
- **bleed-margin**: [https://mpdf.github.io/reference/mpdf-variables/bleed-margin.html](bleedMargin settings)
- **cross-mark-margin**: [https://mpdf.github.io/reference/mpdf-variables/cross-mark-margin.html](crossMarkMargin settings)
- **crop-mark-margin**: [https://mpdf.github.io/reference/mpdf-variables/crop-mark-margin.html](cropMarkMargin settings)
- **crop-mark-length**: [https://mpdf.github.io/reference/mpdf-variables/crop-mark-length.html](cropMarkLength settings)
- **non-print-margin**: [https://mpdf.github.io/reference/mpdf-variables/non-print-margin.html](nonPrintMargin settings)

### Page Numbering
- **alias-nb-pg**: [https://mpdf.github.io/reference/mpdf-variables/alias-nb-pg.html](aliasNbPg settings)
- **alias-nb-pg-gp**: [https://mpdf.github.io/reference/mpdf-variables/alias-nb-pg-gp.html](aliasNbPgGp settings)
- **pagenum-prefix**: [https://mpdf.github.io/reference/mpdf-variables/pagenum-prefix.html](pagenumPrefix settings)
- **pagenum-suffix**: [https://mpdf.github.io/reference/mpdf-variables/pagenum-suffix.html](pagenumSuffix settings)
- **nbpg-prefix**: [https://mpdf.github.io/reference/mpdf-variables/nbpg-prefix.html](nbpgPrefix settings)
- **nbpg-suffix**: [https://mpdf.github.io/reference/mpdf-variables/nbpg-suffix.html](nbpgSuffix settings)
- **default-page-num-style**: [https://mpdf.github.io/reference/mpdf-variables/default-page-num-style.html](defaultPageNumStyle settings)

### Fonts, Languages and Character sets
- **percent-subset**: [https://mpdf.github.io/reference/mpdf-variables/percent-subset.html](percentSubset settings)
- **use-kerning**: [https://mpdf.github.io/reference/mpdf-variables/use-kerning.html](useKerning settings)
- **max-ttffilesize**: [https://mpdf.github.io/reference/mpdf-variables/max-ttffilesize.html](maxTTFFilesize settings)
- **allow-charset-conversion**: [https://mpdf.github.io/reference/mpdf-variables/allow-charset-conversion.html](allow_charset_conversion settings)
- **charset-in**: [https://mpdf.github.io/reference/mpdf-variables/charset-in.html](charset_in settings)
- **bi-directional**: [https://mpdf.github.io/reference/mpdf-variables/bi-directional.html](biDirectional settings)
- **use-adobe-cjk**: [https://mpdf.github.io/reference/mpdf-variables/use-adobe-cjk.html](useAdobeCJK settings)
- **auto-font-group-size**: [https://mpdf.github.io/reference/mpdf-variables/auto-font-group-size.html](autoFontGroupSize settings)
- **use-lang**: [https://mpdf.github.io/reference/mpdf-variables/use-lang.html](useLang settings)
- **auto-script-to-lang**: [https://mpdf.github.io/reference/mpdf-variables/auto-script-to-lang.html](autoScriptToLang settings)
- **auto-lang-to-font**: [https://mpdf.github.io/reference/mpdf-variables/auto-lang-to-font.html](autoLangToFont settings)
- **base-script**: [https://mpdf.github.io/reference/mpdf-variables/base-script.html](baseScript settings)
- **auto-arabic**: [https://mpdf.github.io/reference/mpdf-variables/auto-arabic.html](autoArabic settings)
- **auto-vietnamese**: [https://mpdf.github.io/reference/mpdf-variables/auto-vietnamese.html](autoVietnamese settings)
- **false-bold-weight**: [https://mpdf.github.io/reference/mpdf-variables/false-bold-weight.html](falseBoldWeight settings)
- **sm-caps-scale**: [https://mpdf.github.io/reference/mpdf-variables/sm-caps-scale.html](smCapsScale settings)
- **sm-caps-stretch**: [https://mpdf.github.io/reference/mpdf-variables/sm-caps-stretch.html](smCapsStretch settings)
- **backup-subs-font**: [https://mpdf.github.io/reference/mpdf-variables/backup-subs-font.html](backupSubsFont settings)
- **backup-sipfont**: [https://mpdf.github.io/reference/mpdf-variables/backup-sipfont.html](backupSIPFont settings)

### Configuration
- **repackage-ttf**: [https://mpdf.github.io/reference/mpdf-variables/repackage-ttf.html](repackageTTF settings)
- **use-substitutions**: [https://mpdf.github.io/reference/mpdf-variables/use-substitutions.html](useSubstitutions settings)
- **collapse-block-margins**: [https://mpdf.github.io/reference/mpdf-variables/collapse-block-margins.html](collapseBlockMargins settings)
- **dpi**: [https://mpdf.github.io/reference/mpdf-variables/dpi.html](dpi settings)
- **enable-imports**: [https://mpdf.github.io/reference/mpdf-variables/enable-imports.html](enableImports settings)
- **allow-output-buffering**: [https://mpdf.github.io/reference/mpdf-variables/allow-output-buffering.html](allow_output_buffering settings)
- **allow-html-optional-endtags**: [https://mpdf.github.io/reference/mpdf-variables/allow-html-optional-endtags.html](allow_html_optional_endtags settings)
- **ignore-invalid-utf8**: [https://mpdf.github.io/reference/mpdf-variables/ignore-invalid-utf8.html](ignore_invalid_utf8 settings)
- **text-input-as-html**: [https://mpdf.github.io/reference/mpdf-variables/text-input-as-html.html](text_input_as_HTML settings)
- **progress-bar**: [https://mpdf.github.io/reference/mpdf-variables/progress-bar.html](progressBar settings)
- **progbar-heading**: [https://mpdf.github.io/reference/mpdf-variables/progbar-heading.html](progbar_heading settings)
- **progbar-alt-html**: [https://mpdf.github.io/reference/mpdf-variables/progbar-alt-html.html](progbar_altHTML settings)
- **increment-fpr1 [1-4]**: [https://mpdf.github.io/reference/mpdf-variables/increment-fpr1 [1-4].html](incrementFPR[1-4] settings)

### Debugging
- **debug**: [https://mpdf.github.io/reference/mpdf-variables/debug.html](debug settings)
- **debugfonts**: [https://mpdf.github.io/reference/mpdf-variables/debugfonts.html](debugfonts settings)
- **show-image-errors**: [https://mpdf.github.io/reference/mpdf-variables/show-image-errors.html](showImageErrors settings)

### PDF/A1-b, PDF/X-1a Colorspaces
- **pdfa**: [https://mpdf.github.io/reference/mpdf-variables/pdfa.html](PDFA settings)
- **pdfaauto**: [https://mpdf.github.io/reference/mpdf-variables/pdfaauto.html](PDFAauto settings)
- **pdfx**: [https://mpdf.github.io/reference/mpdf-variables/pdfx.html](PDFX settings)
- **pdfxauto**: [https://mpdf.github.io/reference/mpdf-variables/pdfxauto.html](PDFXauto settings)
- **iccprofile**: [https://mpdf.github.io/reference/mpdf-variables/iccprofile.html](ICCProfile settings)
- **restrict-color-space**: [https://mpdf.github.io/reference/mpdf-variables/restrict-color-space.html](restrictColorSpace settings)

### Annotations
- **title2annots**: [https://mpdf.github.io/reference/mpdf-variables/title2annots.html](title2annots settings)
- **annot-margin**: [https://mpdf.github.io/reference/mpdf-variables/annot-margin.html](annotMargin settings)
- **annot-opacity**: [https://mpdf.github.io/reference/mpdf-variables/annot-opacity.html](annotOpacity settings)

### Bookmarks (Outlines)
- **anchor2bookmark**: [https://mpdf.github.io/reference/mpdf-variables/anchor2bookmark.html](anchor2Bookmark settings)
- **h2bookmarks**: [https://mpdf.github.io/reference/mpdf-variables/h2bookmarks.html](h2bookmarks settings)
- **h2toc**: [https://mpdf.github.io/reference/mpdf-variables/h2toc.html](h2toc settings)

### CSS & Styles
- **cssselect-media**: [https://mpdf.github.io/reference/mpdf-variables/cssselect-media.html](CSSselectMedia settings)

### Page Headers & Footers
- **defaultfooterfontsize**: [https://mpdf.github.io/reference/mpdf-variables/defaultfooterfontsize.html](defaultfooterfontsize settings)
- **defaultfooterfontstyle**: [https://mpdf.github.io/reference/mpdf-variables/defaultfooterfontstyle.html](defaultfooterfontstyle settings)
- **defaultfooterline**: [https://mpdf.github.io/reference/mpdf-variables/defaultfooterline.html](defaultfooterline settings)
- **defaultheaderfontsize**: [https://mpdf.github.io/reference/mpdf-variables/defaultheaderfontsize.html](defaultheaderfontsize settings)
- **defaultheaderfontstyle**: [https://mpdf.github.io/reference/mpdf-variables/defaultheaderfontstyle.html](defaultheaderfontstyle settings)
- **defaultheaderline**: [https://mpdf.github.io/reference/mpdf-variables/defaultheaderline.html](defaultheaderline settings)
- **footer-line-spacing**: [https://mpdf.github.io/reference/mpdf-variables/footer-line-spacing.html](footer_line_spacing settings)
- **header-line-spacing**: [https://mpdf.github.io/reference/mpdf-variables/header-line-spacing.html](header_line_spacing settings)
- **force-portrait-headers**: [https://mpdf.github.io/reference/mpdf-variables/force-portrait-headers.html](forcePortraitHeaders settings)

### Tables
- **simple-tables**: [https://mpdf.github.io/reference/mpdf-variables/simple-tables.html](simpleTables settings)
- **pack-table-data**: [https://mpdf.github.io/reference/mpdf-variables/pack-table-data.html](packTableData settings)
- **table-min-size-priority**: [https://mpdf.github.io/reference/mpdf-variables/table-min-size-priority.html](tableMinSizePriority settings)
- **ignore-table-percents**: [https://mpdf.github.io/reference/mpdf-variables/ignore-table-percents.html](ignore_table_percents settings)
- **ignore-table-widths**: [https://mpdf.github.io/reference/mpdf-variables/ignore-table-widths.html](ignore_table_widths settings)
- **keep-table-proportions**: [https://mpdf.github.io/reference/mpdf-variables/keep-table-proportions.html](keep_table_proportions settings)
- **shrink-tables-to-fit**: [https://mpdf.github.io/reference/mpdf-variables/shrink-tables-to-fit.html](shrink_tables_to_fit settings)
- **table-error-report**: [https://mpdf.github.io/reference/mpdf-variables/table-error-report.html](table_error_report settings)
- **table-error-report-param**: [https://mpdf.github.io/reference/mpdf-variables/table-error-report-param.html](table_error_report_param settings)
- **use-kwt**: [https://mpdf.github.io/reference/mpdf-variables/use-kwt.html](use_kwt settings)
- **iteration-counter**: [https://mpdf.github.io/reference/mpdf-variables/iteration-counter.html](iterationCounter settings)
- **decimal-align**: [https://mpdf.github.io/reference/mpdf-variables/decimal-align.html](decimal_align settings)

### Images
- **img-dpi**: [https://mpdf.github.io/reference/mpdf-variables/img-dpi.html](img_dpi settings)

### Text Spacing & Justification
- **normal-lineheight**: [https://mpdf.github.io/reference/mpdf-variables/normal-lineheight.html](normalLineheight settings)
- **use-fixed-normal-line-height**: [https://mpdf.github.io/reference/mpdf-variables/use-fixed-normal-line-height.html](useFixedNormalLineHeight settings)
- **use-fixed-text-baseline**: [https://mpdf.github.io/reference/mpdf-variables/use-fixed-text-baseline.html](useFixedTextBaseline settings)
- **adjust-font-desc-lineheight**: [https://mpdf.github.io/reference/mpdf-variables/adjust-font-desc-lineheight.html](adjustFontDescLineheight settings)
- **j-smax-char**: [https://mpdf.github.io/reference/mpdf-variables/j-smax-char.html](jSmaxChar settings)
- **j-smax-char-last**: [https://mpdf.github.io/reference/mpdf-variables/j-smax-char-last.html](jSmaxCharLast settings)
- **j-smax-word-last**: [https://mpdf.github.io/reference/mpdf-variables/j-smax-word-last.html](jSmaxWordLast settings)
- **j-sword**: [https://mpdf.github.io/reference/mpdf-variables/j-sword.html](jSWord settings)
- **allow-cjkorphans**: [https://mpdf.github.io/reference/mpdf-variables/allow-cjkorphans.html](allowCJKorphans settings)
- **allow-cjkoverflow**: [https://mpdf.github.io/reference/mpdf-variables/allow-cjkoverflow.html](allowCJKoverflow settings)
- **cjkforceend**: [https://mpdf.github.io/reference/mpdf-variables/cjkforceend.html](CJKforceend settings)
- **tab-spaces**: [https://mpdf.github.io/reference/mpdf-variables/tab-spaces.html](tabSpaces settings)
- **justify-b4br**: [https://mpdf.github.io/reference/mpdf-variables/justify-b4br.html](justifyB4br settings)

### Hyphenation
- **shylang**: [https://mpdf.github.io/reference/mpdf-variables/shylang.html](SHYlang settings)

### Columns
- **keep-columns**: [https://mpdf.github.io/reference/mpdf-variables/keep-columns.html](keepColumns settings)
- **max-col-h-correction**: [https://mpdf.github.io/reference/mpdf-variables/max-col-h-correction.html](max_colH_correction settings)

### Lists
- **list-align-style**: [https://mpdf.github.io/reference/mpdf-variables/list-align-style.html](list_align_style settings)
- **list-auto-mode**: [https://mpdf.github.io/reference/mpdf-variables/list-auto-mode.html](list_auto_mode settings)
- **list-indent-default**: [https://mpdf.github.io/reference/mpdf-variables/list-indent-default.html](list_indent_default settings)
- **list-indent-default-mpdf**: [https://mpdf.github.io/reference/mpdf-variables/list-indent-default-mpdf.html](list_indent_default_mpdf settings)
- **list-indent-first-level**: [https://mpdf.github.io/reference/mpdf-variables/list-indent-first-level.html](list_indent_first_level settings)
- **list-marker-offset**: [https://mpdf.github.io/reference/mpdf-variables/list-marker-offset.html](list_marker_offset settings)
- **list-number-suffix**: [https://mpdf.github.io/reference/mpdf-variables/list-number-suffix.html](list_number_suffix settings)
- **list-symbol-size**: [https://mpdf.github.io/reference/mpdf-variables/list-symbol-size.html](list_symbol_size settings)

### Watermarks
- **show-watermark-image**: [https://mpdf.github.io/reference/mpdf-variables/show-watermark-image.html](showWatermarkImage settings)
- **show-watermark-text**: [https://mpdf.github.io/reference/mpdf-variables/show-watermark-text.html](showWatermarkText settings)
- **watermark-font**: [https://mpdf.github.io/reference/mpdf-variables/watermark-font.html](watermark_font settings)
- **watermark-angle**: [https://mpdf.github.io/reference/mpdf-variables/watermark-angle.html](watermarkAngle settings)
- **watermark-image-alpha**: [https://mpdf.github.io/reference/mpdf-variables/watermark-image-alpha.html](watermarkImageAlpha settings)
- **watermark-img-alpha-blend**: [https://mpdf.github.io/reference/mpdf-variables/watermark-img-alpha-blend.html](watermarkImgAlphaBlend settings)
- **watermark-img-behind**: [https://mpdf.github.io/reference/mpdf-variables/watermark-img-behind.html](watermarkImgBehind settings)
- **watermark-text-alpha**: [https://mpdf.github.io/reference/mpdf-variables/watermark-text-alpha.html](watermarkTextAlpha settings)

### Bookmarks
- **bookmark-styles**: [https://mpdf.github.io/reference/mpdf-variables/bookmark-styles.html](bookmarkStyles settings)

## SUPPORT

For any issues, please reach out to [mailto:support@triplenerdscore.net](support@triplenerdscore.net)