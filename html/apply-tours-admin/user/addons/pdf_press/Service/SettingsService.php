<?php

namespace Anecka\PDFPress\Service;

class SettingsService
{
    public $site_id = 1;

    private $mpdfSettings = [
        'adjust-font-desc-lineheight' => 'adjustFontDescLineheight',
        'alias-nb-pg' => 'aliasNbPg',
        'alias-nb-pg-gp' => 'aliasNbPgGp',
        'allow-charset-conversion' => 'allow_charset_conversion',
        'allow-html-optional-endtags' => 'allow_html_optional_endtags',
        'allow-output-buffering' => 'allow_output_buffering',
        'allow-cjkorphans' => 'allowCJKorphans',
        'allow-cjkoverflow' => 'allowCJKoverflow',
        'anchor2bookmark' => 'anchor2Bookmark',
        'annot-margin' => 'annotMargin',
        'annot-opacity' => 'annotOpacity',
        'auto-arabic' => 'autoArabic',
        'auto-font-group-size' => 'autoFontGroupSize',
        'auto-lang-to-font' => 'autoLangToFont',
        'auto-margin-padding' => 'autoMarginPadding',
        'auto-page-break' => 'autoPageBreak',
        'auto-script-to-lang' => 'autoScriptToLang',
        'auto-vietnamese' => 'autoVietnamese',
        'backup-subs-font' => 'backupSubsFont',
        'backup-sipfont' => 'backupSIPFont',
        'base-script' => 'baseScript',
        'bi-directional' => 'biDirectional',
        'bleed-margin' => 'bleedMargin',
        'bookmark-styles' => 'bookmarkStyles',
        'charset-in' => 'charset_in',
        'cjkforceend' => 'CJKforceend',
        'collapse-block-margins' => 'collapseBlockMargins',
        'crop-mark-length' => 'cropMarkLength',
        'crop-mark-margin' => 'cropMarkMargin',
        'cross-mark-margin' => 'crossMarkMargin',
        'curl-allow-unsafe-ssl-requests' => 'curlAllowUnsafeSslRequests',
        'curl-timeout' => 'curlTimeout',
        'curl-follow-location' => 'curlFollowLocation',
        'cssselect-media' => 'CSSselectMedia',
        'decimal-align' => 'decimal_align',
        'debug' => 'debug',
        'debugfonts' => 'debugfonts',
        'defaultfooterfontsize' => 'defaultfooterfontsize',
        'defaultfooterfontstyle' => 'defaultfooterfontstyle',
        'defaultfooterline' => 'defaultfooterline',
        'defaultheaderfontsize' => 'defaultheaderfontsize',
        'defaultheaderfontstyle' => 'defaultheaderfontstyle',
        'defaultheaderline' => 'defaultheaderline',
        'display-default-orientation' => 'displayDefaultOrientation',
        'default-page-num-style' => 'defaultPageNumStyle',
        'dpi' => 'dpi',
        'enable-imports' => 'enableImports',
        'expose-version' => 'exposeVersion',
        'false-bold-weight' => 'falseBoldWeight',
        'footer-line-spacing' => 'footer_line_spacing',
        'fontdata' => 'fontdata',
        'force-portrait-headers' => 'forcePortraitHeaders',
        'force-portrait-margins' => 'forcePortraitMargins',
        'header-line-spacing' => 'header_line_spacing',
        'h2bookmarks' => 'h2bookmarks',
        'h2toc' => 'h2toc',
        'iccprofile' => 'ICCProfile',
        'ignore-invalid-utf8' => 'ignore_invalid_utf8',
        'ignore-table-percents' => 'ignore_table_percents',
        'ignore-table-widths' => 'ignore_table_widths',
        'img-dpi' => 'img_dpi',
        'increment-fpr1' => 'incrementFPR1',
        'increment-fpr2' => 'incrementFPR2',
        'increment-fpr3' => 'incrementFPR3',
        'increment-fpr4' => 'incrementFPR4',
        'iteration-counter' => 'iterationCounter',
        'j-smax-char' => 'jSmaxChar',
        'j-smax-char-last' => 'jSmaxCharLast',
        'j-smax-word-last' => 'jSmaxWordLast',
        'j-sword' => 'jSWord',
        'justify-b4br' => 'justifyB4br',
        'keep-columns' => 'keepColumns',
        'keep-table-proportions' => 'keep_table_proportions',
        'list-align-style' => 'list_align_style',
        'list-auto-mode' => 'list_auto_mode',
        'list-indent-default' => 'list_indent_default',
        'list-indent-default-mpdf' => 'list_indent_default_mpdf',
        'list-indent-first-level' => 'list_indent_first_level',
        'list-marker-offset' => 'list_marker_offset',
        'list-number-suffix' => 'list_number_suffix',
        'list-symbol-size' => 'list_symbol_size',
        'marg-buffer' => 'margBuffer',
        'max-col-h-correction' => 'max_colH_correction',
        'max-ttffilesize' => 'maxTTFFilesize',
        'mirror-margins' => 'mirrorMargins',
        'nbpg-prefix' => 'nbpgPrefix',
        'nbpg-suffix' => 'nbpgSuffix',
        'non-print-margin' => 'nonPrintMargin',
        'normal-lineheight' => 'normalLineheight',
        'pack-table-data' => 'packTableData',
        'pagenum-prefix' => 'pagenumPrefix',
        'pagenum-suffix' => 'pagenumSuffix',
        'pdfa' => 'PDFA',
        'pdfaauto' => 'PDFAauto',
        'pdfx' => 'PDFX',
        'pdfxauto' => 'PDFXauto',
        'percent-subset' => 'percentSubset',
        'printers-info' => 'printers_info',
        'progbar-alt-html' => 'progbar_altHTML',
        'progbar-heading' => 'progbar_heading',
        'progress-bar' => 'progressBar',
        'repackage-ttf' => 'repackageTTF',
        'restore-block-pagebreaks' => 'restoreBlockPagebreaks',
        'restrict-color-space' => 'restrictColorSpace',
        'set-auto-bottom-margin' => 'setAutoBottomMargin',
        'set-auto-top-margin' => 'setAutoTopMargin',
        'show-image-errors' => 'showImageErrors',
        'show-stats' => 'showStats',
        'show-watermark-image' => 'showWatermarkImage',
        'show-watermark-text' => 'showWatermarkText',
        'shrink-tables-to-fit' => 'shrink_tables_to_fit',
        'shylang' => 'SHYlang',
        'simple-tables' => 'simpleTables',
        'sm-caps-scale' => 'smCapsScale',
        'sm-caps-stretch' => 'smCapsStretch',
        'table-error-report' => 'table_error_report',
        'table-error-report-param' => 'table_error_report_param',
        'table-min-size-priority' => 'tableMinSizePriority',
        'tab-spaces' => 'tabSpaces',
        'text-input-as-html' => 'text_input_as_HTML',
        'title2annots' => 'title2annots',
        'use-kwt' => 'use_kwt',
        'use-adobe-cjk' => 'useAdobeCJK',
        'use-dictionary-lbr' => 'useDictionaryLBR',
        'use-fixed-normal-line-height' => 'useFixedNormalLineHeight',
        'use-fixed-text-baseline' => 'useFixedTextBaseline',
        'use-graphs' => 'useGraphs',
        'use-kerning' => 'useKerning',
        'use-lang' => 'useLang',
        'use-substitutions' => 'useSubstitutions',
        'use-tibetan-lbr' => 'useTibetanLBR',
        'watermark-font' => 'watermark_font',
        'watermark-angle' => 'watermarkAngle',
        'watermark-image-alpha' => 'watermarkImageAlpha',
        'watermark-img-alpha-blend' => 'watermarkImgAlphaBlend',
        'watermark-img-behind' => 'watermarkImgBehind',
        'watermark-text-alpha' => 'watermarkTextAlpha',
    ];

    private $primarySettings = [
        'format' => [
            'options' => [

            ],
            'default' => 'Letter',
        ],
        'orientation' => [
            'options' => [
                'portrait',
                'landscape',
            ],
            'default' => 'portrait',
        ],
        'mode' => [
            'options' => [
                'portrait',
                'landscape',
            ],
            'default' => 'utf-8',
        ],
        'CSSselectMedia' => [
            'print',
            'screen',
            'all'
        ],
    ];

    private $cachePath;

    public function __construct()
    {
        $this->site_id = ee()->config->item('site_id');
        $this->cachePath = SYSPATH . '/user/cache/pdf_press/temp';
    }

    public function getSettings($siteId = null)
    {
        if (!$siteId) {
            $siteId = ee()->config->item('site_id');
        }

        $settings = ee('Model')->get('pdf_press:PdfPressSetting')
                    ->filter('site_id', $siteId)
                    ->all();

        return $settings;
    }

    public function getSetting($id)
    {
        $settings = ee('Model')->get('pdf_press:PdfPressSetting')
                    ->filter('id', $id)
                    ->first();

        return $settings;
    }

    public function mapSettings($settings)
    {
        if (isset(ee()->TMPL)) {
            $tagparams = ee()->TMPL->tagparams;

            foreach ($tagparams as $tagparamKey => $tagParamVal) {
                if (array_key_exists($tagparamKey, $this->mpdfSettings)) {
                    $settings[$tagparamKey] = ee()->TMPL->fetch_param($tagParamVal);
                }
            }
            foreach ($this->primarySettings as $psKey => $psVal) {
                $settings[$psKey] = array_key_exists($psKey, $tagparams)
                    ? $tagparams[$psKey]
                    : ($psVal['default'] ?? '');
            }
        }

        $settings['tempDir'] = $this->cachePath;

        return $settings;
    }

    public function newSettingForm()
    {
        $action_url = ee('CP/URL', 'addons/settings/pdf_press/save_setting');
        $paper_sizes = ee('pdf_press:EnumService')->paperSizes();
        $sizes = [];

        foreach ($paper_sizes as $index => $size) {
            $sizes[$size] = $size;
        }

        $data = [
            'key'           => form_hidden('id', '').form_input('key', ''),
            'attachment'    => "<span style='padding-left:10px;'>".form_radio('attachment', '1', true)." ".lang('Download')."</span><span style='padding-left:10px;'>".form_radio('attachment', '0', false)." ".lang('Browser')."</span>",//form_input('attachment', ''),
            'size'          => form_dropdown('size', $sizes, 'Letter'),
            'orientation'   => "<span style='padding-left:10px;'>".form_radio('orientation', 'portrait', true)." ".lang('Portrait')."</span><span style='padding-left:10px;'>".form_radio('orientation', 'landscape', false)." ".lang('Landscape')."</span>",//form_input('orientation', ''),
            'filename'      => form_input('filename', ''),
            'cache_enabled' => "<span style='padding-left:10px;'>".form_radio('cache_enabled', '1', false)." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('cache_enabled', '0', true)." ".lang('No')."</span>",
            'cache_ttl'     => form_input('cache_ttl', ''),
            ''              => lang('encrypt_description'),
            'encrypt'       => "<span style='padding-left:10px;'>".form_radio('encrypt', '1', false)." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('encrypt', '0', true)." ".lang('No')."</span>",
            'userpass'      => form_input('userpass', ''),
            'ownerpass'     => form_input('ownerpass', ''),
            'can_print'     => "<span style='padding-left:10px;'>".form_radio('can_print', '1', true)." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('can_print', '0', false)." ".lang('No')."</span>",
            'can_modify'    => "<span style='padding-left:10px;'>".form_radio('can_modify', '1', true)." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('can_modify', '0', false)." ".lang('No')."</span>", //form_checkbox('can_modify', 'yes'),
            'can_copy'      => "<span style='padding-left:10px;'>".form_radio('can_copy', '1', true)." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('can_copy', '0', false)." ".lang('No')."</span>",
            'can_add'       => "<span style='padding-left:10px;'>".form_radio('can_add', '1', true)." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('can_add', '0', false)." ".lang('No')."</span>",
        ];

        return $data;
    }

    public function editSettingForm($id, $settings)
    {
        $action_url = ee('CP/URL', 'addons/settings/pdf_press/save_setting');
        $paper_sizes = ee('pdf_press:EnumService')->paperSizes();
        $sizes = [];

        foreach ($paper_sizes as $index => $size) {
            $sizes[$size] = $size;
        }

        $data = [
            'key'           => form_hidden('id', $id).form_input('key', $settings['key']),
            'attachment'    => "<span style='padding-left:10px;'>".form_radio('attachment', '1', $settings['attachment'] == 1)." ".lang('Download')."</span><span style='padding-left:10px;'>".form_radio('attachment', '0', $settings['attachment'] == 0)." ".lang('Browser')."</span>",//form_input('attachment', ''),
            'size'          => form_dropdown('size', $sizes, $settings['size']),
            'orientation'   => "<span style='padding-left:10px;'>".form_radio('orientation', 'portrait', $settings['orientation'] === 'portrait')." ".lang('Portrait')."</span><span style='padding-left:10px;'>".form_radio('orientation', 'landscape', $settings['orientation'] === 'landscape')." ".lang('Landscape')."</span>",//form_input('orientation', ''),
            'filename'      => form_input('filename', $settings['filename']),
            'cache_enabled' => "<span style='padding-left:10px;'>".form_radio('cache_enabled', '1', $settings['cache_enabled'] == 1)." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('cache_enabled', '0', $settings['cache_enabled'] == 0)." ".lang('No')."</span>",
            'cache_ttl'     => form_input('cache_ttl', $settings['cache_ttl']),
            ''              => lang('encrypt_description'),
            'encrypt'       => "<span style='padding-left:10px;'>".form_radio('encrypt', '1', $settings['encrypt'] == '1')." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('encrypt', '0', $settings['encrypt'] == '0')." ".lang('No')."</span>",
            'userpass'      => form_input('userpass', $settings['userpass']),
            'ownerpass'     => form_input('ownerpass', $settings['ownerpass']),
            'can_print'     => "<span style='padding-left:10px;'>".form_radio('can_print', '1', $settings['can_print'] == '1')." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('can_print', '0', $settings['can_print'] == '0')." ".lang('No')."</span>",
            'can_modify'    => "<span style='padding-left:10px;'>".form_radio('can_modify', '1', $settings['can_modify'] == '1')." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('can_modify', '0', $settings['can_modify'] == '0')." ".lang('No')."</span>", //form_checkbox('can_modify', 'yes'),
            'can_copy'      => "<span style='padding-left:10px;'>".form_radio('can_copy', '1', $settings['can_copy'] == '1')." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('can_copy', '0', $settings['can_copy'] == '0')." ".lang('No')."</span>",
            'can_add'       => "<span style='padding-left:10px;'>".form_radio('can_add', '1', $settings['can_add'] == '1')." ".lang('Yes')."</span><span style='padding-left:10px;'>".form_radio('can_add', '0', $settings['can_add'] == '0')." ".lang('No')."</span>",
        ];

        return $data;
    }

    public function saveSetting()
    {
        //validation?
        ee()->load->helper(array('form', 'url'));
        ee()->load->library('form_validation');
        ee()->form_validation->set_rules('key', lang('key'), 'required');

        $config_data = array(
            'key'           => ee()->input->post('key'),
            'attachment'    => ee()->input->post('attachment'),
            'orientation'   => ee()->input->post('orientation'),
            'size'          => ee()->input->post('size'),
            'filename'      => ee()->input->post('filename'),
            'cache_enabled' => ee()->input->post('cache_enabled'),
            'cache_ttl'     => ee()->input->post('cache_ttl'),
            'encrypt'       => ee()->input->post('encrypt'),
            'userpass'      => ee()->input->post('userpass'),
            'ownerpass'     => ee()->input->post('ownerpass'),
            'can_print'     => ee()->input->post('can_print'),
            'can_modify'    => ee()->input->post('can_modify'),
            'can_copy'      => ee()->input->post('can_copy'),
            'can_add'       => ee()->input->post('can_add'),
        );

        $id = ee()->input->post('id');

        $data = array(
            'site_id' => $this->site_id,
            'key'   =>  ee()->input->post('key'),
            'data'  => json_encode($config_data)
        );

        if (ee()->form_validation->run() == false) {
            ee()->load->library('table');
            ee()->load->library('javascript');

            $vars['form_hidden'] = null;
            $vars['action_url'] = ee('CP/URL', 'addons/settings/pdf_press/save_setting'); //BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=pdf_press'.AMP.'method=save_setting';

            $row = (object) array('id' => $id, 'key' => ee()->input->post('key'), 'data' => json_encode($config_data));
            $vars['data'] = $this->_edit_setting_form($row);
            $vars['preset_page_title'] = lang('Edit Preset: '.$row->key);

            ee()->session->set_flashdata('message_failure', lang('setting_form_error'));

            return ee()->load->view('setting-form', $vars, true);
        } else {
            if ($id) {
                $setting = ee('Model')->get('pdf_press:PdfPressSetting')
                        ->filter('id', $id)
                        ->first();
                $setting->site_id = $this->site_id;
                $setting->key =  ee()->input->post('key');
                $setting->data = json_encode($config_data);
                $setting->save();
            } else {
                $setting = ee('Model')->make(
                    'pdf_press:PdfPressSetting',
                    $data
                );
                $setting->save();
            }

            ee()->functions->redirect(ee('CP/URL', 'addons/settings/pdf_press'));
        }
    }
}
