<?php

namespace Anecka\PDFPress\Service;

class FontService
{
    public $site_id = 1;

    public function __construct()
    {
        $this->site_id = ee()->config->item('site_id');
    }

    public function index()
    {
        $fonts = ee('Model')->get('pdf_press:PdfPressFont')
                    ->filter('site_id', ee()->config->item('site_id'))
                    ->all();
        return $fonts;
    }

    public function newFontForm($id = '', $presets = [])
    {
        $action_url = ee('CP/URL', 'addons/settings/pdf_press/save_font');
        $sizes = [];

        if ($id) {
            $font = ee('Model')->get('pdf_press:PdfPressFont', $id)->first();
            $fontData = json_decode($font->data, true);
            $presets['font_name'] = $font->name;
            $presets['font_regular'] = $fontData['font_regular'];
            $presets['font_bold'] = $fontData['font_bold'];
            $presets['font_italic'] = $fontData['font_italic'];
            $presets['font_default'] = $font->default;
        }

        $data = [
            'font_name' => ($id ? form_hidden('font_id', $id) : '') . form_input('font_name', $presets['font_name'] ?? ''),
            'font_regular' => form_input('font_regular', $presets['font_regular'] ?? ''),
            'font_bold' => form_input('font_bold', $presets['font_bold'] ?? ''),
            'font_italic' => form_input('font_italic', $presets['font_italic'] ?? ''),
            'font_default' => form_yes_no_toggle('font_default', $presets['font_default'])
        ];

        return $data;
    }

    public function saveFont()
    {
        //validation?
        ee()->load->helper(array('form', 'url'));
        ee()->load->library('form_validation');

        $config_data = [
            'font_name'    => ee()->input->post('font_name'),
            'font_regular'   => ee()->input->post('font_regular'),
            'font_bold'   => ee()->input->post('font_bold'),
            'font_italic'   => ee()->input->post('font_italic'),
        ];

        $id = ee()->input->post('font_id');

        $data = [
            'site_id' => $this->site_id,
            'name'   =>  strtolower(str_replace(' ', '-', ee()->input->post('font_name'))),
            'data'  => json_encode($config_data),
            'default' => ee()->input->get_post('font_default'),
        ];

        if ($id) {
            $setting = ee('Model')->get('pdf_press:PdfPressFont')
                    ->filter('font_id', $id)
                    ->first();
            $setting->site_id = $this->site_id;
            $setting->name = ee()->input->post('font_name');
            $setting->data = json_encode($config_data);
            $setting->default = ee()->input->get_post('font_default');
            $setting->save();
        } else {
            $setting = ee('Model')->make(
                'pdf_press:PdfPressFont',
                $data
            );
            $setting->save();
        }

        ee()->functions->redirect(ee('CP/URL', 'addons/settings/pdf_press/fonts'));
    }
}
