<?php

namespace Anecka\PDFPress\Service;

require_once __DIR__ . '/../vendor/autoload.php';

use ExpressionEngine\Library\Filesystem\Filesystem;
use Mpdf\Mpdf;
use Mpdf\Config\ConfigVariables as MpdfConfigVariables;
use Mpdf\Config\FontVariables as MpdfFontVariables;
use Mpdf\HTMLParserMode as MpdfHTMLParserMode;
use Mpdf\Output\Destination;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;

class PdfService
{
    public $cssConverter;
    public $html;
    public $mpdf;
    public $settings;

    public function __construct($html, $settings)
    {
        $this->html = $html;
        $this->cssConverter = new CssToInlineStyles;
        $this->settings = ee('pdf_press:SettingsService')->mapSettings($settings);
        if (isset($this->settings['size'])) {
            $this->settings['format'] = $this->settings['size'];
        }
        $this->loadFonts();
        $this->mpdf = new Mpdf($this->settings);
        $this->mpdf->setBasePath(ee()->config->item('site_url'));
        $this->mpdf->curlAllowUnsafeSslRequests = true;
        // ->showImageErrors = true;
        $html = $this->loadCss($html);
        // $this->mpdf->WriteHTML($html, MpdfHTMLParserMode::HTML_BODY, true);
        $this->mpdf->WriteHTML($html, MpdfHTMLParserMode::DEFAULT_MODE, true);
    }

    public function output($fileName = 'filename.pdf', $type = 'F')
    {
        // $path = rtrim(PATH_CACHE, '/') . '/pdf_press/' . $fileName;
        return $this->mpdf->Output($fileName, $type);
    }

    public function stream($fileName, $options = [])
    {
        if (headers_sent()) {
            die("Unable to stream pdf: headers already sent");
        }

        $debug = empty($options['compression']);

        if (!isset($options["attachment"])) {
            $options["attachment"] = true;
        }

        if ($options["attachment"]) {
            return $this->output($fileName, 'I');
        } else {
            return $this->output($fileName, 'I');
        }

        exit;
    }

    public function streamFromFilesystem($pdfPath, $filename, $asAttachment = false)
    {
        if (strpos($filename, ".pdf") === false) {
            $filename .= ".pdf";
        }

        if ($attachment) {
            header('Content-Type: application/pdf');
            header("Content-Transfer-Encoding: Binary");
            header("Content-disposition: attachment; filename=".$filename);
            header('Accept-Ranges: bytes');
        } else {
            //open in browser
            header('Content-type: application/pdf');
            header("Content-Disposition: inline; filename=".$filename);
            header('Content-Transfer-Encoding: Binary');
            header('Accept-Ranges: bytes');
        }

        ob_clean();
        flush();

        readfile($pdfPath);
    }

    private function loadFonts()
    {
        $defaultConfig = (new MpdfConfigVariables)->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new MpdfFontVariables)->getDefaults();
        $fontDefaults = $defaultFontConfig['fontdata'];

        $themePath = ee()->config->slash_item('theme_folder_path');
        $themePath = rtrim($themePath, '/') . '/user/pdf_press/assets/fonts';

        $fontData = [];

        $fontModels = ee('Model')->get('pdf_press:PdfPressFont')
                            ->filter('site_id', ee()->config->item('site_id'))
                            ->all();
        $defaultFont = null;

        foreach ($fontModels as $fm) {
            $data = json_decode($fm->data, true);
            $item = [];
            if ($data['font_bold']) {
                $item['B'] = $data['font_bold'];
            }

            if ($data['font_regular']) {
                $item['R'] = $data['font_regular'];
            }

            if ($data['font_italic']) {
                $item['I'] = $data['font_italic'];
            }

            if ((isset($data['font_default']) && $data['font_default'])) {
                $defaultFont = $fm->name;
            } else if ((isset(ee()->TMPL) && ee()->TMPL->fetch_param('default_font') === $fm->name) || ee()->input->get_post('default_font') === $fm->name) {
                $defaultFont = $fm->name;
            }

            $fontData[$fm->name] = $item;
        }

        if (empty($fontData)) {
            return;
        }

        $this->settings['fontDir'] = array_merge(
            $fontDirs,
            [$themePath,]
        );

        $this->settings['fontdata'] = array_merge($fontDefaults, $fontData);

        if ($defaultFont) {
            $this->settings['default_font'] = $defaultFont;
        }
    }

    private function loadCss($html = '')
    {
        $themePath = ee()->config->slash_item('theme_folder_path');
        $themePath = rtrim($themePath, '/') . '/user/pdf_press/assets/css';
        $service = new Filesystem;
        $contents = $service->getDirectoryContents($themePath);

        $allCss = '';

        foreach ($contents as $file) {
            if ($service->isFile($file) && $service->extension($file) === 'css') {
                $stylesheet = $service->read($file);
                $allCss .= $stylesheet;
            }
        }

        // Now parse document
        try {
            $dom = new \DOMDocument;

            @$dom->loadHTML($html);

            $linkTags = $dom->getElementsByTagName('link');

            foreach ($linkTags as $linkTag) {
                if ($linkTag->getAttribute('rel') && $linkTag->getAttribute('rel') === 'stylesheet') {
                    $href = $linkTag->getAttribute('href');

                    $contents = ee('pdf_press:GuzzleService', $href)->html();
                    $allCss .= (string) $contents;
                }
            }
        } catch (\Exception $e) {
            // TODO: What do we do?
        }
        $finalizedCss = $this->convertCssVariables($allCss);

        $output = $this->cssConverter->convert($html, $finalizedCss);
        return $output;
    }

    private function convertCssVariables($css)
    {
        // Remove CSS comments
        $css = preg_replace('!/\*.*?\*/!s', '', $css);
        $css = preg_replace('/\n\s*\n/', "\n", $css);
        // match and extract CSS variables and their values, capture the spaces after the colon
        $variables = [];
        preg_match_all('/--(.+?):(\s)?(.+?);/', $css, $matches);

        foreach ($matches[0] as $match) {
            $items = explode(':', $match, 2);
            $items[1] = rtrim($items[1], ';');
            $variables[$match] = $items;
        }

        // First, lets get rid of nested vars
        foreach ($variables as $fullVar => $splitVar) {
            if (strpos($fullVar, 'var(') !== false) {
                // Get variable inside the variable
                preg_match_all('/var\((.*)\)/', $fullVar, $innerMatches);

                foreach ($innerMatches[1] as $im) {
                    $key = array_search($im, array_column($variables, 0));
                    if ($key) {
                        $keys = array_keys($variables);
                        $item = $variables[$keys[$key]];
                        [$tempMatchKey, $tempMatchVar] = explode(':', $fullVar, 2);
                        $tempMatchVar = str_replace($tempMatchVar, $item[1], $tempMatchVar);
                        $variables[$tempMatchKey] = [
                            $tempMatchKey, $tempMatchVar
                        ];
                    }
                }
            }
        }

        // Replace all of them and remove
        foreach ($variables as $fullVar => $splitVar) {
            $css = str_replace('var(' . $splitVar[0] . ')', rtrim($splitVar[1], ';'), $css);
            $css = str_replace($fullVar, '', $css);
        }
        
        return $css;
    }
}
