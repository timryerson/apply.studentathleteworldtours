<?php

namespace Anecka\PDFPress\Service;

require_once __DIR__ . '/../vendor/autoload.php';

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;

class GuzzleService
{
    public $client;
    public $response;
    public $url;
    public $error;

    private $guzzleOptions = [
        'verify' => false,
    ];

    private $hasError = false;

    public function __construct($url, $options = [])
    {
        $this->url = $url;
        $this->client = new GuzzleClient();
        try {
            $this->response = $this->client->request('GET', $this->url, $this->guzzleOptions);
        } catch (\RequestException|\Exception|\ErrorException $e) {
            $this->error = $e->getMessage();
            $this->hasError = true;
            $this->_guzzleFallback();
        }
    }

    public function html()
    {
        return $this->hasError
                ? $this->response
                : $this->response->getBody()->getContents();
    }

    /**
     * Gets called if Guzzle fails. It's a hail Mary
     * attempt.
     * @return string
     */
    private function _guzzleFallback()
    {
        $this->response = @file_get_contents($this->url);
    }
}
