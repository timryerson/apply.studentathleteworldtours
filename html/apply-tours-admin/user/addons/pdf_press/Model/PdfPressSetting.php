<?php

namespace Anecka\PDFPress\Model;

use ExpressionEngine\Service\Model\Model;

class PdfPressSetting extends Model
{
    protected static $_primary_key = 'id';
    protected static $_table_name = 'pdf_press_configs';

    protected $id;
    protected $site_id;
    protected $key;
    protected $data;
}
