<?php

namespace Anecka\PDFPress\Model;

use ExpressionEngine\Service\Model\Model;

class PdfPressFont extends Model
{
    protected static $_primary_key = 'font_id';
    protected static $_table_name = 'pdf_press_fonts';

    protected $font_id;
    protected $site_id;
    protected $name;
    protected $data;
    protected $default;

    protected static $_typed_columns = [
        'default' => 'boolString',
    ];
}
