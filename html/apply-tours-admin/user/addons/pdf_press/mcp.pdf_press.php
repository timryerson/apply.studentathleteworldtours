<?php

if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

use ExpressionEngine\Library\CP\Table;

class Pdf_press_mcp
{
    public $site_id = 1;
    public $base_url;
    public $perpage = 25;
    public $sidebar;
    public $homeNav;
    public $settingsNav;
    public $fonts_nav;
    public $previewNav;

    public function __construct()
    {
        // Make a local reference to the ExpressionEngine super object
        $this->site_id = ee()->config->item('site_id');
        $this->base_url = ee('CP/URL', 'addons/settings/pdf_press');
        ee()->view->cp_page_title = ee()->lang->line('pdf_press_module_name');

        $this->sidebar = ee('CP/Sidebar')->make();
        $this->sidebar->addHeader(
            lang('settings'),
            ee('CP/URL', 'addons/settings/pdf_press')
        )->withButton(
            lang('new'),
            ee('CP/URL', 'addons/settings/pdf_press/new')
        );
        $this->sidebar->addHeader(
            lang('fonts'),
            ee('CP/URL', 'addons/settings/pdf_press/fonts')
        )->withButton(
            lang('new'),
            ee('CP/URL', 'addons/settings/pdf_press/new_font')
        );
        $this->sidebar->addHeader(lang('preview'), ee('CP/URL', 'addons/settings/pdf_press/preview'));
    }

    public function index()
    {
        ee()->cp->add_js_script('file', 'cp/sort_helper');
        ee()->cp->add_js_script('plugin', 'ee_table_reorder');
        ee()->cp->load_package_js('pdf_press');

        $query = ee('pdf_press:SettingsService')->getSettings();

        $settings = [];

        foreach ($query as $row) {
            $setting_data = json_decode($row->data, true);

            $settingLabel = ($setting_data['attachment'] == 1 ? lang('Download') : lang('Browser')).", "
                . $setting_data['orientation'].", ".$setting_data['size'].", "
                . lang('encrypt')."? ".($setting_data['encrypt'] == 1 ? lang('Yes') : lang('No'))." "
                . $setting_data['filename'];

            $settings[] = [
                'id'            => $row->id,
                'key'           => $row->key,
                'data'          => $settingLabel,
                [
                    'toolbar_items' => [
                        'edit' => [
                            'href' => ee('CP/URL', 'addons/settings/pdf_press/edit_setting/' . $row->id),
                            'title' => lang('edit')
                        ],
                    ],
                ],
            ];
        }

        $table = ee('CP/Table');
        $table->setColumns(
            [
                'id',
                'key',
                'data',
                'manage' => [
                    'type'  => Table::COL_TOOLBAR
                ],
            ]
        );

        $table->setData($settings);
        $table->setNoResultsText('no_settings', 'create_settings', ee('CP/URL', 'addons/settings/pdf_press/new'));

        $vars['table'] = $table->viewData(ee('CP/URL', 'addons/settings/pdf_press/settings'));

        return array(
            'body' => ee()->load->view('table', $vars, true),
            'breadcrumb' => array(
                ee('CP/URL', 'addons/settings/pdf_press/settings')->compile() => lang('pdf_press_module_name')
            ),
            'heading' => lang('settings'),
            'sidebar' => $this->sidebar
        );
    }

    public function edit_setting($id)
    {
        $setting = ee('pdf_press:SettingsService')->getSetting($id);
        ee()->load->helper('form');
        ee()->load->library('table');
        ee()->load->library('javascript');
        $settingData = json_decode($setting->data, true);
        $vars = [];
        $vars['form_hidden'] = null;
        $vars['action_url'] = ee('CP/URL', 'addons/settings/pdf_press/save_setting');
        $vars['data'] = ee('pdf_press:SettingsService')->editSettingForm($id, $settingData);
        $vars['preset_page_title'] = lang('New Preset:');

        return [
            'body' => ee()->load->view('setting-form', $vars, true),
            'breadcrumb' => [
                ee('CP/URL', 'addons/settings/pdf_press/settings')->compile() => lang('pdf_press_module_name')
            ],
            'heading' => lang('pdf_press_module_name'),
            'sidebar' => $this->sidebar,
        ];
    }

    public function new()
    {
        ee()->load->helper('form');
        ee()->load->library('table');
        ee()->load->library('javascript');

        $vars = [];
        $vars['form_hidden'] = null;
        $vars['action_url'] = ee('CP/URL', 'addons/settings/pdf_press/save_setting');
        $vars['data'] = ee('pdf_press:SettingsService')->newSettingForm();
        $vars['preset_page_title'] = lang('New Preset:');

        return [
            'body' => ee()->load->view('setting-form', $vars, true),
            'breadcrumb' => [
                ee('CP/URL', 'addons/settings/pdf_press/settings')->compile() => lang('pdf_press_module_name')
            ],
            'heading' => lang('pdf_press_module_name'),
            'sidebar' => $this->sidebar,
        ];
    }

    public function save_setting()
    {
        return ee('pdf_press:SettingsService')->saveSetting();
    }

    public function fonts()
    {
        ee()->cp->add_js_script('file', 'cp/sort_helper');
        ee()->cp->add_js_script('plugin', 'ee_table_reorder');
        ee()->cp->load_package_js('pdf_press');

        $query = ee('pdf_press:FontService')->index();

        $settings = [];

        foreach ($query as $row) {
            $settings[] = [
                'id'            => $row->font_id,
                'name'           => $row->name . ($row->default ? ' (default)' : ''),
                [
                    'toolbar_items' => [
                        'edit' => [
                            'href' => ee('CP/URL', 'addons/settings/pdf_press/edit-font/' . $row->font_id),
                            'title' => lang('edit')
                        ],
                    ],
                ],
            ];
        }

        $table = ee('CP/Table');
        $table->setColumns(
            [
                'id',
                'name',
                'manage' => [
                    'type'  => Table::COL_TOOLBAR
                ],
            ]
        );

        $table->setData($settings);
        $table->setNoResultsText('no_fonts', 'create_fonts', ee('CP/URL', 'addons/settings/pdf_press/new_font'));

        $vars['table'] = $table->viewData(ee('CP/URL', 'addons/settings/pdf_press/fonts'));

        return array(
            'body' => ee()->load->view('table', $vars, true),
            'breadcrumb' => array(
                ee('CP/URL', 'addons/settings/pdf_press/fonts')->compile() => lang('pdf_press_module_name')
            ),
            'heading' => lang('fonts'),
            'sidebar' => $this->sidebar
        );
    }

    public function new_font()
    {
        ee()->load->helper('form');
        ee()->load->library('table');
        ee()->load->library('javascript');

        $vars = [];
        $vars['form_hidden'] = null;
        $vars['action_url'] = ee('CP/URL', 'addons/settings/pdf_press/save_font');
        $themePath = ee()->config->slash_item('theme_folder_path');
        $themePath = rtrim($themePath, '/') . '/user/pdf_press/assets/fonts';
        $vars['theme_path'] = $themePath;
        $vars['data'] = ee('pdf_press:FontService')->newFontForm();
        $vars['preset_page_title'] = lang('New Preset:');

        return [
            'body' => ee()->load->view('setting-form', $vars, true),
            'breadcrumb' => [
                ee('CP/URL', 'addons/settings/pdf_press/settings')->compile() => lang('pdf_press_module_name')
            ],
            'heading' => lang('pdf_press_module_name'),
            'sidebar' => $this->sidebar,
        ];
    }

    public function editFont($id)
    {
        ee()->load->helper('form');
        ee()->load->library('table');
        ee()->load->library('javascript');

        $vars = [];
        $vars['form_hidden'] = null;

        $vars['action_url'] = ee('CP/URL', 'addons/settings/pdf_press/save_font');
        $themePath = ee()->config->slash_item('theme_folder_path');
        $themePath = rtrim($themePath, '/') . '/user/pdf_press/assets/fonts';
        $vars['theme_path'] = $themePath;
        $vars['data'] = ee('pdf_press:FontService')->newFontForm($id);
        $vars['preset_page_title'] = lang('New Preset:');

        return [
            'body' => ee()->load->view('setting-form', $vars, true),
            'breadcrumb' => [
                ee('CP/URL', 'addons/settings/pdf_press/settings')->compile() => lang('pdf_press_module_name')
            ],
            'heading' => lang('pdf_press_module_name'),
            'sidebar' => $this->sidebar,
        ];
    }

    public function save_font()
    {
        return ee('pdf_press:FontService')->saveFont();
    }

    public function preview()
    {
        $_SESSION["authenticated"] = true;
        ee()->load->helper('form');
        ee()->load->library('table');
        ee()->load->library('javascript');

        $action_id = ee()->cp->fetch_action_id('Pdf_press', 'create_pdf');

        $add_query = (ee()->config->item('force_query_string') == 'y') ? '': '?';

        $path = ee()->input->post('path', '');

        $attachment = 0;
        $size = ee()->input->post('size', 'letter');
        $orientation = ee()->input->post('orientation', 'portrait');

        $url = ee()->functions->create_url("").$add_query."ACT=$action_id&attachment=$attachment&path=$path&size=$size&orientation=$orientation&preview=1";

        $vars = [];
        $vars['url_path'] = $path;
        $vars['size'] = $size;
        $vars['orientation'] = $orientation;

        $vars['dom_path'] = $url;
        $vars['paper_sizes'] = ee('pdf_press:EnumService')->paperSizes();

        //var_dump($vars);
        return array(
            'body' => ee()->load->view('preview', $vars, true),
            'breadcrumb' => array(
                ee('CP/URL', 'addons/settings/pdf_press')->compile() => lang('pdf_press_module_name')
            ),
            'heading' => lang('preview')
        );
    }
}
