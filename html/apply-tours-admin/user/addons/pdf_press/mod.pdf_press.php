<?php

if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pdf_press
{
    /**
     * ExpressionEngine site ID
     * @var integer
     */
    public $site_id = 1;

    /**
     * TTL seconds
     * @var integer
     */
    public $cache_ttl = 3600;
    public $cache_path = "";

    public function __construct()
    {
        $this->site_id = ee()->config->item('site_id');
        ee()->lang->loadfile('pdf_press');
        $this->checkCacheDir();
    }

    public function save_to_pdf_form()
    {
        /* this method is deprecated, please use save_to_pdf or parse_pdf instead */
        ee()->output->show_user_error('general', $errors);
    }

    public function save_to_pdf()
    {
        $path = ee()->TMPL->fetch_param('path', ee()->uri->uri_string());
        $attachment = ee()->TMPL->fetch_param('attachment', '1');
        $compress = ee()->TMPL->fetch_param('compress', '1');
        $size = ee()->TMPL->fetch_param('size', 'Letter');
        $orientation = ee()->TMPL->fetch_param('orientation', 'portrait');
        $filename = ee()->TMPL->fetch_param('filename', '');
        $default_font = ee()->TMPL->fetch_param('default_font', '');
        $key = ee()->TMPL->fetch_param('key', '');

        $action_id = ee()->functions->fetch_action_id('Pdf_press', 'create_pdf');

        $add_query = ee()->config->item('force_query_string') == 'y' ? '' : '?';

        return ee()->functions->create_url("") . $add_query . "ACT=$action_id&path=" . urlencode($path) . "&size=" . urlencode($size) . "&orientation=$orientation&key=$key&attachment=$attachment&compress=$compress&filename=" . urlencode($filename) . '&default_font=' . urlencode($default_font);
    }

    public function parse_pdf()
    {
        $settings = [
            'attachment'    => ee()->TMPL->fetch_param('attachment', '1'),
            'compress'      => ee()->TMPL->fetch_param('compress', '1'),
            'orientation'   => ee()->TMPL->fetch_param('orientation', 'portrait'),
            'size'          => ee()->TMPL->fetch_param('size', 'Legal'),
            'filename'      => ee()->TMPL->fetch_param('filename', ''),
            'cache_enabled' => false,
            'cache_ttl'     => $this->cache_ttl,
            'encrypt'       => false,
            'userpass'      => '',
            'ownerpass'     => '',
            'can_print'     => true,
            'can_modify'    => true,
            'can_copy'      => true,
            'can_add'       => true,
        ];

        //get the key
        $key = ee()->TMPL->fetch_param('key', '');

        try {
            if ($key) {
                //lookup the key & pull settings, if key not found then throw user error
                $dataSettings = $this->_lookup_settings($key);
                //array merge the key with the user parameter overrides
                foreach ($dataSettings as $field => $value) {
                    if ($value && $value != "") {
                        $settings[$field] = $value;
                    }
                }
            }

            $html = $this->_render(ee()->TMPL->tagdata);
            $this->_generate_pdf($html, $settings, false);
            exit;
        } catch (Exception $e) {
            $checkMarkup = ee()->lang->line('error_check_markup');
            $pdfError =  ee()->lang->line('dompdf_error');

            $errors = [
                $checkMarkup,
                $pdfError.$e->getMessage(),
            ];
            ee()->output->show_user_error('general', $errors);
        }
    }

    public function create_pdf()
    {
        $settings = [
            'attachment'    => ee()->input->get_post('attachment'),
            'compress'      => ee()->input->get_post('compress'),
            'orientation'   => ee()->input->get_post('orientation'),
            'size'          => urldecode(ee()->input->get_post('size')),
            'filename'      => urldecode(ee()->input->get_post('filename')),
            'cache_enabled' => false,
            'cache_ttl'     => $this->cache_ttl,
            'encrypt'       => false,
            'userpass'      => '',
            'ownerpass'     => '',
            'can_print'     => true,
            'can_modify'    => true,
            'can_copy'      => true,
            'can_add'       => true,
        ];

        //get the key
        $key = ee()->input->get_post('key', '');
        $path = urldecode(ee()->input->get_post('path'));
        $filename = $settings['filename'];

        if ($filename == "") {
            $path_array = explode('/', $path);
            $filename = end($path_array) . '.pdf';
            $settings['filename'] = $filename;
        }

        // check if already full path
        $fullUrl = (strpos($path, 'http') === false)
                    ? ee()->functions->create_url($path)
                    : $path;

        $html = ee('pdf_press:GuzzleService', $fullUrl)->html();

        if ($key) {
            //lookup the key & pull settings, if key not found then throw user error
            $dataSettings = $this->_lookup_settings($key);
            //array merge the key with the user parameter overrides
            foreach ($dataSettings as $field => $value) {
                if ($value) {
                    $settings[$field] = $value;
                }
            }
        }

        $shouldSendOutput = ee()->input->get('preview', false);

        try {
            $this->_generate_pdf($html, $settings, $shouldSendOutput, $fullUrl);
            exit;
        } catch (Exception $e) {
            $check_markup = ee()->lang->line('error_check_markup');
            $dompdf_error =  ee()->lang->line('dompdf_error');

            $errors = array($check_markup,
                    $dompdf_error.$e->getMessage());
            ee()->output->show_user_error('general', $errors);
        }
    }

    public function create_pdf_ext($path, $pdfSettings = [], $key = "")
    {
        $fullUrl = ee()->functions->create_url($path);
        $html = ee('pdf_press:GuzzleService', $fullUrl)->html();

        if ($key != "") {
            $dataSettings = $this->_lookup_settings($key);
            //array merge the key with the user parameter overrides
            foreach ($dataSettings as $field => $value) {
                if ($value) {
                    $pdfSettings[$field] = $value;
                }
            }
        }

        return $this->_generate_pdf($html, $pdfSettings, true);
    }

    public function physical_path()
    {
        $to_path = ee()->TMPL->fetch_param('to', '');
        return realpath($to_path);
    }

    /**
     * PRIVATE FUNCTIONS
     */

    private function checkCacheDir()
    {
        $path = SYSPATH . 'user/cache/' . ee()->config->item('site_short_name') . '/' . PDF_PRESS_PACKAGE;
        if (!is_dir($path)) {
            mkdir($path, 0755, true);
        }
        $this->cache_path = $path;
    }

    private function _lookup_settings($key)
    {
        $settings = ee('Model')->get('pdf_press:PdfPressSetting')
                        ->filter('key', $key)
                        ->first();

        if (!$settings) {
            $errors = array(lang('no_setting_found'), "missing setting preset: '$key'");
            ee()->output->show_user_error('general', $errors);
            return;
        }
        
        $settingData = json_decode($settings->data, true);

        $finalSettings = [
            'attachment'    => $settingData['attachment'],
            'orientation'   => $settingData['orientation'],
            'size'          => $settingData['size'],
            'filename'      => $settingData['filename'],
            'cache_enabled' => $settingData['cache_enabled'],
            'cache_ttl'     => $settingData['cache_ttl'],
            'encrypt'       => $settingData['encrypt'],
            'userpass'      => $settingData['userpass'],
            'ownerpass'     => $settingData['ownerpass'],
            'can_print'     => $settingData['can_print'],
            'can_modify'    => $settingData['can_modify'],
            'can_copy'      => $settingData['can_copy'],
            'can_add'       => $settingData['can_add'],
        ];

        return $finalSettings;
    }

    private function _render($text, $opts = [])
    {
        /* Run through the initial parsing phase, set output type */
        ee()->TMPL->parse($text, false);
        ee()->TMPL->final_template = ee()->TMPL->parse_globals(ee()->TMPL->final_template);
        ee()->output->out_type = ee()->TMPL->template_type;

        /* Return source. If we were given opts to do template replacement, parse them in */
        if (count($opts) > 0) {
            ee()->output->set_output(
                ee()->TMPL->parse_variables(
                    ee()->TMPL->final_template,
                    [$opts]
                )
            );
        } else {
            ee()->output->set_output(ee()->TMPL->final_template);
        }
        return ee()->output->final_output;
    }

    private function getCacheKeyFromUri($url = '')
    {
        $key = '/'.PDF_PRESS_PACKAGE;

        if ($url == '') {
            $key .= '/'.ee()->uri->uri_string();
        } else {
            $pos = strpos($url, "index.php");
            if ($pos) {
                $key .= substr($url, $pos + strlen("index.php"), strlen($url)-1);
            }
        }

        return $key;
    }

    private function getCacheFilePath($filename)
    {
        return $this->cache_path.'/'.$filename.'.pdf';
    }

    private function _generate_pdf($html, $settings, $returnOutput = false, $path = '')
    {
        $cacheKey = $this->getCacheKeyFromUri($path);
        $md5Key = md5($cacheKey);
        $pdfPath = ee()->cache->get($md5Key);
        $filename = $settings['filename'];
        $service = ee('pdf_press:PdfService', $html, $settings);

        if ($settings['cache_enabled']) {
            if ($pdfPath == '' && !file_exists($pdfPath)) {
                $pdf = $service->output();

                if ($returnOutput) {
                    return $pdf;
                }

                $pdfPath = $this->getCacheFilePath($md5Key);
                if ($settings['cache_ttl'] != '' && $settings['cache_ttl'] > 0) {
                    ee()->cache->save($md5Key, $pdfPath, intval($settings['cache_ttl']));
                } else {
                    ee()->cache->save($md5Key, $pdfPath);
                }

                file_put_contents($pdfPath, $pdf);
            }

            $service->streamFromFilesystem($pdfPath, $filename, $settings['attachment']);
        } else {
            if ($returnOutput) {
                return $service->output($filename, 'I');
            }

            $options = [];

            if ($settings['attachment'] != '') {
                $options['attachment'] = $settings['attachment'];
            }

            if ($settings['compress'] != '') {
                $options['compress'] = $settings['compress'];
            }
            
            if (count($options) > 0) {
                $service->stream($settings['filename'], $options);
            } else {
                $service->stream($settings['filename']);
            }
        }
    }
}
