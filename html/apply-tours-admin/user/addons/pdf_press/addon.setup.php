<?php

use Anecka\PDFPress\Service\EnumService;
use Anecka\PDFPress\Service\FontService;
use Anecka\PDFPress\Service\GuzzleService;
use Anecka\PDFPress\Service\PdfService;
use Anecka\PDFPress\Service\SettingsService;

defined('PDF_PRESS_NAME') || define('PDF_PRESS_NAME', 'Pdf_press');
defined('PDF_PRESS_PACKAGE') || define('PDF_PRESS_PACKAGE', 'pdf_press');
defined('PDF_PRESS_FULL_NAME') || define('PDF_PRESS_FULL_NAME', 'PDF Press');
defined('PDF_PRESS_VERSION') || define('PDF_PRESS_VERSION', '5.2.1');
defined('PDF_PRESS_DOCS') || define('PDF_PRESS_DOCS', '');
defined('PDF_PRESS_DESCRIPTION') || define('PDF_PRESS_DESCRIPTION', 'Generate PDFs from EE Templates');

return [
    'author'            => 'tripleNERDscore',
    'author_url'        => 'https://triplenerdscore.net',
    'name'              => PDF_PRESS_NAME,
    'description'       => PDF_PRESS_DESCRIPTION,
    'version'           => PDF_PRESS_VERSION,
    'namespace'         => 'Anecka\PDFPress',
    'settings_exist'    => true,
    // Advanced settings
    'models' => [
        'PdfPressSetting' => 'Model\PdfPressSetting',
        'PdfPressFont' => 'Model\PdfPressFont',
    ],
    'services' => [
        'PdfService' => function ($addon, $html, $settings) {
            return new PdfService($html, $settings);
        },
        'GuzzleService' => function ($addon, $url) {
            return new GuzzleService($url);
        },
        'EnumService' => function ($addon) {
            return new EnumService;
        },
        'FontService' => function ($addon) {
            return new FontService;
        },
        'SettingsService' => function ($addon) {
            return new SettingsService;
        }
    ],
];
