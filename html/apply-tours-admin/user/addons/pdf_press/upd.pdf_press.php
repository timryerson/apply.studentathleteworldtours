<?php

if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

use ExpressionEngine\Service\Addon\Installer;

class Pdf_press_upd extends Installer
{
    public $has_cp_backend = 'y';
    public $has_publish_fields = 'n';
    public $version = PDF_PRESS_VERSION;

    public $actions = [
        [
            'class' => PDF_PRESS_NAME,
            'method' => 'create_pdf'
        ],
    ];

    public $methods = [
        [
            'hook'      => 'template_types',
            'method'    => 'add_pdf_template_type',
            'priority'  => 1
        ],
        [
            'method'    => 'create_pdf',
            'hook'      => 'pdf_press_generate_pdf',
            'priority'  => 1,
        ]
    ];

    public function install()
    {
        parent::install();
        $this->installConfigs();
        $this->installFonts();
        return true;
    }

    public function update($current = '')
    {
        // Runs migrations
        parent::update($current);

        if (version_compare($current, '1.5', '<')) {
            $this->installConfigs();
        }

        if (version_compare($current, '3.5.0', '<')) {
            $field = [
                'site_id' => [
                    'type' => 'int',
                    'constraint' => '10',
                    'unsigned' => true,
                ],
            ];

            ee()->dbforge->add_column('pdf_press_configs', $field);
        }

        if (version_compare($current, '5.0.0', '<')) {
            $this->runV5Updates();
        }

        if (version_compare($current, '5.2.0', '<')) {
            $this->runV520Updates();
        }

        return true;
    }

    public function uninstall()
    {
        parent::uninstall();

        return true;
    }

    private function installConfigs()
    {
        ee()->load->dbforge();
        $fields = [
            'id' => [
                'type' => 'int',
                'constraint' => '10',
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'site_id' => [
                'type' => 'int',
                'constraint' => '10',
                'unsigned' => true,
            ],
            'key' => [
                'type' => 'varchar',
                'constraint' => '500',
            ],
            'data' => [
                'type' => 'text',
                'null' => true,
            ],
        ];

        if (!ee()->db->table_exists('pdf_press_configs')) {
            ee()->dbforge->add_field($fields);
            ee()->dbforge->add_key('id', true);
            ee()->dbforge->create_table('pdf_press_configs');
        }
    }

    private function installFonts()
    {
        ee()->load->dbforge();
        $fields = [
            'font_id' => [
                'type' => 'int',
                'constraint' => '10',
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'site_id' => [
                'type' => 'int',
                'constraint' => '10',
                'unsigned' => true,
            ],
            'name' => [
                'type' => 'varchar',
                'constraint' => '500',
            ],
            'data' => [
                'type' => 'text',
                'null' => true,
            ],
            'default' => [
                'type' => 'varchar',
                'constraint' => '1',
                'default' => 'n',
            ],
        ];

        if (!ee()->db->table_exists('pdf_press_fonts')) {
            ee()->dbforge->add_field($fields);
            ee()->dbforge->add_key('font_id', true);
            ee()->dbforge->create_table('pdf_press_fonts');
        }
    }

    private function runV5Updates()
    {
        $this->installFonts();
    }

    private function runV520Updates()
    {
        if (ee()->db->field_exists('default', 'pdf_press_fonts')) {
            return;
        }

        ee()->load->dbforge();

        ee()->dbforge->add_column(
            'pdf_press_fonts',
            [
                'default' => [
                    'type' => 'varchar',
                    'constraint' => '1',
                    'default' => 'n',
                ],
            ]
        );
    }
}
