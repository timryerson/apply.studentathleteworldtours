<?php

class Pdf_press_ext
{
    public $name = PDF_PRESS_FULL_NAME;
    public $version = PDF_PRESS_VERSION;
    public $description = PDF_PRESS_DESCRIPTION;
    public $settings_exist  = 'n';
    public $docs_url        = '';
    public $settings        = [];

    public function add_pdf_template_type()
    {
        $custom_templates = ee()->extensions->last_call;
        
        if (!$custom_templates) {
            $custom_templates = [];
        }

        $custom_templates['pdf'] = array(             // Short name for database
            'template_name'           => 'PDF',  // Display name for Template Type dropdown
            'template_file_extension' => '.pdf',       // File extension for saving templates as files
            'template_headers'        => array(        // Custom headers for file type
                'Content-Type: application/pdf'
            )
        );

        return $custom_templates;
    }

    public function create_pdf($path, $settings)
    {
        require_once 'mod.pdf_press.php';

        $pdfpress = new Pdf_press();
        return $pdfpress->create_pdf_ext($path, $settings);
    }
}
