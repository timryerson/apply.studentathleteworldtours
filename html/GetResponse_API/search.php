<?php
include_once('GetResponseAPI3.class.php');
//getContacts($params = array()); //line 216
//updateContact($contact_id, $params = array()) //line228

$_apiKey = 'eb924e3721d3b6456b11db7ce1e84595';

$getresponse = new GetResponse($_apiKey);
$getresponse->enterprise_domain = 'mail.studentathleteworld.com';
$getresponse->api_url = 'https://api3.getresponse360.com/v3'; 

$email = $_REQUEST['email'];
$campaignID = $_REQUEST['campaignID'];
$name = $_REQUEST['name'];
$ipaddress = $_SERVER["REMOTE_ADDR"];

$query = array(
    'query' => array(
        'email' => $email,
        'campaignId' => $campaignID
    ),
    'fields' => 'name,email'
  );

$results = json_encode($getresponse->getContacts($query));

if ( strrpos($results, $email) > -1) {
//  if name on list, do nothing
    echo 'Name is already on list';
 } else {
// if name not on list (no results), then post a new one.
    echo 'Name is not on list';
    $add = $getresponse->addContact(array(
        'name'              => $name,
        'email'             => $email,
        'dayOfCycle'        => 0,
        'campaign'          => array('campaignId' => $campaignID),
        'ipAddress'         => $ipaddress
    ));
}
?>
