/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Webfont loader -- https://github.com/typekit/webfontloader
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
WebFontConfig = {
    google: {
        families: [
            'Open Sans:300,400,700,900'
        ]
    },
    custom: {
        families: [
            'FontAwesome'
        ],
        urls: [
            '//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css'
        ],
        testStrings : {
            'FontAwesome' : '\uf083\uf015'
        }
    },
    timeout: 2000,
    // classes: false,
    loading: function() {
        if (window.console) {
            console.log('[_fontloader.js] All fonts loading');
        }
    },
    active: function() {
        if (window.console) {
            console.log('[_fontloader.js] All fonts active');
        }
        if ('google' in WebFontConfig) {
            if ('effects' in WebFontConfig['google']) {
                var gf   = document.querySelector('link[href*="fonts.googleapis.com"]'),
                    href = gf.getAttribute('href'),
                    fx   = WebFontConfig['google']['effects'].join('|');
                gf.getAttributeNode('href').value = href + '&effect=' + fx;
                if (window.console) {
                    console.log('[_fontloader.js] google font effects loaded');
                }
                document.querySelector('html').classList.add('wf-gfx-loaded');
            }
        }
    },
    inactive: function() {
        if (window.console) {
            console.log('[_fontloader.js] A font is inactive');
        }
    },
    fontloading: function(familyName, fvd) {
        if (window.console) {
            console.log('[_fontloader.js] Font loading: "' + familyName + '" (' + fvd + ')');
        }
    },
    fontactive: function(familyName, fvd) {
        if (window.console) {
            console.log('[_fontloader.js] Font loaded: "' + familyName + '" (' + fvd + ')');
        }
        document.querySelector('html').classList.remove('wf-' + familyName.toLowerCase().replace(' ', '').replace('-', '') + '-' + fvd + '-active');
    },
    fontinactive: function(familyName, fvd) {
        if (window.console) {
            console.log('[_fontloader.js] Font inactive: "' + familyName + '" (' + fvd + ')');
        }
    }
};

(function(d) {
    var wf = d.createElement('script'), s = d.scripts[0];
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js';
    s.parentNode.insertBefore(wf, s);
})(document);
