/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Site Specific Functions (Responsive)
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
sawtours = (function ($) {
    'use strict';

    var E  = {}, /* set an object to streamline all DOM elements */
        fn = {}, /* set an object to streamline all functions */
        jsFolder = '/themes/user/default_site/plugins/', /* The path to the JS folder */
        size = {
            /* in layout.scss match the variables (no units):
                $tablet_breakpoint and $desktop_breakpoint
             */
            'tablet'  : 480,
            'desktop' : 768
        },
        device;

/* Set default values of Ajax requests */
    $.ajaxSetup({ cache: true });

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 
    fn.initializeVars = function () {
    // Set up contextual regions for quicker selection/searching later
        E.header      = $('header[role="banner"]');
        E.mainNav     = $('nav.main');
        E.subNav      = $('nav.subnav');
        E.mainContent = $('#main-content');
        E.footer      = $('footer[role="contentinfo"]');
    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 
    fn.getDevice = function () {
    /* get device based on responsive size
       you can test against "device" variable in any function
       DEPENDENCIES - global vars "size" and "device"
     */
        var runCheck = debounce(function () {
                if ( $(window).width() <= size['tablet'] ) {
                    device = 'mobile'
                }
                if ( $(window).width()  > size['tablet'] && $(window).width() <= size['desktop'] ) {
                    device = 'tablet'
                }
                if ( $(window).width() >= size['desktop'] ) {
                    device = 'desktop'
                }
                // console.log('width: ' + $(window).width() + ' | device: ' + device);
                return device;
            }, 24, true);
        runCheck();
        $(window).resize( function () {
            runCheck();
        });
        return device;
    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.mobileSideNav = function () {
      $('a[href="#navigation"]').on('click', function(e) {
        e.preventDefault();
        $('#navigation, header').toggleClass('open');
      });
    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.progressBar = function () {
        var links   = $('a:not(".further-actions")', '#navigation'),
            total   = links.length,
            count   = $('a.complete', '#navigation').length + $('a.in-progress', '#navigation').length,
            percent = Math.floor((count/total) * 100),
            bar     = $('.progress-bar');
        if (bar.length > 0) {
            bar.attr('data-percent', percent);
        }
    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.formFocus = function (selector) {
    /* adds a tagetable class to filled, placeholdered and focused input fields */
        var ctx = $(selector),
            fields = $('input:not([type="submit"], [type="reset"]), textarea', ctx),
            functionClass = 'fn_formFocus-filled';
        fields.each(function() {
            if ($(this).val() != '' || $(this)[0].hasAttribute('placeholder')) {
                $(this).parents('.group').addClass(functionClass)
            }
        }).on({
            focusout : function () {
                if ($(this).val() == '' || ($(this).attr('placeholder') == '' && $(this).val() == '')) {
                    $(this).parents('.group').removeClass(functionClass);
                }
            },
            focusin : function () {
                $(this).parents('.group').addClass(functionClass);
            }
        })
    }

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.formatPhoneNumbers = function (form, fieldName) {
        var ctx         = $(form),
            phoneFields = $('input[name$="' + fieldName + '"]', ctx);
        phoneFields.each(function () {
            $(this)
                .attr({
                    // 'placeholder' : '(###) ###-####',
                    'pattern'     : '\\(\\d{3}\\) \\d{3}-\\d{4}'
                })
                .on('blur', function () {
                    var field = $(this),
                        value = $(this).val(),
                        strip = value.replace(/\D/g,''),
                        spltz = strip.split(''),
                        newValue;

                    (spltz[0] == 'undefined') ? 'X' : spltz[0];
                    (spltz[1] == 'undefined') ? 'X' : spltz[1];
                    (spltz[2] == 'undefined') ? 'X' : spltz[2];
                    (spltz[3] == 'undefined') ? 'X' : spltz[3];
                    (spltz[4] == 'undefined') ? 'X' : spltz[4];
                    (spltz[5] == 'undefined') ? 'X' : spltz[5];
                    (spltz[6] == 'undefined') ? 'X' : spltz[6];
                    (spltz[7] == 'undefined') ? 'X' : spltz[7];
                    (spltz[8] == 'undefined') ? 'X' : spltz[8];
                    (spltz[9] == 'undefined') ? 'X' : spltz[9];

                    if (value != '') {
                        newValue = '(' + spltz[0] + spltz[1] + spltz[2] + ') ' + spltz[3] + spltz[4] + spltz[5] + '-' + spltz[6] + spltz[7] + spltz[8] + spltz[9];
                        $(this).val(newValue.replace(/undefined/g,''))
                    };
                });
        });
    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.loadChosen = function (selector, options) {
        var scriptURL = jsFolder + 'chosen.jquery-imp.js',//'chosen.jquery.min.js',
            selector  = $(selector),
            options   = (!options || options == '') ? {
                    //defaults listed here: http://harvesthq.github.io/chosen/options.html
                              allow_single_deselect : false,
                                     disable_search : false,
                           disable_search_threshold : 0,
                           enable_split_word_search : true,
                             inherit_select_classes : false,
                               max_selected_options : Infinity,
                                    no_results_text : 'No results match',
                          placeholder_text_multiple : 'Select Some Options',
                            placeholder_text_single : 'Select an Option',
                                    search_contains : false, // By default, Chosen’s search matches starting at the beginning of a word. Setting this option to true allows matches starting from anywhere within a word. This is especially useful for options that include a lot of special characters or phrases in ()s and []s.
                           single_backstroke_delete : true,
                                              width : selector.parents('form').width() + 'px', // The width of the Chosen select box. By default, Chosen attempts to match the width of the select box you are replacing. If your select is hidden when Chosen is instantiated, you must specify a width or the select will show up with a width of 0.
                           display_disabled_options : true,
                           display_selected_options : true,
                    include_group_label_in_selected : false,
                    browser_is_supported : true
                } : options;
        if (selector.is(':visible')) {
             $.getScript(scriptURL)
                 .done(function () {
                   selector.chosen(options);
                 })
                 .fail(function () {
                     if (window.console) {
                         console.log(scriptURL + ' didn\'t load');
                     }
                 });
        };
    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.loadValidator = function (selector) {
        // Source for files: https://github.com/posabsolute/jQuery-Validation-Engine
        var scripts = [
                'jquery.validationEngine.js',
                'jquery.validationEngine-en.js'
            ],
            count   = 0,
            goScripts,
            target  = $(selector);

        for (var i = 0; i < scripts.length; i++) {
            var url = jsFolder + scripts[i];
            $.getScript(url)
                .done(function () {
                    count++;
                    if (count == scripts.length) {
                        goScripts();
                    }
                })
                .fail(function () {
                    if (window.console) {
                        console.log(url + ' failed to load.');
                    };
                });
        }

        goScripts = function () {
            $(target).validationEngine('attach', {
                relative : true,
                overflownDIV : 'main',
                promptPosition : 'bottomLeft',
                validateNonVisibleFields : true,
                updatePromptsPosition : true
            });
        }
    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.loadFancyBox = function (selector, options) {
        var scriptURL = jsFolder + 'jquery.fancybox.pack.js',
            selector = $(selector),
            options  = (!options || options == '') ? {
                    //defaults listed here: http://fancyapps.com/fancybox/
                    padding : 15,
                    marging : 20,
                    width: 800,
                    height: 600,
                    minWidth: 100,
                    minHeight: 100,
                    maxWidth: 9999,
                    maxHeight: 9999,
                    autoSize: true,
                    autoHeight: false,
                    autoWidth: false
                    // there's a crap-ton more at the url above -- these are defaults shown as a guide for what can be overridden with
                    // options.padding == [42,12,42,12]
                } : options;
        $.ajaxSetup({ cache: true });
        $.getScript(scriptURL)
            .done(function () {
                console.log('load yourself')
                console.log(options)
                selector.fancybox(options);
            })
            .fail(function () {
                if (window.console) {
                    console.log(scriptURL + ' didn\'t load');
                }
            });
    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.loadSignature = function (form, signatureSelector, dataFieldSelector) {
        var scriptURL = jsFolder + 'jSignature.min.js',
            ctx       = $(form),
            signature = $(signatureSelector);
        signature.append('<button id="reset-signature" class="small button">Clear Signature</button>');
        if (signature.is(':visible')) {
            $.getScript(scriptURL)
                .done(function () {
                    var reset = $('#reset-signature'),
                        dataField = $(dataFieldSelector),
                        defaultValue = 'image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj48c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIiB3aWR0aD0iMCIgaGVpZ2h0PSIwIj48L3N2Zz4=',
                        datapair;
                    signature.jSignature();
                    reset.on('click', function (e) {
                        e.preventDefault();
                        signature.jSignature('reset');
                        dataField.attr('value', '');
                    })
                    form.on('submit', function (e) {
                        e.preventDefault();
                        datapair = signature.jSignature('getData', 'svgbase64');
                        dataField.val(datapair);
                        if (dataField.val() == defaultValue) {
                            signature.css({
                                'border' : '4px sold rgb(161, 14, 33)',
                                'box-shadow' : '0px 0px 4px 0px rgb(161, 14, 33)'
                            });
                            return false;
                        }
                        form[0].submit();
                    })
                })
                .fail(function () {
                    if (window.console) {
                        console.log(scriptURL + ' didn\'t load');
                    }
                });
        };
    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.guestType = function () {
        var guestForm = $('#guest-form'),
            input     = $('input[name="channel_id"]');
        $('[data-channel-toggle]').hide();
        input.on('change', function () {
            var fields = $(this).data('channel-group'),
                divs   = $('[data-channel-toggle]');
            divs.slideUp(500);
            divs.filter('[data-channel-' + fields + ']').slideDown();
        })
    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.fileUploadField = function (inputSelector, segment_1, segment_2) {
        var inputs       = document.querySelectorAll(inputSelector),
            fileClass    = (segment_1 == 'videos') ? '-video-o' : '',
            formType     = (segment_2 == 'edit') ?  'edit' : 'new',
            fileIconHTML = '&emsp;<span class="fa fa-file' + fileClass + '"></span>',
            removeHTML   = '<span class="fa fa-2x fa-times-circle remove-file" title="Remove file"></span>',
            warningHTML  = '<span class="fa fa-exclamation-triangle"></span>&emsp;Bad File Name&emsp;<span class="fa fa-exclamation-triangle"></span>',
            removeFile   = function (label, labelVal) {
                var ctx        = jQuery(label),
                    removeLink = $('span.remove-file', ctx);
                removeLink.on('click', function (e) {
                    var input = $(this).parents('.group').find('input[type="file"]'),
                        hidden = $(this).parents('.group').find('input[name*="hidden_file"]');
                    e.preventDefault();
                    input.removeAttr('disabled');
                    input.val('');
                    if (formType == 'edit') {
                        hidden.val('');
                    }
                    $(this).parents('label').html(labelVal);
                });
            };

        Array.prototype.forEach.call( inputs, function( input ) {
            //hat-tip: http://tympanus.net/codrops/2015/09/15/styling-customizing-file-inputs-smart-way/
            var label      = input.nextElementSibling,
                labelVal   = label.innerHTML,
                hiddenFile = (formType == 'edit') ? input.previousElementSibling : null;

            if (formType == 'edit') {
                if (window.console) {
                  console.log('hiddenFile.value: ' + hiddenFile.value)
                }

                hiddenFile.value = hiddenFile.value.slice((hiddenFile.value.lastIndexOf('}') + 1),hiddenFile.value.length);
                if (hiddenFile.value != '') {
                  label.querySelector( 'span.filename' ).innerHTML = hiddenFile.value + fileIconHTML + removeHTML;
                  removeFile(label, labelVal);
                }
            }

            input.addEventListener( 'change', function( e ) {
                var fileName = '';
                if( this.files && this.files.length > 1 )
                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                else
                    fileName = e.target.value.split( '\\' ).pop();

                if( fileName ) {
                    if (fileName.indexOf('\"') > 0 || fileName.indexOf('\'') > 0 ) {
                        alert('No quotation marks or apostrophes in the file name, please.');
                        label.querySelector( 'span.filename' ).innerHTML = warningHTML + removeHTML;
                    } else {
                        label.querySelector( 'span.filename' ).innerHTML = fileName + fileIconHTML + removeHTML;
                    }
                    removeFile(label, labelVal);
                } else {
                    label.innerHTML = labelVal;
                }
            });
        });

    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.validateURLField = function (selector) {
        var input = $(selector);
        input.on('focusout', function () {
            var val = $(this).val();
            if (val != '' &&  val.lastIndexOf('http', 0) != 0) {
                $(this).addClass('error');
                alert('Start with "http://" or "https://", please');
            }
        })
    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.sendInBlueAPI = function (listId, sib_name, email, returnURL, phone, sport, hsgradyear) {
        var tempName = sib_name.replace(/%20/g," "),
            fullName = tempName.split(' '),
            firstName = fullName[0],
            lastName = fullName[fullName.length - 1],
            phone = (!phone) ? '' :'+1' + phone.replace(/%20/g," ").replace(/\D/g,""),
        
            url;
        if (lastName === firstName) {
            lastName = '';
        }
        url = '/APIv3-php-library/create.php?listId=' + parseInt(listId) + ((email) ? '&email=' + email : '') + '&firstName=' + firstName + '&lastName=' + lastName + ((phone) ? '&phone=' + phone : '') + ((sport) ? '&sport=' + sport : '') + ((hsgradyear) ? '&hsgradyear=' + hsgradyear : '');
console.log(url)
        $.get(url, function (data){
            if (data.indexOf('error creating SIB account') > -1 ) {
                if (window.console) {
                    console.log(data);
                }
            }
        }).done(function() {
            setTimeout(function () {
                window.location.href = returnURL;
             }, 1500);
        })

    }

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.grAPI = function (campaignID, gr_name, email, returnURL) {
        var url = '/GetResponse_API/search.php?campaignID=' + campaignID + ((email) ? '&email=' + email : '') + '&name=' + gr_name;
        $.get(url, function (data) {
            if (window.console) {
                console.log(data);
            }
        }).done(function () {
            setTimeout(function () {
               window.location.href = returnURL;
            }, 1500);
        });
    };

    return {
        /*-----------------------------*
            set Public APIs that can be
            called on any page using
            <script type="text/javascript">
                $(document).ready(function(){
                    'use strict';
                    sawtours.js.testFunction();
                });
            </script>
        -----------------------------*/
        testFunction       : fn.testFunction,
        getDevice          : fn.getDevice,
        progressBar        : fn.progressBar,
        loadChosen         : fn.loadChosen,
        loadValidator      : fn.loadValidator,
        loadFancyBox       : fn.loadFancyBox,
        formFocus          : fn.formFocus,
        formatPhoneNumbers : fn.formatPhoneNumbers,
        guestType          : fn.guestType,
        loadSignature      : fn.loadSignature,
        fileUploadField    : fn.fileUploadField,
        validateURLField   : fn.validateURLField,
        grAPI              : fn.grAPI,
        sendInBlueAPI      : fn.sendInBlueAPI,

        init : function () {
            /*-----------------------------*
                these always run
            -----------------------------*/
            fn.initializeVars();
            fn.getDevice();
            fn.mobileSideNav();
        }
    }
})(jQuery);

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    Admin dashboard Specific Functions
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

sawtoursDash = (function($) {
    'use strict';

    var fn = {}, /* set an object to streamline all functions */
        jsFolder = '/themes/user/default_site/plugins/';

/* Set default values of Ajax requests */
    $.ajaxSetup({ cache: true });

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.tableFilter = function() {
        var filter = $('#top input[name="filter"]'),
            table  = $('table.filter');

//rewrite jquery :contains selector to be case insensitive
        jQuery.expr[':'].contains = function(a, i, m) {
          return jQuery(a).text().toUpperCase()
              .indexOf(m[3].toUpperCase()) >= 0;
        };

        filter.next('button').on('click', function(e) {
            e.preventDefault();
        }).end().on('keyup change', function() {
            var val  = this.value.split(' '),
                rows = table.find('tr'),
                icon = $(this).next('button').find('span');
            if (this.value == '') {
                rows.show();
                icon.addClass('fa-search').removeClass('fa-times');
                return;
            }
            icon.removeClass('fa-search').addClass('fa-times').on('click', function () {
                $(this).parent().prev('input').val('');
                rows.show();
                $(this).addClass('fa-search').removeClass('fa-times');
                return;
            });

            rows.hide();
            rows.filter(function (i,v) {
                var row = $(this);
                for (var d = 0; d < val.length; ++d) {
                    if (row.is(':contains("' + val[d] + '")')) {
                        return true;
                    }
                }
                return false;
            }).show();
        });
    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.loadTablesorter = function (selector, options) {
        var script = jsFolder + 'tablesorter.js',
            table  = $(selector);
        $.getScript(script)
          .done(function () {
              table.tablesorter(options);
          })
          .fail(function () {
              if (window.console) {
                  console.log(script + ' failed to load.')
              }
          });
    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.loadMore = function (tabName, tablesorterData, _entryStatus) {
        var tab = '/dashboard/ajax-' + tabName,
            loadMore         = $('div.load-more'),
            loadContainer    = $('div.load-container'),
            absoluteResults  = $('.ajax-container').data('absolute-results'),
            offset           = loadMore.data('offset'),
            limit            = loadMore.data('limit'),
            channel          = loadMore.data('channel'),
            entryStatus      = (_entryStatus != '') ? _entryStatus : '',

            doIt               = function (c, o, l, s) {
                var url   =  tab + '/' + encodeURIComponent(c) + '/' + o + '/' + l + ((s != '') ? '/' + s : ''),
                    count = ($('.ajax-container').children('tr').length < absoluteResults) ? $('.ajax-container').children('tr').length : absoluteResults;
                if (count != absoluteResults) {
                    $('.loading-counter span.count').html(count);
                    loadContainer.load(url, function (response) {
                        if (response.indexOf('End of Entries') > -1) {
                            loadMore.remove();
                            fn.loadTablesorter(tablesorterData['tableSelector'], tablesorterData['tablesorterOptions']);
                        }
                        else {
                            offset = $('.ajax-container').children('tr').length + l;
                            doIt(channel, offset, limit);
                        }
                        loadContainer.find('.ajax-entry').appendTo('.ajax-container');
                        fn.archiveEntry();
                        fn.profileLinks();
                    });
                }
                if (count == absoluteResults) {
                    $('.loading-counter span.count').html(count);
                    $('.loading-counter').fadeOut(500);
                }
            };
        $('.ajax-container tfoot td:first-child').append('<div class="loading-counter"><i class="fa fa-spinner fa-spin"></i>&emsp;<span class="count"></span> of <span class="total">' + absoluteResults + '</span></div>')
        doIt(channel, offset, limit, entryStatus);
    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.archiveEntry = function () {
        var script = jsFolder + 'jquery.form.min.js',
            link = $('a.archive, .archive > a'),
            url  = link.attr('href');
        $.getScript(script)
            .done(function () {
                link.on('click', function(e) {
                    var form = $(this).parents('form'),
                        row  = $(this).parents('tr');
                    e.preventDefault();
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function(data) {
                            if (data.success) {
                                if (window.console) {
                                    console.log('Entry ID ' + data.entry_id + '\'s status has been changed ')
                                }
                                if (form.is('.closed, .open, .archived')) {
                                    row.slideUp(1000);
                                }
                                if (form.is('.interviewed')) {
                                    location.reload();
                                }
                                if (row.length == 0) {
                                    location.reload();
                                }
                            } else {
                                if (window.console) {
                                    console.log('Failed with the following errors: '+data.errors.join(', '));
                                }
                            }
                        }
                    });
                });
            })
            .fail(function () {
                if (window.console) {
                    console.log(script + ' failed to load.')
                }
            })
        ;
    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.scheduleInterview = function (previousUrl) {
        var script = jsFolder + 'jquery.form.min.js',
            form   = $('form#schedule-interview'),
            rescheduleForm = $('#reschedule'),
            button = $('a#schedule-submit'),
            prev   = (previousUrl) ? previousUrl : '/dashboard/interviews',
            allTimes = [];
        form.find('input[name="interview_time"]').each(function () {
            if ($(this).val() != 'no good') {
                allTimes.push($(this).val());
            }
        });
        $.getScript(script)
            .done(function () {
                button.on('click', function(e) {
                    var form            = $('form#schedule-interview'),
                        newLine         = '%0D%0A%0D%0A',
                        halfLine        = '%0D%0A',
                        goodTime        = form.find('input[name="interview_time"]:checked').val(),
                        emailAddress    = form.find('input[data-mailto-email]').data('mailto-email'),
                        emailSubject    = (goodTime == 'no good') ? 'STUDENTathleteWorld Tours - Please reschedule your interview' : 'STUDENTathleteWorld Tours - Interview Scheduled - Please confirm with an e-mail back',
                        emailBodyGood   = [ ].join(''),
                        emailBodyReject = [ ].join(''),
                        mailToLink   = 'mailto:' + emailAddress + '?subject=' + emailSubject + '&amp;body=' + ( goodTime == 'no good' ? emailBodyReject : emailBodyGood ),
                        mailButton   = '<a class="button small email-link" href="' + mailToLink + '">2. Send Email</a>',
                        backButton   = '<a class="button small back-link " href="' + prev + '">3. Return to Previous Page</a>';

                    // e.preventDefault();
                    if (goodTime == 'no good') {
                        form.append('<input type="hidden" name="status" value="reschedule">');
                    }
                    $(this).css({'opacity':'0.5', 'cursor' : 'default'}).after(mailButton + backButton);
                    form.ajaxSubmit({
                        dataType: 'json',
                        success: function(data) {
                            if (data['success'] == 1) {
                                if (window.console) {
                                    console.log('Entry ID ' + data.entry_id + ' has been added ')
                                }
                            } else {
                                if (window.console) {
                                    console.log('Failed with the following errors: ');
                                    console.log(data);
                                    alert('Something went wrong. There is probably already an interview scheduled.');
                                }
                            }
                        },
                        error: function (data) {
                            console.log('error - data:' + data)
                        }
                    });
                });
            })
            .fail(function () {
                if (window.console) {
                    console.log(script + ' failed to load.')
                }
            })
        ;
    }

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.showUploadProgress = function (formSelector, returnHref, validateFields) {
        var form       = $(formSelector),
            script     = jsFolder + 'jquery.form.min.js',
            loaderHTML = '<div class="page-overlay">' +
                           '<span class="page-icon"><i class="fa fa-spinner fa-pulse"></i></span>' +
                           '<div class="progress-bar" data-percent="0" style="width: 50%; margin: 1em auto;"></div>' +
                           '<p>&#8230;uploading&#8230;</p>' +
                         '</div>',
            doIt       = function() {
                form.submit(function (e) {
                    var hasVal = 0,
                        fields = $(this).find(validateFields);
                    e.preventDefault();
                    fields.each(function() {
                        var field = $(this);
                        if (field.val() != '') {
                            hasVal += 1;
                        }
                    });
                    if (hasVal > 0 && hasVal <= 3) {
                        $(this).ajaxSubmit({
                            beforeSubmit: function () {
                                $('body').append(loaderHTML);
                            },
                            uploadProgress: function (event, position, total, percentComplete) {
                                var bar = $('div.progress-bar');
                                bar.attr('data-percent', percentComplete);
                            },
                            success: function() {
                                window.location.href = returnHref;
                            }
                        });
                        return false;
                    } else if (hasVal > 3) {
                        if ($('#overWarning').length == 0) { $('body').append('<div id="overWarning" class="page-overlay" style="display: none;"><span class="page-icon"><i class="fa fa-exclamation-triangle"></i></span><p>No more than 3 please</p></div>'); }
                        $('div#overWarning').fadeIn('fast').delay(1000).fadeOut('fast');
                        return false;
                    } else {
                        if ($('#emptyWarning').length == 0) { $('body').append('<div id="emptyWarning" class="page-overlay" style="display: none;"><span class="page-icon"><i class="fa fa-exclamation-triangle"></i></span><p>Please upload or link to 1-3 items</p></div>'); }
                        $('div#emptyWarning').fadeIn('fast').delay(1000).fadeOut('fast');
                        return false;
                    }
                });


            };
        $.getScript(script)
            .done(function () {
                doIt();
            })
            .fail(function () {
                if (window.console) {
                    console.log(script + ' failed to load.')
                }
            })
    };

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    fn.profileLinks = function () {
        var profileLink = $('[data-profile-link]');
        profileLink.css('cursor', 'pointer').on('click', function () {
            var url = $(this).data('profile-link');
            window.location = url;
        })
    }

    return {
        /*-----------------------------*
            set Public APIs that can be
            called on any page using
            <script type="text/javascript">
                $(document).ready(function(){
                    'use strict';
                    sawtoursDash.testFunction();
                });
            </script>
        -----------------------------*/
        tableFilter        : fn.tableFilter,
        loadTablesorter    : fn.loadTablesorter,
        loadMore           : fn.loadMore,
        scheduleInterview  : fn.scheduleInterview,
        showUploadProgress : fn.showUploadProgress,

        init : function () {
            /*-----------------------------*
                these always run
            -----------------------------*/
            fn.archiveEntry();
            fn.profileLinks();

        }
    }

})(jQuery);

/*-----------------------------------*
  When DOM is loaded, call the functions in "init" section
  -----------------------------------*/
$(document).ready(function(){
    'use strict';
    sawtours.init();
});
